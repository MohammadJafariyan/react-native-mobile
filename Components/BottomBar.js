import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import home from '../assets/img/home.png';
import cart from '../assets/img/cart.png';
import menu from '../assets/img/menu.png';
import {SettingSingleton} from '../Sceeens/NavigateManagerScreen';
import {DataBean, DataBeanService, Listener} from '../Service/DataBean';
import help from '../assets/img/help.png';
import service from '../assets/img/service.png';
import {BottomNavigation, Text} from 'react-native-paper';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {AbstractScreen} from '../Sceeens/AbstractScreen';
import {Badge} from 'react-native-paper';

export default class BottomNavigationBar extends React.Component {
  state = DataBean;

  componentDidMount() {
    DataBean.bottomBar = this;
  }

  _handleIndexChange = index => {
    DataBean.routes[index].navPress();
    this.setState({index: index});
  };

  _handleBack = index => {
    this.setState({index: index});
  };

  /* _renderScene = BottomNavigation.SceneMap({
        music: MusicRoute,
        albums: AlbumsRoute,
        recents: RecentsRoute,
    });*/

  _renderScene = () => {
    return (
      <View
        style={{
          height: '100%',
          width: '100%',
        }}
      />
    );
  };

  /*componentDidMount(): void {
    _MyLog('777777777777777777777777777777777777777777');

    const lister = new Listener();
    lister.serviceObject = () => {
      //   console.log("---------------------------------------Listener:callback ------------------------------")
    };
    if (!DataBean.listerners) {
      DataBean.listerners = [];
    }
    DataBean.listerners.push(lister);
  }*/

  render() {
    return (
      <View style={{height: 60}}>
        <BottomNavigation
          style={{height: 50}}
          navigationState={this.state}
          onIndexChange={this._handleIndexChange}
          renderScene={this._renderScene}
        />
      </View>
    );
  }
  renderBackup() {
    return (
      <View style={styles.bottomRow}>
        {/*
        <TouchableOpacity onPress={() => {
            SettingSingleton.navigator.navigate('ProfileScreen');
        }}>
          <Image source={user} style={styles.icons} />
        </TouchableOpacity>
*/}
        <TouchableOpacity
          onPress={() => {
            SettingSingleton.navigator.navigate('WizardScreen');
          }}>
          <Image source={help} style={styles.icons} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            SettingSingleton.navigator.navigate('TireServiceScreen');
          }}>
          <Image source={service} style={styles.icons} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            SettingSingleton.navigator.navigate('CategoryScreen');
          }}>
          <Image source={home} style={styles.icons} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            SettingSingleton.navigator.navigate('TblItemsListScreen');
          }}>
          <Image source={menu} style={styles.icons} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            SettingSingleton.navigator.navigate('BuySuccessedScreen');
          }}>
          {DataBean.selectedItems && DataBean.selectedItems.length > 0 && (
            <View
              style={{position: 'absolute', right: 0, backgroundColor: 'red'}}>
              <Text style={{color: 'white', fontSize: 10, zIndex: 15}}>
                جدید
              </Text>
            </View>
          )}
          <Image source={cart} style={styles.icons} />
        </TouchableOpacity>
      </View>
    );
  }

  showCart() {
    return (
      <TouchableOpacity
        onPress={() => {
          SettingSingleton.navigator.navigate('BuySuccessedScreen');
        }}>
        {DataBean.selectedItems && DataBean.selectedItems.length > 0 && (
          <View
            style={{position: 'absolute', right: 0, backgroundColor: 'red'}}>
            <Text style={{color: 'white', fontSize: 10, zIndex: 15}}>جدید</Text>
          </View>
        )}
        <Image source={cart} style={styles.icons} />
        <Text>سبد خرید</Text>
      </TouchableOpacity>
    );
  }
}

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  icons: {
    height: 30,
    width: 30,
    zIndex: -1,
  },

  bottomRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 60,
    width: windowWidth,
    backgroundColor: '#fbb717',
    position: 'absolute',
    bottom: 0,
    padding: 10,
  },
});
