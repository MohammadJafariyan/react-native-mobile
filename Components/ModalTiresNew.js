import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, Dimensions } from 'react-native';
import RadioButton from './RadioButton'

class ModalTiresNew extends Component {
    constructor(props){
        super(props);
    } 

    render() {
        const { name, Price, Partner, style } = this.props;
        const { tireWithPriceView, tireWithPriceName , tireWithPriceInfo , tireWithPriceImg} = constStyles;
        const combineStyles = StyleSheet.flatten([tireWithPriceView, style]);    
    return(
      <View>
        <View style={combineStyles}>
          <Image style={tireWithPriceImg}  resizeMode='contain' source={this.props.ImageURL}/>
          <Text numberOfLines={1} style={tireWithPriceName}>{name}</Text>
          <View style={{flexDirection:'column-reverse',justifyContent:"flex-start",}}>
            <View>
              <Text style={tireWithPriceInfo}>{Price}</Text>
              <Text numberOfLines={1} style={tireWithPriceInfo}>{Partner}</Text>
            </View>
            <View style={{height:0.01 * windowWidth}}></View>
          </View>
        </View>
        <RadioButton style={{left: 11, top: -(windowWidth*.4) + 10, position: 'absolute'}}/>
        </View>
    );
}
}
export default ModalTiresNew

const windowWidth = Dimensions.get('window').width;

const constStyles=StyleSheet.create({

    tireWithPriceView: {
        width: 0.3 * windowWidth,
        height: 0.4 * windowWidth,
        marginTop: 0.03 * windowWidth,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: '#feb816',
        borderRadius: 5,
        textAlign: 'right',
        position: 'relative',
        
      },
      tireWithPriceName: {
        color: 'black',
        fontSize: 16,
        textAlign: 'right',
        paddingRight: 5,
        marginTop: 5

      },
      
      tireWithPriceInfo: {
        color: 'white',
        fontSize: 13,
        textAlign: "right",
        paddingRight: 5,
      },
      
      tireWithPriceImg: {
        width: '95%',
        height: '50%',
        borderRadius: 5,
        marginLeft: '2.5%',
        marginRight: '2.5%',
        marginTop: 5,
        backgroundColor: 'white',
      },
      
      
})


