import {StyleSheet} from 'react-native';
export const global_Colors = {
  mainColor: '#d70a0a',
  secondColor: '#ee6060',
};

export const global_Styles = StyleSheet.create({
  footer: {
    position: 'absolute',
    bottom: 40,
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
