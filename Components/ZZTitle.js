import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {global_Colors} from './MyGlobal';

export const ZZTitle = props => {
  return (
    <View style={{flex: 1, flexDirection: 'row-reverse', marginBottom: 10}}>
      <View
        style={[
          styles.cartSuggestionTextOrangeBox,
          {backgroundColor: props.bgColor || global_Colors.secondColor},
        ]}
      />
      <Text style={styles.cartSuggestionText}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  cartSuggestionText: {
    textAlign: 'right',
    fontSize: 14,
    paddingRight: 5,
    color: '#808285',
  },

  cartSuggestionTextOrangeBox: {
    height: 5,
    width: 50,
    top: 9,
  },
});
