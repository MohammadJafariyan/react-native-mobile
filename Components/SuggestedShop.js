import React from 'react';
import { Text, View, StyleSheet, Image, Dimensions } from 'react-native';
import diamond from '../assets/img/diamond.png';

const SuggestedShop = (props) => {
    return(
            <View style={{flexDirection : 'row-reverse', justifyContent: "space-between", right:50}}>

                <View style={{flexDirection:'column'}}>
                    <View style={{alignItems:'flex-end'}}>
                    <Image style={{  width: 88 , height: 88, }} source={props.img} />
                    </View>
                    <Text style={{marginVertical: 10, textAlign: "center", color: 'grey'}}>{props.shop}</Text>
                    <View style={{flex: 1, flexDirection: "row-reverse", justifyContent: "flex-start"}}>
                        <Image style={{width: 20, height: 20}} source={diamond} resizeMode="contain"/>
                        <Text style={{color: "grey"}}>{props.score}</Text>
                    </View>
                </View>

                <View style={styles.infoParent}>
                    <Text style={{textAlign: "right", color: 'grey'}}>مشخصات: {props.info}</Text>
                    <Text style={{textAlign: "right", color: 'grey'}}>قیمت: {props.price}</Text>
                    <Text style={{textAlign: "right", color: 'grey'}}>ارسال {props.days} روز دیگر</Text>
                </View>

                <View style={styles.leftArrowViewParent}>
                    <View style={styles.leftArrowView}></View>
                </View>

            </View>
);
};

export default SuggestedShop;

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create(
    {
        infoParent: {
            flexDirection: 'column',
            top: 15,
            left: 15,
            width: windowWidth * .4
        },

        leftArrowView: {
            borderColor: "grey",
            borderBottomWidth: 2,
            borderLeftWidth: 2,
            width: 20,
            height: 20,
            transform: [{
                rotate: "45deg"
            }],
            alignSelf: "flex-end",
        },

        leftArrowViewParent: {
            width: windowWidth * .3,
            height: windowWidth * .3,
            alignItems: "center",
            justifyContent: "center"
        }
    }
)