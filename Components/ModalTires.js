import React , {Component} from 'react';
import {View,StyleSheet,Text,Image, Dimensions} from "react-native";
class ModalTires extends Component{
    render(){
        return(
            <View style={styles.tireWithPriceView}>
                    
            <Image style={styles.tireWithPriceImg}  resizeMode='contain' source={this.props.ImageURL}/>
                    
            <Text style={styles.tireWithPriceName}>{this.props.name}</Text>
            <View style={{flexDirection:'column-reverse',justifyContent:"flex-start",}}>
                    
              <View>
            <Text style={styles.tireWithPriceInfo}>{this.props.Price}</Text>
            <Text style={styles.tireWithPriceInfo}>{this.props.Partner}</Text>
              </View>
              <View style={{height:0.01 * windowWidth}}></View>
                    
            </View>
                    
            </View>
        );
    }
}
export default ModalTires

const windowWidth = Dimensions.get('window').width;

const styles=StyleSheet.create({
    tireWithPriceView: {

        width: 0.3 * windowWidth,
        height: 0.50 * windowWidth,
        marginTop: 0.03 * windowWidth,
        marginLeft: 5,
        marginRight: 5,
        marginBottom:0,
        backgroundColor: '#feb816',
        borderRadius: 5,
        textAlign: "right",
        position: 'relative',
        opacity:0.3,
      },
      tireWithPriceName: {
        color: 'black',
        fontSize: 16,
        textAlign: "right",
        paddingRight: 5,

      },
      
      tireWithPriceInfo: {
        color: 'white',
        fontSize: 13,
        textAlign: "right",
        paddingRight: 5,
      },
      
      tireWithPriceImg: {
        width: '95%',
        height: '50%',
        borderRadius: 5,
        marginLeft: '2.5%',
        marginRight: '2.5%',
        marginTop: 5,
        backgroundColor: 'white',
      },
      
      
})

