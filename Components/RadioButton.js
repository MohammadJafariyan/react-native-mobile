import React, { Component } from 'react';
import {
    Button,
    Text,
    View,
    ScrollView,
    SafeAreaView,
    Alert,
    Image,
    TouchableOpacity,
    StatusBar,
    StyleSheet,
    Dimensions,
} from 'react-native';


class RadioButton extends Component { 
    constructor(props) {
        super(props);
    }

    state = {
        selected: false
    }
    selectItem = () => {
        this.setState({selected: !this.state.selected})
    }


   render() {
       return(
        <TouchableOpacity onPress={this.selectItem}>
           <View style={[{
        height: 24,
        width: 24,
        borderRadius: 12,
        borderWidth: 2,
        borderColor: this.state.selected ? '#fbb717' : 'grey',
        alignItems: 'center',
        justifyContent: 'center',
      }, this.props.style]}>
        {
          this.state.selected ?
            <View style={{
              height: 12,
              width: 12,
              borderRadius: 6,
              backgroundColor: '#fbb717',
            }}/>
            : null
        }
      </View>
      </TouchableOpacity>
       )
   }

}

export default RadioButton;