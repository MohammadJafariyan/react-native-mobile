import React from 'react';
import { Text, View, StyleSheet , Image} from 'react-native';

const PartnersCategory = (props) => {
    return(
         <View style={{flex: 1, flexDirection:'column', alignItems:'center'}}>
            <Image style={{width:40,height:40}} source={props.img}/>
            <Text style={{fontSize:10,textAlign:'center'}}>{props.title}</Text>
            <Text style={{fontSize:11,textAlign:'center'}}>{props.detail}</Text>
        </View>
    );
}

export default PartnersCategory;