import React,{ Component } from 'react';
import { Text, View, StyleSheet,Image,Dimensions } from 'react-native';


export default class ItemPresentation extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return(
            <View style={styles.tireWithPriceView}>
              <Image
                style={styles.tireWithPriceImg}
                resizeMode="contain"
                source={require('../assets/img/index.jpg')}
              />
              <Text style={styles.tireWithPriceName}>
                {this.props.itemComNameByPartner
                  ? this.props.itemComNameByPartner
                  : this.props.tblItemName}
              </Text>
              <Text style={styles.tireWithPriceInfo}>قیمت: {this.props.price}</Text>
              {/*
              <Text style={styles.tireWithPriceInfo}>
                فروشنده: {this.props.partnerName}
              </Text>
               */}
              <View style={{top:190,left:1,position:'absolute'}}>
                  <Image style={{width:20,height:20}} source={this.props.img}/>
              </View>
            </View>
            )
        }
    }
   // '../assets/img/FirstPage/FIX.png'
    const windowWidth = Dimensions.get('window').width;
const styles = StyleSheet.create(
    {
        tireWithPriceView: {
            width: 0.3 * windowWidth,
            height: 0.52 * windowWidth,
            marginTop: 0.03 * windowWidth,
            marginLeft: 5,
            marginRight: 5,
           // backgroundColor: '#feb816',
            backgroundColor: 'white',
            borderRadius: 5,
            textAlign: 'right',
            position: 'relative',
            borderWidth:2,
            borderTopColor:'white',
            borderRightColor:'#feb816',
            borderLeftColor:'white',
            borderBottomColor:'#feb816',
      
          },
          tireWithPriceImg: {
            width: '95%',
            height: '50%',
            borderRadius: 5,
            marginLeft: '2.5%',
            marginRight: '2.5%',
            marginTop: 5,
            backgroundColor: 'white',
          },
          tireWithPriceName: {
            color: 'black',
            fontSize: 16,
            textAlign: 'right',
            paddingRight: 5,
          },
        
          tireWithPriceInfo: {
            color: 'gray',
            fontSize: 13,
            textAlign: 'right',
            paddingRight: 5,
          },
        
        
        
    }
)






























