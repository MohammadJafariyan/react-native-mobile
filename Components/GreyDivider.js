import React, { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default class GreyDivider extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return(
            <LinearGradient colors={['white', 'grey', 'white']} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={[styles.greyDividerView, {height: parseInt(this.props.h) || 3}]}></LinearGradient>
        )
    }
}

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create(
    {
        greyDividerView: {

            width: windowWidth * .9,
            marginLeft: windowWidth * .05,
            marginRight: windowWidth * .05,
            marginVertical: 25,
            backgroundColor: 'red',
        }
    }
)
