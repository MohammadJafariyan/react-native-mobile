import React from 'react';
import {
  Button,
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {global_Colors} from './MyGlobal';

export const LKBInput = function(props) {
  return (
    <TextInput
      {...props}
      style={{
        backgroundColor: global_Colors.secondColor,
        color: 'white',
        borderWidth: 1,
        borderColor: global_Colors.mainColor,
        borderRadius: 5,
        flex: 1,
      }}
    />
  );
};
