import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

class TitleNew extends Component {
    constructor(props){
        super(props);
    } 

    render() {
        const { title, style } = this.props;
        const { sectionerText, OrangeBox } = constStyles;
        const combineStyles = StyleSheet.flatten([OrangeBox, style]);    

    return(
         <View style={{flex: 1, flexDirection: "row-reverse"}}>
            <View style={combineStyles}></View>
            <Text style={sectionerText}>{title}</Text>
        </View>
    );
}
}

export default TitleNew;

const constStyles = StyleSheet.create(
    {
        sectionerText: {
            textAlign: "right",
            fontSize: 14,
            paddingRight: 5
        },

        OrangeBox: {
            height: 5, 
            width: 50, 
            top: 9,
            backgroundColor:'orange'
        },
    }
)
