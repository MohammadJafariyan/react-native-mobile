import React from 'react';
import {Alert, BackHandler, View} from 'react-native';
import TblItemsListScreen from './TiresListScreen';
import BuySuccessedScreen from '../Service/BuySuccessedScreen';
import AfterBuySuccessScreen from '../Service/afterBuySuccessScreen';
import RegisterScreen from './RegisterScreen';
import HelloScreen from './HelloScreen';
import VerifyRegisterScreen from './VerifyRegisterScreen';
import CarInfo from '../Zaman/CarInfo';
import SellerScreen from './SellerScreen';
import {PartnersItems, TblItem} from '../Service/models';
import CodeScreen from './CodeScreen';
import SignInScreen from './SignInScreen';
import RegisterInfoScreen from './RegisterInfoScreen';
import CategoryScreen from './CategoryScreen';
import BottomNavigationBar from '../Components/BottomBar';
import {BottomNavigationLayout} from './BottomNavigationLayout';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {ParnterItemDetailScreen} from './ParnterItemDetailScreen';
import {BuySuccessedHelperScreen} from './BuySuccessedHelperScreen';
import {ProfileScreen} from './ProfileScreen';
import SignInByUsernamePasswordScreen from './SignInByUsernamePasswordScreen';
import BasketPreviewBeforeBuyScreen from './BasketPreviewBeforeBuyScreen';
import {WizardScreen} from './WizardScreen';
import {TireServiceScreen} from './TireServiceScreen';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {UsernamePasswordScreen} from './UsernamePasswordScreen';

import {BottomNavigationWithoutSearchBox} from './BottomNavigationWithoutSearchBox';
import {BottomNavigationLayoutwithFilterBox} from './BottomNavigationLayoutwithFilterBox'

import BankPaymentFailScreen from './BankPaymentFailScreen';
import BankPaymentSuccessScreen from './BankPaymentSuccessScreen';
import {MyPurchasesScreen} from './MyPurchasesScreen';
import {MyPurchaseDetailScreen} from './MyPurchaseDetailScreen';
import SelectMyCarScreen from './SelectMyCarScreen';
import SelectMyCarDetailScreen from './SelectMyCarDetailScreen';
import {MultiTabScreen} from './MultiTabScreen';
import {AllCarsHomeScreen} from './AllCarsHomeScreen';
import {YourSpecificCarHomeScreen} from './YourSpecificCarHomeScreen';
import {UploadImagesSubScreen} from './UploadImagesSubScreen';

export const SettingSingleton = {
  navigator: NavigateManagerScreen,
  isDebug: true,
  hasBottomNavigationBar: false,
};

export default class NavigateManagerScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentScreen: 'HelloScreen',
      // currentScreen: 'VerifyRegisterScreen',
    };
    SettingSingleton.navigator = this;
    // this.navigate('TblItemsListScreen');
  }

  navigate(name) {
    // console.log(name);
    _MyLog('navigate', name);

    _MyLog('DataBean.navigateScreensStack', DataBean.navigateScreensStack);
    _MyLog(DataBean.navigateScreensStack);
    DataBeanService.pushToNavScreens(this.state.currentScreen);

    this.setState({currentScreen: name});
  }
  backHandler;

  componentWillUnmount() {
    this.backHandler.remove();
  }

  componentDidMount(): void {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      return this._handleBack();
    });
  }

  render() {
 SettingSingleton.navigator = this;
    // todo:در این متد صفحه اصلی اسکرین نمایش داده می شود

    console.log(
      'NavigateManagerScreen ==>render() ==> ',
      this.renderCurrentRoute(),
    );
    return (
      <View
        style={{
          flex: 1,
        }}>
        {this.renderCurrentRoute()}
      </View>
    );
  }

  renderCurrentRoute() {
    // console.log('renderCurrentRoute', this.state.currentScreen);
    if (this.state.currentScreen == 'TblItemsListScreen') {
      return (

/*Zeini
<<<<<<< HEAD
        <BottomNavigationLayoutwithFilterBox>
=======
*/

        <BottomNavigationLayout title="تایر ها" subtitle="تایر های فروشی">

          <TblItemsListScreen />
        </BottomNavigationLayout>
      );
    }
    if (this.state.currentScreen == 'SelectMyCarDetailScreen') {
      return <SelectMyCarDetailScreen />;
    }
    if (this.state.currentScreen == 'UsernamePasswordScreen') {
      return (
        <BottomNavigationLayout
          title="اطلاعات ورود"
          subtitle="تغییر نام کاربری و رمز عبور">
          <UsernamePasswordScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'BuySuccessedScreen') {
      return (
/*
<<<<<<< HEAD
        <BottomNavigationWithoutSearchBox>
*/
        <BottomNavigationLayout
          title="سبد خرید"
          subtitle="افزایش و کاهش اقلام سبد خرید">

          <BuySuccessedScreen />
       </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'MyPurchasesScreen') {
      return (
        <BottomNavigationLayout
          title="اطلاعات خرید"
          subtitle="نتیجه مراجعه به بانک">
          <MyPurchasesScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'AfterBuySuccessScreen') {
      return (
        <BottomNavigationLayout>
          <AfterBuySuccessScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'BankPaymentFailScreen') {
      return (
        <BottomNavigationLayout
          title="خرید ناموفق"
          subtitle="نتیجه مراجعه به بانک">
          <BankPaymentFailScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'BankPaymentSuccessScreen') {
      return (
        <BottomNavigationLayout
          title="اطلاعات خرید"
          subtitle="نتیجه مراجعه به بانک">
          <BankPaymentSuccessScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'RegisterScreen') {
      return <RegisterScreen />;
    } else if (this.state.currentScreen == 'HelloScreen') {
      return <HelloScreen />;
    } else if (this.state.currentScreen == 'VerifyRegisterScreen') {
      return <VerifyRegisterScreen />;
    } else if (this.state.currentScreen == 'TireServiceScreen') {
      return (

/*
<<<<<<< HEAD
        Zeini
        <BottomNavigationLayout>
=======
*/
        <BottomNavigationLayout
          title="خدمات در محل"
          subtitle="درخواست خدمت از سرویس کاران ماهر">


          <TireServiceScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'SellerScreen') {
      return (
        <BottomNavigationLayout title="فروشنده" subtitle="صفحه معرفی فروشنده">
          <SellerScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'WizardScreen') {
      return (
        <BottomNavigationLayout title="راهنما" subtitle="راهنمای خرید">
          <WizardScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'CodeScreen') {
      return <CodeScreen />;
    } else if (this.state.currentScreen == 'SignInScreen') {
      return <SignInScreen />;
    } else if (this.state.currentScreen == 'SignInByUsernamePasswordScreen') {
      return <SignInByUsernamePasswordScreen />;
    } else if (this.state.currentScreen == 'RegisterInfoScreen') {
      return <RegisterInfoScreen />;
    } else if (this.state.currentScreen == 'BuySuccessedHelperScreen') {
      return <BuySuccessedHelperScreen />;
    } else if (this.state.currentScreen == 'BasketPreviewBeforeBuyScreen') {
      return (
        <BottomNavigationLayout title="فاکتور" subtitle=" آماده پرداخت">
          <BasketPreviewBeforeBuyScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'ParnterItemDetailScreen') {
      return (
        <BottomNavigationLayout title="صفحه محصول" subtitle="انتخاب به سبد">
          <ParnterItemDetailScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'CategoryScreen') {
      return (
        <BottomNavigationLayout
          title="دسته بندی"
          subtitle="به دنبال چه می گردید ؟ در کدام شهر ؟">
          <MultiTabScreen names={['کل ماشین ها', 'مخصوص ماشین شما']}>
            <AllCarsHomeScreen />
            <YourSpecificCarHomeScreen />
            {/*<CategoryScreen />*/}
          </MultiTabScreen>
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'ProfileScreen') {
      return (
        <BottomNavigationLayout title="پروفایل" subtitle="اطلاعات پرونده شما">
          <ProfileScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'MyPurchaseDetailScreen') {
      return (
        <BottomNavigationLayout>
          <MyPurchaseDetailScreen />
        </BottomNavigationLayout>
      );
    } else if (this.state.currentScreen == 'SelectMyCarScreen') {
      return <SelectMyCarScreen />;
    } else {
      throw new Error('خطا در سیستم ، اسکرین داده شده یافت نشد');
    }
  }

  _handleBack() {
    _MyLog('hardwareBackPress');

    _MyLog('DataBean.navigateScreensStack', DataBean.navigateScreensStack);

    if (
      DataBean.navigateScreensStack &&
      DataBean.navigateScreensStack.length > 0
    ) {
      var previeusScreen = DataBean.navigateScreensStack.pop();

      _MyLog('previeusScreen', previeusScreen);
      _MyLog(
        'DataBean.navigateScreensStack after',
        DataBean.navigateScreensStack,
      );

      this.setState({currentScreen: previeusScreen});
      DataBeanService.setBottomNavbar(previeusScreen);
    } else {
      Alert.alert('پیغام ', 'آیا قصد خروج از برنامه را دارید ؟', [
        {
          text: 'خیر',
          onPress: () => console.log('NO Pressed'),
          style: 'cancel',
        },
        {
          text: 'بله',
          onPress: () => {
            BackHandler.exitApp();
            this.backHandler.remove();
          },
        },
        //  {text: 'بله', onPress: () => this.addToBasketWithRelatedItems(item)},
      ]);
    }
    return true;
  }
}

export const Routes = [
  {name: 'TblItemsListScreen', comp: TblItemsListScreen},
  {name: 'BuySuccessedScreen', comp: BuySuccessedScreen},
  {name: 'AfterBuySuccessScreen', comp: AfterBuySuccessScreen},
  {name: 'RegisterScreen', comp: RegisterScreen},
  {name: 'HelloScreen', comp: HelloScreen},
  {name: 'VerifyRegisterScreen', comp: VerifyRegisterScreen},
  {name: 'CarInfo', comp: CarInfo},
  {name: 'SellerScreen', comp: SellerScreen},
  {name: 'CodeScreen', comp: CodeScreen},
  {name: 'SignInScreen', comp: SignInScreen},
  {name: 'RegisterInfoScreen', comp: RegisterInfoScreen},
  {name: 'CategoryScreen', comp: CategoryScreen},
  {name: 'BuySuccessedHelperScreen', comp: BuySuccessedHelperScreen},
  {name: 'BasketPreviewBeforeBuyScreen', comp: BasketPreviewBeforeBuyScreen},
  {name: 'WizardScreen', comp: WizardScreen},
  {name: 'ParnterItemDetailScreen', comp: ParnterItemDetailScreen},
  {name: 'TireServiceScreen', comp: TireServiceScreen},
  {name: 'SelectMyCarScreen', comp: SelectMyCarScreen},
  {
    name: 'SignInByUsernamePasswordScreen',
    comp: SignInByUsernamePasswordScreen,
  },
  {
    name: 'UsernamePasswordScreen',
    comp: UsernamePasswordScreen,
  },
  {
    name: 'BankPaymentSuccessScreen',
    comp: BankPaymentSuccessScreen,
  },
  {
    name: 'BankPaymentFailScreen',
    comp: BankPaymentFailScreen,
  },
  {
    name: 'MyPurchasesScreen',
    comp: MyPurchasesScreen,
  },
  {
    name: 'MyPurchaseDetailScreen',
    comp: MyPurchaseDetailScreen,
  },
  {
    name: 'SelectMyCarDetailScreen',
    comp: SelectMyCarDetailScreen,
  },
];
