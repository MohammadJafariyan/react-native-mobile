import React from 'react';
import {
  Image,
  Dimensions,
  Picker,
  SafeAreaView,
  ScrollView,
  StatusBar,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import {MyResponse, MyResponseType} from '../Service/Myglobal';
import {City} from '../Service/models';
import {SettingSingleton} from './NavigateManagerScreen';
import LinearGradient from 'react-native-linear-gradient';
import {gradientStyles} from './CodeScreen';
import TblItemService from '../Service/TblItemService';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import menu from '../assets/img/FirstPage/DynamicGrouping.png';
import neww from '../assets/img/FirstPage/new_new.png';
import perc from '../assets/img/FirstPage/percentage.png';
import srch from '../assets/img/FirstPage/searchTool.png';
import serv from '../assets/img/FirstPage/services.png';
import shop from '../assets/img/FirstPage/shops.png';
import slid from '../assets/img/FirstPage/slider.png';
import belt from '../assets/img/FirstPage/belt.png';
//import Weekly_Sales_Ad from '../assets/img/FirstPage/Weekly_Sales_Ad.png';
import Weekly_Sales_Ad from '../assets/img/ad.png';
import Benzin from '../assets/img/fuel.png';
import Taviz from '../assets/img/change.png';
import Balans from '../assets/img/balance.png';
import SuggestedTireInEmptyCart from '../Service/SuggestedTireInEmptyCart';
import sampletire from '../assets/img/index.jpg';

import GrayDivider from '../Components/GreyDivider';
import {ZZTitle} from '../Components/ZZTitle';
import TitleNew from '../Components/TitleNew';
import user from '../assets/img/user.png';
import offer from '../assets/img/offer.png';
import newicon from '../assets/img/new.png';
import services from '../assets/img/services.png';
import shop2 from '../assets/img/shop.png';
import search from '../assets/img/search.png';
import slidepic from '../assets/img/slidepic.png';
import slidepic2 from '../assets/img/slidepic2.png';
import slidepic3 from '../assets/img/slidepic3.png';
import {ImageCarousel} from '../Components/ImageCarousel';
import GeneralStatusBarColor from '../Zaman/GeneralStatusBarColor';
import {AbstractScreen} from './AbstractScreen';
import {MyPurchasesScreen} from './MyPurchasesScreen';
import CityService from '../Service/CityService';

export default class CategoryScreen extends AbstractScreen {
  constructor(state: {text: string, externalUse: boolean}) {
    super();
    if (state.externalUse) {
    } else {
      this.state = state;
      DataBean.currentScreen = this;
      this.state = DataBean;
    }
  }
  getAllCategories(selectedCityId) {
    DataBeanService.retRefreshing(true);
    var s = new TblItemService();
    return s.getAllCategories(selectedCityId);
  }
  getAllCities(): Promise<MyResponse<City>> {
    DataBeanService.retRefreshing(true);
    var s = new CityService();
    return s.getAllCity();
  }

  /*comment for merge*/
  componentDidMount() {
    // todo: در این تابع ، وب سرویس ها فراخوانی می شوند
    this._isMounted = true;

    // console.log('categoryScreen ==>componentDidMount');

    DataBeanService.retRefreshing(true);
    // todo:لیست شهر ها
    if (!DataBean.cities) {
      this.getAllCities()
        .then(res => {
          DataBeanService.retRefreshing(false);
          //   console.log('111111111111111111111111111111111111');
          if (res.type == MyResponseType.Success) {
            // after loading cities :
            if (res.list.length > 0) {
              if (!res.list[0]) {
                console.error(' res.list[0] is null');
              }
              if (!res.list[0].id) {
                console.error(' !res.list[0].id is null');
              }

              //   console.log('DataBean.selectedCityId====>');
              // اگر بار اول باشد که این کامپوننت لود می شود
              if (!DataBean.selectedCityId) {
                DataBeanService.setSelectedCityId(res.list[0].id);
                DataBeanService.setSelectedCityName(res.list[0].name);
              }
              //  console.log('DataBeanService.setCities(res.list)========>');

              DataBeanService.setCities(res.list);

              // todo:لیست دسته بندی
              this.readAndSetCategories();
            }
          } else {
            //  console.log('this.getAllCities() fail====>', res);
            alert(ApiUrls.showErrorMsg(res.message));
          }
          DataBeanService.retRefreshing(false);
        })
        .catch(e => {
          DataBeanService.retRefreshing(false);
        });
    } else {
      if (!DataBean.selectedCityId) {
        DataBeanService.setSelectedCityId(DataBean.cities[0].id);
        DataBeanService.setSelectedCityName(DataBean.cities[0].name);
      }
      this.readAndSetCategories();
    }
  }

  showCitiesItems() {
    // todo: تابع نمایش دهنده شهر ها
    return DataBean.cities.map((m, i) => {
      return <Picker.Item label={m.name} key={m.id} value={m.id} />;
    });
  }

  showCategories() {
    // todo: تابع نمایش دهنده شهر ها
    return DataBean.categories.map((m, i) => {
      return <Picker.Item label={m.name} key={m.id} value={m.id} />;
    });
  }

  showDummyList() {
    this.state = {
      array: [
        {
          title: '1',
        },
        {
          title: '2',
        },
        {
          title: '3',
        },
      ],
    };

    return this.state.array.map(element => {
      return (
        <View
          style={{
            width: windowWidth / 3 - 0.01 * windowWidth,
            height: windowWidth / 3 - 0.01 * windowWidth,
            borderWidth: 1,
            borderColor: '#F44336',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 30}}>{element.title}</Text>
        </View>
      );
    });
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={
            <RefreshControl enabled={false} refreshing={DataBean.refreshing} />
          }>
          <View style={{height: 200}}>
            <ImageCarousel images={[slidepic, slidepic2, slidepic3]} />
          </View>

          <View style={style.firstRow}>
            <TouchableOpacity
              style={[
                {backgroundColor: '#fdbb13'},
                style.iconsTouchableOpacity,
              ]}
              onPress={() => {
                SettingSingleton.navigator.navigate('ProfileScreen');
              }}>
              <Image source={user} style={style.icons} />
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                {backgroundColor: '#f69d20'},
                style.iconsTouchableOpacity,
              ]}>
              <Image source={offer} style={style.icons} />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                {backgroundColor: '#f68b1f'},
                style.iconsTouchableOpacity,
              ]}>
              <Image source={newicon} style={style.icons} />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                {backgroundColor: '#f37221'},
                style.iconsTouchableOpacity,
              ]}>
              <Image source={services} style={style.icons} />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                {backgroundColor: '#e65622'},
                style.iconsTouchableOpacity,
              ]}
              onPress={() => {
                SettingSingleton.navigator.navigate('MyPurchasesScreen');
              }}>
              <Image source={shop2} style={style.icons} />
            </TouchableOpacity>
          </View>

          <View style={{justifyContent: 'center'}}>
            {/* <Image style={{width: windowWidth*0.96, height:windowWidth*0.96*4/345 ,}}  source={belt} /> */}
            <GrayDivider />
            {/* <View style={{height: 25}} /> */}
          </View>

          <View style={style.sectionerView}>
            {/* <View style={{flex: 1, flexDirection: "row-reverse"}}>
              <View style={style.groupingBox}></View>
              <Text style={style.groupingText}>دسته بندی ها</Text>
            </View>

            <TitleNew title="تغییرات" style={{backgroundColor:'black'}}/>

            */}

            <ZZTitle title="دسته بندی ها" />
          </View>

          <View
            style={{
              flex: 1,
              //height: 100,
              //borderWidth: 5,
              //borderColor: '#F44336' ,
              flexDirection: 'row-reverse',
              justifyContent: 'flex-start',
              alignContent: 'flex-start',
              flexWrap: 'wrap',
              margin: 10,
            }}>
            {DataBean.categories && this.showCategoriesAsButtons()}

            {DataBean.categories && (
              <View>
                {!DataBean.categories ||
                  (DataBean.categories.length <= 0 && (
                    <Text style={{textAlign: 'center', marginTop: 100}}>
                      {' '}
                      بازار در این شهر خالی است
                    </Text>
                  ))}
              </View>
            )}
          </View>

          {/* <View style={{flex:1,justifyContent: 'center'}}> */}
          {/* <View style={{height: 35}} /> */}
          {/* <Image style={{width: windowWidth*0.96, height:windowWidth*0.96*4/345 ,}}  source={belt} /> */}
          <GrayDivider />
          {/* <View style={{height: 25}} /> */}
          {/* </View> */}

          <View style={{height: 280, marginBottom: 20}}>
            <Image
              resizeMode="contain"
              style={{
                width: windowWidth,
                height: '100%',
                //  marginHorizontal: "5%"
              }}
              source={Weekly_Sales_Ad}
            />
          </View>

          {/* <View style={{height: 25}} /> */}

          <ZZTitle title="سرویس های اضطراری" />
          {/* <View style={{height: 25}} /> */}
          <View
            style={{
              flexDirection: 'row-reverse',
              justifyContent: 'space-around',
              width: '90%',
              marginHorizontal: '5%',
            }}>
            <View style={{alignItems: 'center'}}>
              <Image style={{width: 90, height: 90}} source={Balans} />
              <Text>بالانس</Text>
            </View>
            <View style={{alignItems: 'center'}}>
              <Image style={{width: 90, height: 90}} source={Taviz} />
              <Text>تعویض</Text>
            </View>
            <View style={{alignItems: 'center'}}>
              <Image style={{width: 90, height: 90}} source={Benzin} />
              <Text>بنزین</Text>
            </View>
          </View>

          <View style={{justifyContent: 'center'}}>
            {/* <View style={{height: 35}} /> */}
            <GrayDivider />
            {/* <View style={{height: 25}} /> */}
          </View>

          <ZZTitle title="فروش ویژه" />
          {/* <View style={{height: 25}} /> */}

          <View
            style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
            <View style={style.cartSuggestionTiresView}>
              <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
              <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
              <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
            </View>
          </View>

          <View style={{justifyContent: 'center'}}>
            {/* <View style={{height: 35}} /> */}
            <GrayDivider />
            {/* <View style={{height: 35}} /> */}
          </View>

          <ZZTitle title="تازه ترین ها" />
          {/* <View style={{height: 25}} /> */}

          <View
            style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
            <View style={style.cartSuggestionTiresView}>
              <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
              <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
              <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
            </View>
          </View>

          {/*
          <View style={{

                  borderWidth : 5,
                  borderColor: '#F44336',

                  flexDirection: 'row-reverse',
                  flexWrap: 'wrap',

                  justifyContent: 'flex-start',

                  alignItems: 'flex-start',
                  alignContent: 'flex-start',



                  }}>

                  {this.showDummyList()}
          </View>








          */}

          <View style={{justifyContent: 'center'}}>
            <GrayDivider />
            {/* <View style={{height: 25}} /> */}
          </View>

          <View style={{zIndex: 3}}>
            <ZZTitle title="انتخاب شهر" />
            {this.renderCitiesPicker()}
          </View>

          <View style={{height: 170}} />
        </ScrollView>
      </SafeAreaView>
    );
  }

  readAndSetCategories() {
    DataBeanService.retRefreshing(true);

    // console.log('readAndSetCategories==>', DataBean.selectedCityId);
    this.getAllCategories(DataBean.selectedCityId)
      .then(res => {
        //   console.log('this.getAllCategories() == > response');
        DataBeanService.retRefreshing(false);

        if (res.type == MyResponseType.Success) {
          // after loading cities :
          if (res.list.length > 0) {
            if (!res.list[0]) {
              console.error(' res.list[0] is null');
            }
            if (!res.list[0].id) {
              console.error(' !res.list[0].id is null');
            }

            console.log('readAndSetCategories===>');
            DataBeanService.setCategories(res.list);
            console.log('readAndSetCategories===>');
          }
        } else {
          console.log('this.getAllCities() fail====>', res);
          alert(ApiUrls.showErrorMsg(res.message));
        }
      })
      .catch(e => {
        DataBeanService.retRefreshing(false);
      });
  }

  goNextScreenForCategoryScreen(selectedCategoryId, name) {
    if (!DataBean.selectedCityId || !selectedCategoryId) {
      alert('مقادیر انتخاب نشده است');
      return;
    }

    DataBeanService.setSelectedCategoryId(selectedCategoryId);
    DataBeanService.setSelectedCategoryName(name);

    SettingSingleton.navigator.navigate('TblItemsListScreen');
  }

  showCategoriesAsButtons() {
    // todo: تابع نمایش دهنده cateogry ها
    return DataBean.categories.map((m, i) => {
      return (
        <TouchableOpacity
          style={{
            borderWidth: 3,
            borderRadius: 20,
            borderColor: 'white',
            backgroundColor: '#ee710e',
            color: 'white',
            padding: 10,
            width: windowWidth / 3 - 8,
            height: windowWidth / 3 - 8,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => this.selectCategory(m.id, m.name)}>
          <View>
            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 25}}>
              {m.name}
            </Text>
          </View>
        </TouchableOpacity>
      );
    });
  }

  selectCategory(itemValue, name) {
    this.goNextScreenForCategoryScreen(itemValue, name);
  }

  renderCitiesPicker() {
    return (
      // <View style={{flexDirection:'row',borderWidth: 2,borderColor: '#ffebd6',alignItems:'center'
      // ,margin:20,borderRadius:5}}>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Picker
          selectedValue={DataBean.selectedCityId}
          style={{height: 35, width: '100%'}}
          onValueChange={(itemValue, itemIndex, arr) => {
            console.log('picker====>selected==>', itemValue);

            DataBeanService.setSelectedCategoryId(itemValue);
            DataBeanService.setSelectedCityName(
              DataBean.cities[itemIndex].name,
            );

            DataBeanService.setSelectedCityId(itemValue);
            DataBeanService.setCategories([]);
            DataBeanService.setItemList([]);

            this.readAndSetCategories();
          }}>
          {DataBean.cities && this.showCitiesItems()}
        </Picker>
        {/* <Text>
                انتخاب شهر
            </Text> */}
      </View>
    );
  }
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const style = StyleSheet.create({
  groupingText: {
    textAlign: 'right',
    fontSize: 22,
    paddingRight: 5,
  },

  groupingBox: {
    height: 5,
    width: 50,
    backgroundColor: 'orange',
    top: 16,
  },

  icons: {
    height: 50,
    width: 50,
  },

  sectionerView: {
    fontSize: 30,
    height: 40,
    width: windowWidth,
    bottom: 10,
  },

  firstRow: {
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'space-around',
    height: 80,
    bottom: 0,
    padding: 10,
  },

  container: {
    flex: 1,
    backgroundColor: '#fbb717',
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  linearBG: {
    height: windowHeight,
  },
  enterButton: {
    backgroundColor: '#72bf45',
    width: 80,
    height: 50,
    borderRadius: 5,
    marginTop: 50,
  },

  enterText: {
    color: 'white',
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
  },

  text: {
    paddingTop: 200,
    width: windowWidth,
    textAlign: 'center',
    fontSize: 50,
    fontFamily: 'Harmattan-Regular',
  },

  textinput: {
    width: 300,
    borderColor: 'black',
    backgroundColor: 'white',
    borderRadius: 10,
    height: 50,
    marginTop: 10,
    padding: 10,
    fontFamily: 'Harmattan-Regular',
    fontSize: 25,
    borderWidth: 1,
    textAlign: 'center',
  },

  registerButton: {
    backgroundColor: '#72bf45',
    width: 150,
    height: 50,
    borderRadius: 5,
    marginTop: 50,
  },

  registerText: {
    color: 'white',
    fontSize: 40,
    lineHeight: 55,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
    fontWeight: '900',
  },

  iconsTouchableOpacity: {
    width: 55,
    height: 55,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },

  searchBoxView: {
    width: windowWidth * 0.95,
    height: 100,
    marginLeft: windowWidth * 0.025,
    marginRight: windowWidth * 0.025,
    marginBottom: 10,
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
  },

  searchBoxImage: {
    height: 42,
    width: 42,
  },

  searchBoxTouchableOpacity: {
    height: 55,
    width: 55,
    backgroundColor: '#cfcfcf',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
  },

  searchBox: {
    backgroundColor: '#cfcfcf',
    color: '#303030',
    height: 55,
    width: windowWidth * 0.8,
    fontSize: 20,
    paddingRight: 10,
    textAlign: 'right',
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4,
  },
  cartSuggestionTiresView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    width: '90%',
    marginHorizontal: '5%',
  },
});
