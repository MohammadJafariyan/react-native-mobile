import React from 'react';
import {
  Alert,
  Button,
  Dimensions,
  Image,
  Modal,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  DataBean,
  DataBeanService,
  DataBeanStructure,
} from '../Service/DataBean';
import {PartnersItems, ServiceProposeViewModel} from '../Service/models';
import ServiceItemsApiService from '../Service/ServiceItemsApiSerivce';
import {MyResponseType} from '../Service/Myglobal';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import {BasketValidatorService} from '../Service/BasketValidatorService';
import cart3 from "../assets/img/FirstPage/cart3.png";
import GrayDivider from '../Components/GreyDivider';
import SuggestedTireInEmptyCart from '../Service/SuggestedTireInEmptyCart'
import sampletire from '../assets/img/index.jpg';
import ModalTires from '../Components/ModalTires';
import TitleNew from '../Components/TitleNew';
import ModalTiresNew from '../Components/ModalTiresNew';
import {AbstractScreen} from "./AbstractScreen";
import yellowCart from '../assets/img/yellow-cart.png';

export default class ServiceRelatedGoodsModal extends AbstractScreen{


  componentDidMount(): void {
    var s = new ServiceItemsApiService();

    // کد سرویس از نوع آیتم
    // کد فروشنده ی آن سرویس
    // شهر

      DataBeanService.retRefreshing(true);
    s.getServiceItemsForServiceByPartnerId(
      DataBean.selectedServiceTypeTblItem.id,
      DataBean.selectedServiceTypeTblItem.partnerId,
      DataBean.selectedCityId,
    )
      .then(res => {

          DataBeanService.retRefreshing(false);
        //  console.log('getServiceItemsForServiceByPartnerId', res);
        if (res.type == MyResponseType.Success) {
          this.setCheckedGoods(res.single.thisPartnerSell);
          this.setCheckedGoods(res.single.otherPartnersSell);

          DataBeanService.setServiceProposeViewModel(res.single);
        } else {
          ApiUrls.showErrorMsg(res.message);
        }
      })
      .catch(err => {
          DataBeanService.retRefreshing(false);
          ApiUrls.showErrorMsg(err);
      });
  }

  render() {
    return (

      <Modal
        animationType="slide"
        transparent={true}
        visible={DataBean.showServiceRelatedGoodsModal}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
             <View style={{flexDirection:"row-reverse", right:15, marginTop:30, marginBottom: 20}}>
                  <Image source={yellowCart} style={{height:25,width:25,}}></Image>
                  <View style={{left:15,}}>
                  <Text style={{color: "grey", fontSize: 20}}>ثبت سفارش خدمات</Text>
                  </View>
            </View>
            {/* <SafeAreaView> */}
              <ScrollView
                contentInsetAdjustmentBehavior="automatic"
               >
                <View
                  style={{
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-end',
                    flex: 1,
                  }}>
                 
                  {/*
                  <View style={{marginTop: 30, flexDirection: 'row'}}>
                    <Text style={{margin: 5}} />

                    <Text style={{margin: 5}}>برای انجام سرویس</Text>
                  </View>
                    <Button
                        title={DataBean.selectedServiceTypeTblItem.itemComNameByPartner ? DataBean.selectedServiceTypeTblItem.itemComNameByPartner : DataBean.selectedServiceTypeTblItem.tblItem.name}
                        color="orange"
                    />
                  <View
                    style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                    <Text style={{marginTop: 5}}>
                      اقلام زیر نیز مورد نیاز است ، انتخاب نمایید تا سرویس کار
                      همراه بیاورد و یا انتخاب نمایید که این اقلام را همراه
                      دارید
                    </Text>
                  </View>
                </View>
                    */}
                <View style={{backgroundColor:'#fbb717',  paddingHorizontal:windowWidth*.1, paddingVertical: 20, width: "100%"}}> 
                      <Text style={{textAlign: "right", color: "white", fontSize: 16}}>برای انجام سرویس اقلام زیر نیز مورد نیاز است ، در صورت نداشتن انتخاب کنید تا سرویس کار
                      همراه بیاورد. </Text>
                </View>

                  {/* <View
                    style={{flexDirection: 'row', justifyContent: 'flex-end',marginBottom:20}}>
                    <Text style={{marginTop: 5,backgroundColor:"orange",color:'white'}}>
                    برای انجام سرویس اقلام زیر نیز مورد نیاز است ، در صورت نداشتن انتخاب کنید تا سرویس کار
                      همراه بیاورد. 
                    </Text>
                  </View> */}
                </View>


                <Text style={{fontWeight: '500',fontSize: 18, textAlign: "center", width: "100%", marginTop: 10}}>
                  {DataBean.selectedServiceTypeTblItem.itemComNameByPartner ? DataBean.selectedServiceTypeTblItem.itemComNameByPartner : DataBean.selectedServiceTypeTblItem.tblItem.name}

                    </Text>


               {/*  <View
                  style={{
                    marginTop:20,
                    borderBottomColor: 'black',
                    borderBottomWidth: 1,
                  }}
                />
                */}
                    <GrayDivider h="4"/>

                <Text style={{fontSize: 18, marginBottom:10, color: "grey", width: "100%", textAlign: "right", paddingRight: windowWidth*.1}}>
                  اقلامی که این فروشنده ارائه می دهد
                </Text>
                <View style={{flex: 1}}>
                   <ScrollView horizontal={true} 
                      // contentInset={{justifyContent:'space-between'}}
                      showsHorizontalScrollIndicator={false}
                      automaticallyAdjustContentInsets
                      contentContainerStyle={{paddingLeft: 150}}
                      style={{flexDirection: "row-reverse", marginHorizontal: windowWidth*.1, flexWrap: "wrap"}}
                        >
                      {!DataBean.refreshing &&  DataBean.serviceProposeViewModel &&
                        DataBean.serviceProposeViewModel.thisPartnerSell &&
                        this.showButtons_Zeini(
                          DataBean.serviceProposeViewModel.thisPartnerSell,
                        )}

                        {(!DataBean.serviceProposeViewModel ||
                            !DataBean.serviceProposeViewModel.thisPartnerSell ||
                            !DataBean.serviceProposeViewModel.thisPartnerSell
                                .length) && <Text>خالی</Text>}
                        {DataBean.refreshing && <Text>در حال خواندن اطلاعات</Text>}
                    </ScrollView>
                </View>
                
                <GrayDivider h="4"/>


                <Text style={{fontSize: 18, marginBottom:10, color: "grey", width: "100%", textAlign: "right", paddingRight: windowWidth*.1}}>
                  میتوانید از بین فروشندگان دیگر خرید نمایید.
                </Text>
                <Text style={{fontSize: 15, marginBottom:10, color: "grey", width: "100%", textAlign: "right", paddingRight: windowWidth*.1}}>سرویس کننده آن را خرید نموده و بهمراه می آورد.</Text>

                {/*}
                <View
                  style={{
                    borderBottomColor: 'black',
                    borderBottomWidth: 1,
                  }}
                />
                */}
                <View style={{flex: 1}}>

                <ScrollView horizontal={true} contentInset={{justifyContent:'space-between'}}
                   showsHorizontalScrollIndicator={false}
                   contentInsetAdjustmentBehavior = "automatic"
                   style={{flexDirection: "row-reverse", width: windowWidth*.8, marginHorizontal: windowWidth*.1}}
                    >
                  {!DataBean.refreshing && DataBean.serviceProposeViewModel &&
                    DataBean.serviceProposeViewModel.otherPartnersSell &&
                    this.showButtons_Zeini(
                      DataBean.serviceProposeViewModel.otherPartnersSell,
                    )}

                  {(!DataBean.serviceProposeViewModel ||
                    !DataBean.serviceProposeViewModel.otherPartnersSell ||
                    !DataBean.serviceProposeViewModel.otherPartnersSell
                      .length) && <Text>خالی</Text>}

                    {DataBean.refreshing && <Text>در حال خواندن اطلاعات</Text>}
                    </ScrollView>

                </View>

                <View
                  style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row-reverse',
                    alignItems: 'space-between',
                  }}>
                  {DataBean.selectedServiceTypeTblItem &&
                    DataBean.selectedServiceTypeTblItem.relatedGoods &&
                    DataBean.selectedServiceTypeTblItem.relatedGoods.length >
                      0 && (
                       // #72bf45
                      <TouchableOpacity
                        style={{
                          ...styles.openButton,
                          backgroundColor: '#feb816',
                          flexDirection: 'row',
                          marginTop: 30,
                          marginRight: windowWidth * .1
                        }}
                        onPress={() => {
                          DataBeanService.setShowServiceRelatedGoodsModal(
                            false,
                          );

                          DataBean.selectedServiceTypeTblItem.iHaveGoodsConfirmed = false;

                          DataBeanService.pushOrUpdateToSelectedItems(
                            DataBean.selectedServiceTypeTblItem,
                          );
                          if (DataBean.callbackObject)
                          {
                            DataBean.callbackObject();
                          }
                        }}>
                        <Text style={styles.textStyle}>افزودن به سبد و ادامه</Text>
                        
                      </TouchableOpacity>
                    )}

                  {// انتخاب نکرده است
                  (!DataBean.selectedServiceTypeTblItem.relatedGoods ||
                    DataBean.selectedServiceTypeTblItem.relatedGoods.length <=
                      0) && (
                        
                    <TouchableHighlight
                      style={{
                        ...styles.openButton,
                        backgroundColor: '#fbb717',
                        marginTop: 30,
                        marginRight: windowWidth * .1
                      }}
                      onPress={() => {
                        DataBeanService.setShowServiceRelatedGoodsModal(false);

                        DataBean.selectedServiceTypeTblItem.iHaveGoodsConfirmed = true;
                        DataBean.selectedServiceTypeTblItem.relatedGoods = [];

                        DataBeanService.pushOrUpdateToSelectedItems(
                          DataBean.selectedServiceTypeTblItem,
                        );
                        if (DataBean.callbackObject)
                        {
                          DataBean.callbackObject();
                        }
                      }}>
                      <Text style={styles.textStyle}>
                        اقلام را دارم، ادامه بده
                      </Text>
                    </TouchableHighlight>
                  )}

                  <TouchableHighlight
                    style={{
                      ...styles.openButton,
                      backgroundColor: '#d4d4d4',
                      marginTop: 30,
                      marginLeft: windowWidth*.1
                    }}
                    onPress={() => {
                      DataBeanService.setShowServiceRelatedGoodsModal(false);

                      DataBean.selectedServiceTypeTblItem.iHaveGoodsConfirmed = false;
                      DataBean.selectedServiceTypeTblItem.relatedGoods = [];

                      if (DataBean.selectedServiceTypeTblItem.selected) {
                        DataBeanService.pushOrPopToBasket(
                          DataBean.selectedServiceTypeTblItem,
                        );
                      }
                      if (DataBean.callbackObject)
                      {
                        DataBean.callbackObject();
                      }
                    }}>
                    <Text style={[styles.textStyle, {color: "grey"}]}>حذف از سبد و انصراف</Text>
                  </TouchableHighlight>
                </View>
              </ScrollView>
            {/* </SafeAreaView> */}
          </View>
        </View>
      </Modal>
    );
  }

  showButtons(items: PartnersItems[]) {
    if (!items) {
      return;
    }

    return items.map((value, index, array) => {
      return (
        <TouchableOpacity
          style={{
            marginTop: 30,
            backgroundColor: '#72bf45',
            height: 50,
            padding: 10,
            minWidth: 100,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
            fontFamily: 'Harmattan-Regular',
          }}
          onPress={() => this.checkRelatePartnerItem(value)}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{color: 'white', fontWeight: 'bold'}}>
              {value.itemComNameByPartner ? value.itemComNameByPartner : value.tblItem.name} ({value.partner.name} -{' '}
              {value.partner.city.name})
            </Text>
            {value.checked && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}
          </View>
        </TouchableOpacity>
      );
    });
  }



showButtons_Zeini(items: PartnersItems[]) {
    if (!items) {
      return;
    }
    jewelStyle = function(opt) {
      const qq = (opt ? 0.6 : 1.0)
      return {
         opacity: qq,
      }
    }
    return items.map((value, index, array) => {
      return (
        <TouchableOpacity
          onPress={() => this.check_Relate_Partner_Item_Zeini(value)}>
            <ModalTiresNew ImageURL={require('../assets/img/index.jpg')}
            name    = {value.itemComNameByPartner ? value.itemComNameByPartner : value.tblItem.name}
            Price   = {value.price}
            Partner = {value.partner.name}
            style={jewelStyle(value.checked)}/>
        </TouchableOpacity>
      );
    });
  }



  /// کالا های وابتسه سرویس را به آن متصل می کند
  checkRelatePartnerItem(value: PartnersItems) {
    // ALL FALSE , UNCHECK OTHERS
    for (
      let i = 0;
      i < DataBean.serviceProposeViewModel.thisPartnerSell.length;
      i++
    ) {
      DataBean.serviceProposeViewModel.thisPartnerSell[i].checked = false;
    }
    // ALL FALSE, UNCHECK OTHERS
    for (
      let i = 0;
      i < DataBean.serviceProposeViewModel.otherPartnersSell.length;
      i++
    ) {
      DataBean.serviceProposeViewModel.otherPartnersSell[i].checked = false;
    }
    // DONE

    DataBeanService.pushOrPopToBasket(DataBean.selectedServiceTypeTblItem);

    // IS UNCHECK
    if (DataBean.selectedServiceTypeTblItem.relatedGoods) {
      var j = DataBean.selectedServiceTypeTblItem.relatedGoods.findIndex(
        r => r.id == value.id,
      );
      if (j >= 0) {
        DataBean.selectedServiceTypeTblItem.relatedGoods = [];

        value.checked = false;

        DataBeanService.setSelectedServiceTypeTblItem(
          DataBean.selectedServiceTypeTblItem,
        );
        return;
      }
    }
    // DONE

    // FIRST TIME CHECK
    DataBean.selectedServiceTypeTblItem.relatedGoods = [];
    value.checked = true;
    DataBean.selectedServiceTypeTblItem.relatedGoods.push(value);

    DataBeanService.setSelectedServiceTypeTblItem(
      DataBean.selectedServiceTypeTblItem,
    );
    // DONE
  }


/// کالا های وابتسه سرویس را به آن متصل می کند
check_Relate_Partner_Item_Zeini(value: PartnersItems) {
  // ALL FALSE , UNCHECK ourselves
  for (
    let i = 0;
    i < DataBean.serviceProposeViewModel.thisPartnerSell.length;
    i++
  ) {
    DataBean.serviceProposeViewModel.thisPartnerSell[i].checked = false;
  }
  // ALL FALSE, UNCHECK OTHERS
  for (
    let i = 0;
    i < DataBean.serviceProposeViewModel.otherPartnersSell.length;
    i++
  ) {
    DataBean.serviceProposeViewModel.otherPartnersSell[i].checked = false;
  }
  // DONE

  DataBeanService.pushOrPopToBasket(DataBean.selectedServiceTypeTblItem);

  // IS UNCHECK
  if (DataBean.selectedServiceTypeTblItem.relatedGoods) {
    var j = DataBean.selectedServiceTypeTblItem.relatedGoods.findIndex(
      r => r.id == value.id,
    );
    if (j >= 0) {
      DataBean.selectedServiceTypeTblItem.relatedGoods = [];

      value.checked = false;

      DataBeanService.setSelectedServiceTypeTblItem(
        DataBean.selectedServiceTypeTblItem,
      );
      return;
    }
  }
  // DONE

  // FIRST TIME CHECK
  DataBean.selectedServiceTypeTblItem.relatedGoods = [];
  value.checked = true;
  DataBean.selectedServiceTypeTblItem.relatedGoods.push(value);

  DataBeanService.setSelectedServiceTypeTblItem(
    DataBean.selectedServiceTypeTblItem,
  );
  // DONE
}

  setCheckedGoods(list: PartnersItems[]) {
    if (
      !list ||
      !list.length ||
      !DataBean.selectedServiceTypeTblItem.relatedGoods
    ) {
      return;
    }

    for (let i = 0; i < list.length; i++) {
      var obj = DataBean.selectedServiceTypeTblItem.relatedGoods.find(
        r => r.id == list[i].id,
      );
      if (obj != null) {
        list[i].checked = true;
        DataBean.selectedServiceTypeTblItem.relatedGoods[i] = list[i];
      }
    }
  }
}

/*
const Modal = () => {
    const [modalVisible, setModalVisible] = useState(false);
    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Hello World!</Text>

                        <TouchableHighlight
                            style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                        >
                            <Text style={styles.textStyle}>Hide Modal</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>

            <TouchableHighlight
                style={styles.openButton}
                onPress={() => {
                    setModalVisible(true);
                }}
            >
                <Text style={styles.textStyle}>Show Modal</Text>
            </TouchableHighlight>
        </View>
    );
};
*/

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 20,
    // padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: windowHeight * 0.9,
    width: windowWidth,
  },
  openButton: {
    backgroundColor: '#4d4d4d',
    /*borderRadius: 20,  */
    padding: 10,
    elevation: 2,
    width: 155,
    marginBottom: 20
  },
  textStyle: {
    color: 'white',
    fontWeight: '400',
    textAlign: 'center',
    fontSize: 15
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  items: {
    borderWidth: 5,
    borderColor: '#F44336',
    height:windowWidth*0.25,
    width:windowWidth*0.25,
    marginLeft:20,
  },
tireWithPriceContainer: {
  flex: 1,
  flexDirection: "row",
  justifyContent: "flex-end",
  flexWrap: "wrap",
  paddingBottom: 50,
},
tireWithPriceView: {

  width: 0.3 * windowWidth,
  height: 0.42 * windowWidth,
  marginTop: 0.03 * windowWidth,
  marginLeft: 5,
  marginRight: 5,
  marginBottom:0,
  backgroundColor: '#feb816',
  borderRadius: 5,
  textAlign: "right",
  position: 'relative',

},
tireWithPriceName: {
  color: 'black',
  fontSize: 16,
  textAlign: "right",
  paddingRight: 5
},

tireWithPriceInfo: {
  color: 'white',
  fontSize: 13,
  textAlign: "right",
  paddingRight: 5,
},

tireWithPriceImg: {
  width: '95%',
  height: '50%',
  borderRadius: 5,
  marginLeft: '2.5%',
  marginRight: '2.5%',
  marginTop: 5,
  backgroundColor: 'white'
},

});
