import React from 'react';
import {
  Button,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Alert,
  Image,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  Dimensions,
  RefreshControl,
} from 'react-native';
import TblItemsListScreen, {tireList_styles} from './TiresListScreen';
import {ZZTitle} from '../Components/ZZTitle';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {AbstractScreen} from './AbstractScreen';
import ServicesService from '../Service/ServicesService';
import {MyResponseType} from '../Service/Myglobal';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {_scale} from '../Helpers/SizeHelper';

export class OnFlyFilterScreen extends AbstractScreen {
  render() {
    return (
      <View>
        <View style={onFlyFilterScreenStyles.categoryContainer}>
          <View style={onFlyFilterScreenStyles.categorySelected}>
            <Text style={onFlyFilterScreenStyles.categoryNonText}>پرو 405</Text>
          </View>
          <View style={onFlyFilterScreenStyles.category}>
            <Text style={onFlyFilterScreenStyles.categoryText} />
          </View>
        </View>
      </View>
    );
  }
}
const onFlyFilterScreenStyles = StyleSheet.create({
  categoryContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  categoryNonText: {
    color: '#bfbdbd',
  },
  categoryText: {
    color: 'orange',
  },
  category: {
    color: 'orange',
  },
});
