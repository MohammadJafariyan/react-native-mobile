// @flow
import React, {Component} from 'react';

import {
  Text,
  View,
  Image,Button,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  SafeAreaView, BackHandler,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {SettingSingleton} from './NavigateManagerScreen';

import {DataBean, DataBeanService} from '../Service/DataBean';
import RegisterService from '../Service/RegisterService';
import {MyResponse} from '../Service/Myglobal';
import {MobileStorageService} from '../Service/MobileStorageService';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import BasketPreviewBeforeBuyScreen, {_KFactorScreen, FactorScreen} from "./BasketPreviewBeforeBuyScreen";
import {AbstractScreen} from "./AbstractScreen";

export default class BankPaymentFailScreen extends AbstractScreen {


  constructor(props, state, context: *) {
    super(props, state, context);
    this._KFactorScreen=DataBeanService.getActiveComponent('BasketPreviewBeforeBuyScreen');
  }

  _KFactorScreen;


  render() {

    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={<RefreshControl enabled={false} refreshing={DataBean.refreshing} />}>
          {this.renbodyForCode()}
        </ScrollView>
      </SafeAreaView>
    );
  }



  renbodyForCode() {
    return (
      <View style={{flex:1}}>
        <View style={{flex:1}} >
          <Text>{DataBean.paymentStatus.message}</Text>
          <View>
            <Button title={'تلاش مجدد'} onPress={()=>{
/*
              _KFactorScreen.validateProfileBeforeBuy()
*/
              this._KFactorScreen.validateProfileBeforeBuy()
              SettingSingleton.navigator.navigate('BasketPreviewBeforeBuyScreen');

            }}></Button>
          </View>
          <View>
            <FactorScreen items={DataBean.selectedItems}></FactorScreen>
          </View>
        </View>
      </View>
    );
  }
}
