import React from 'react';
import {Appbar} from 'react-native-paper';
import {
  Button,
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import BottomNavigationBar from '../Components/BottomBar';
import {DataBean, DataBeanService} from '../Service/DataBean';
import search from '../assets/img/search.png';
import GeneralStatusBarColor from '../Zaman/GeneralStatusBarColor';
import {_scale} from '../Helpers/SizeHelper';
import CategoryScreen from './CategoryScreen';
import {ActiveComponent} from '../Service/models';
import {SettingSingleton} from './NavigateManagerScreen';
import {AbstractScreen} from './AbstractScreen';
import {CarService} from '../Service/CarService';

export class BottomNavigationLayout extends AbstractScreen {
  constructor(state: {text: string, externalUse: boolean}) {
    super();
  }

  render():
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    return (
      <>
        {this.renderSearchBar()}
        <View style={{flex: 1}}>{this.props.children}</View>

        {this.props.onFlyButton && (
          <View style={style.footer}>
            {DataBean.showOnFlyMenu && (
              <View style={{backgroundColor: 'white'}}>
                <Text style={style.onFlyMenu}>تعویض در محل</Text>
                <Text style={style.onFlyMenu}> تعمیر در محل</Text>
                <Text style={style.onFlyMenu}>شارژ باطری</Text>
                <Text style={style.onFlyMenu}>مشاوره</Text>
              </View>
            )}
            <Button
              onPress={() => {
                DataBeanService.toggleShowOnFlyMenu();
              }}
              title="خدمات آنلاین"
            />
          </View>
        )}
        <BottomNavigationBar />
      </>
    );
  }
  _goBack = () => {
    SettingSingleton.navigator._handleBack();
  };

  _handleSearch = () => console.log('Searching');

  _handleMore = () => SettingSingleton.navigator.navigate('ProfileScreen');

  renderSearchBar() {
    return (
                            

      <Appbar.Header>
        <Appbar.BackAction onPress={this._goBack} />
        <Appbar.Content
          title={this.props.title ? this.props.title : 'Title'}
          subtitle={this.props.subtitle ? this.props.subtitle : 'subtitle'}
        />
        <Appbar.Action icon="magnify" onPress={this._handleSearch} />

        <Appbar.Action icon="account" onPress={this._handleMore} />
        <Appbar.Action
          icon="car"
          onPress={() => {
            var s = new CarService();
            s.clearMyCar().then(r => {
              SettingSingleton.navigator.navigate('SelectMyCarScreen');
            });
          }}
        />
      </Appbar.Header>
    );
  }
  renderSearchBarBackup() {
    return (
      <View>
        <GeneralStatusBarColor
          backgroundColor="white"
          barStyle="dark-content"
        />

        <View style={style.searchBoxViewL}>
          <TouchableOpacity style={{width: 88, alignItems: 'center'}}>
            <Text style={style.cityName}>{DataBean.selectedCityName}</Text>
          </TouchableOpacity>
          <TextInput
            style={style.searchBox}
            placeholder="جست و جو در کاچار"
            placeholderTextColor="grey"
          />
          <View>
            <TouchableOpacity style={style.iconsTouchableOpacity}>
              <Image style={style.searchBoxImage} source={search} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      
     
    );
  }
}

const windowWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
  iconsTouchableOpacity: {
    width: 55,
    height: 55,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },

  searchBoxImage: {
    height: windowWidth * 0.1,
    width: windowWidth * 0.1,
  },

  searchBoxViewL: {
    backgroundColor: '#cfcfcf',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    color: '#303030',
    height: 50,
    fontSize: 15,
    padding: 10,
    borderRadius: 4,
    width: '90%',
    marginHorizontal: '5%',
    marginBottom: 10,
  },

  searchBox: {
    fontSize: 17,
    paddingRight: 10,
    textAlign: 'right',
    width: windowWidth * 0.75 - 88,
    height: 30,
    borderLeftColor: 'white',
    borderLeftWidth: 2,
  },

  cityName: {
    fontSize: 20,
    color: 'grey',
    textAlign: 'center',
    width: '95%',
  },
  footer: {
    position: 'absolute',
    bottom: 80,
  },
  onFlyMenu: {
    borderBottomWidth: 1,
    padding: 10,
    margin: 10,
    backgroundColor: '#d2d2d2',
  },
});
