import React from 'react';
import {
  Alert,
  Button,
  Dimensions,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Image,
  ScrollView
} from 'react-native';
import {
  DataBean,
  DataBeanService,
  DataBeanStructure,
} from '../Service/DataBean';
import {PartnersItems} from '../Service/models';

import {AbstractScreen} from "./AbstractScreen";
import yellowCart from '../assets/img/yellow-cart.png';
import RadioButtonZeini from '../Components/RadioButtonZeini'

function numberWithCommas(x) {
  y = Number(x);
  return y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export default class SelectRelateItemsModal extends AbstractScreen{


  componentDidMount(): void {}

  render() {
    /*comment for merge*/
    return (
    
      <Modal
        animationType="slide"
        transparent={true}
        visible={DataBean.showModal}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
         
        <View style={styles.centeredView}>
          <View style={styles.modalView}>


          <ScrollView>
            <View style={{flexDirection:"row-reverse", right:15, marginTop:30}}>
                   <Image source={yellowCart} style={{height:25,width:25,}}></Image>
                   <View style={{left:15,}}>
                   <Text style={{color: "grey", fontSize: 20}}>ثبت سفارش</Text>
                   </View>
               </View>


            <View style={{backgroundColor:'#fbb717',  paddingHorizontal:windowWidth*.1, paddingVertical: 20, width: "100%", marginTop: 20}}> 
                  <Text style={{textAlign: "right", color: "white", fontSize: 16}}>کالاهای موجود در سبد شما ثبت و رزرو نشده اند برای ثبت سفارش مراحل بعدی را تکمیل کنید. </Text>
            </View>

            <Text style={styles.cartTireName}>
                      {DataBean.showRelateAlertForItem.itemComNameByPartner
                        ? DataBean.showRelateAlertForItem.itemComNameByPartner
                        : DataBean.showRelateAlertForItem.tblItem.name
                        }
                    </Text>
            <View style = {
              {
                flexDirection: 'row-reverse',
                flexWrap: "nowrap",
                padding: 5,
                marginTop: 10,
                marginBottom: 20,
                justifyContent: 'space-around',
                height: 150
              }
            } >
              <View
                    style={{
                      flex: 1,
                    }}>
                    <View style={styles.cartTireImageView}>
                      <Image
                        style={styles.cartTireImage}
                        source={require('../assets/img/index.jpg')}
                      />
                    </View>
                    <View
                      style={{width: 30, marginTop: -40, marginLeft: 10}}>
                    </View>
                  </View>
                  <View style={styles.cartProductInfoViewParent}>
                    <View style={styles.cartProductInfoView}>
                      <Text style={{textAlign: 'right', color: '#808285'}}>
                        {' '}
                        قیمت:{' '}
                      </Text>
                      <Text style={{textAlign: 'left', color: '#808285'}}>
                        {
                          DataBean.showRelateAlertForItem.muliplyPrice
                          ? numberWithCommas(DataBean.showRelateAlertForItem.muliplyPrice)
                          : numberWithCommas(DataBean.showRelateAlertForItem.price)}{' '}
                        تومان
                      </Text>
                    </View>
                    <View style={styles.cartProductInfoView}>
                      <Text style={{textAlign: 'right', color: '#808285'}}>
                        {' '}
                        شهر:‌{' '}
                      </Text>
                      <Text style={{textAlign: 'left', color: '#808285'}}>
                        {DataBean.showRelateAlertForItem.partner.city.name}
                      </Text>
                    </View>
                    <View style={styles.cartProductInfoView}>
                      <Text style={{textAlign: 'right', color: '#808285'}}>
                        {' '}
                        فروشگاه:‌{' '}
                      </Text>
                      <Text style={{textAlign: 'left', color: '#808285'}}>
                        {DataBean.showRelateAlertForItem.partner.name}
                      </Text>
                    </View>
                    </View>

            </View>
            

            <View>{this.showButtons()}</View>

            <View style={{
              width: windowWidth*.8,
              height: 50,
              marginHorizontal: windowWidth*.1,
              flexDirection: "row",
              justifyContent: "space-between",
              marginVertical: 30
            }}>
                <TouchableHighlight
                  style={{
                    backgroundColor: '#d7d7d7',
                    width: 150,
                    height: 50,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  onPress={() => {
                    DataBeanService.setShowModal(false);
                  }}>
                  <Text style={[styles.textStyle, {color: "#4d4d4d", fontWeight: "400", fontSize: 20}]}>لغو</Text>
                </TouchableHighlight>

                <TouchableOpacity style = {
                  {
                    backgroundColor: '#fbb717',
                    width: 160,
                    height: 50,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                onPress={()=>{
                  this.AddToBasket();
                }} >
                  <Text style={[styles.textStyle, {color: "white", fontWeight: "400", fontSize: 20}]}>ادامه فرایند خرید</Text>
                </TouchableOpacity>
            </View>
            </ScrollView>
          </View>
        </View>
        
      </Modal>
      
    );
  }

  showButtons() {
    return DataBean.showRelateAlertForItem.relatedBasketPartnerItems.map(
      (value, index, array) => {
        if (value.id == DataBean.showRelateAlertForItem.id) {
          return <></>;
        }

        return (
          <TouchableOpacity
            style={{
              marginTop: 30,
              height: 50,
              padding: 10,
              minWidth: 100,
              borderRadius: 5,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              fontFamily: 'Harmattan-Regular',
              flexDirection: 'row-reverse',
            }}
            onPress={() => this.checkRelatePartnerItem(value)}>


            <RadioButtonZeini myValue={value.checked} />


            <View>
              <Text style={{color: '#4d4d4d', fontWeight: '400', marginHorizontal: 10, fontSize: 18}}>
                {value.itemComNameByPartner
                  ? value.itemComNameByPartner
                  : value.tblItem.name}
              </Text>
            </View>


          </TouchableOpacity>
        );
      },
    );
  }

  AddToBasket(){
    DataBean.callbackObject[DataBean.callbackfuncName](
           true,
            DataBean.showRelateAlertForItem,
    );
  }
  checkRelatePartnerItem(value: PartnersItems) {
    for (
      let i = 0;
      i < DataBean.showRelateAlertForItem.relatedBasketPartnerItems.length;
      i++
    ) {
      DataBean.showRelateAlertForItem.relatedBasketPartnerItems[
        i
      ].checked = false;
    }

    

    //DataBean.showRelateAlertForItem.relatedBasketPartnerItems = [] ;
    value.checked = true;
    //DataBean.showRelateAlertForItem.relatedBasketPartnerItems.push(value);
    DataBeanService.setState() ;

    //DataBean.selectedServiceTypeTblItem.relatedGoods = [];
    //value.checked = true;
    //DataBean.selectedServiceTypeTblItem.relatedGoods.push(value);

    if (DataBean.callbackObject){

        console.log('callbackfuncname ==  Zeini ==>',DataBean.callbackfuncName)
      //  Add to cart is here  
      //  DataBean.callbackObject[DataBean.callbackfuncName](
      //      true,
      //      DataBean.showRelateAlertForItem,
      // );

    }
  }
}

/*
const Modal = () => {
    const [modalVisible, setModalVisible] = useState(false);
    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Hello World!</Text>

                        <TouchableHighlight
                            style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                        >
                            <Text style={styles.textStyle}>Hide Modal</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>

            <TouchableHighlight
                style={styles.openButton}
                onPress={() => {
                    setModalVisible(true);
                }}
            >
                <Text style={styles.textStyle}>Show Modal</Text>
            </TouchableHighlight>
        </View>
    );
};
*/

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    // padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: windowHeight * 0.9,
    width: windowWidth,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },

   cartTireImageView: {
       borderColor: '#fbb717',
       borderWidth: 4,
       borderRadius: 3,
       width: 140,
       height: 140,
     },

     cartTireImage: {
       width: '90%',
       height: '90%',
       margin: '5%',
     },

     cartTireName: {
       fontSize: 25,
       textAlign: 'right',
       marginRight: windowWidth * 0.1,
       marginTop: 20,
     },

     cartProductInfoView: {
      //  flex: 1,
       flexDirection: 'row-reverse',
       justifyContent: 'space-between',
       marginVertical: 10,
     },

     cartProductInfoViewParent: {
      //  flex: 1,
       flexDirection: 'column',
       paddingLeft: windowWidth * 0.1,
       paddingRight: windowWidth * 0.1,
     },
});
