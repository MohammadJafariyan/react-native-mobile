import React from 'react';
import {Button, Dimensions, Image, Text, View} from 'react-native';
import {SettingSingleton} from './NavigateManagerScreen';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class HelloScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  componentDidMount(): void {
    setTimeout(() => {
      SettingSingleton.navigator.navigate('VerifyRegisterScreen');
    }, 3000);
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#eec913',
        }}>
        <Image
          source={require('../assets/img/kachar_app_1.jpg')}
          style={{height: windowHeight, width: windowWidth}}
        />
      </View>
    );
  }
}
