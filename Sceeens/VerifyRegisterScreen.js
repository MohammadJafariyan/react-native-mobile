import React from 'react';
import {Image, Text, View} from 'react-native';
import {AsyncStorage} from 'react-native';
import {SettingSingleton} from './NavigateManagerScreen';
import {TokenService} from '../Service/tokenService';
import {MyResponse, MyResponseType} from '../Service/Myglobal';

import {DataBean} from '../Service/DataBean';
import {MobileStorageService} from '../Service/MobileStorageService';
import RegisterService from '../Service/RegisterService';
import {_scale} from '../Helpers/SizeHelper';
import {AbstractScreen} from './AbstractScreen';
import {CustomerService} from '../Service/CustomerService';
import {_MyLog} from '../MyGlobal/ApiUrls';

export default class VerifyRegisterScreen extends AbstractScreen {
  profileService: CustomerService = new CustomerService();
  constructor(state: {text: string}) {
    super();
    this.state = state;
    DataBean.currentScreen = this;
    this.state = DataBean;
  }

  _retrieveData = async () => {
    return MobileStorageService._retrieveData('token');
  };

  componentDidMount(): void {
    // دیتای ذخیره شده در خود موبایل را برمیگرداند
    this._retrieveData().then(savedToken => {
      if (savedToken) {
        this.profileService.isMyCarSelected().then(isSet => {
          _MyLog('isSet');
          _MyLog(isSet);
          _MyLog('isSet.single');
          _MyLog(isSet.single);

          if (isSet.single) {
            //قبلا انتخاب کرده است
            this.redirectToInsideApp();
          } else {
            this.redirectToSelectMyCarScreen();
          }
        });
        /* // به سرور می فرستد تا مطمئن شود که توکن درست کار می کند
        this._verifyTokenCall(savedToken).then(res => {
          if (res.Type == MyResponseType.Fail) {
            alert(res.Message);
            this.redirectToRegister();
          } else {
            // آیا ماشین اش را انتخاب کرده است ؟
            this.profileService.isMyCarSelected().then(isSet => {
              if (res.Type == MyResponseType.Success) {
                // با موفقیت برگشت
              } else {
                // خطایی در سرور بوجود آمد
                //  alert(res.Message);
              }

              _MyLog('isSet');
              _MyLog(isSet);
              _MyLog('isSet.single');
              _MyLog(isSet.single);

              if (isSet.single) {
                //قبلا انتخاب کرده است
                this.redirectToInsideApp();
              } else {
                this.redirectToSelectMyCarScreen();
              }
            });
          }
        });*/
      } else {
        this.redirectToRegister();
      }
    });
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#ee8d20',
        }}>
        <Text
          style={{color: 'white', textAlign: 'center', fontSize: _scale(26)}}>
          به
        </Text>

        <View style={{paddingBottom: 100, paddingTop: 60}}>
          <Text
            style={{color: 'white', textAlign: 'center', fontSize: _scale(46)}}>
            گاراژ
          </Text>
        </View>

        <Text
          style={{color: 'white', textAlign: 'center', fontSize: _scale(26)}}>
          سرویس کار آنلاین ماشین
        </Text>

        <Text
          style={{color: 'white', textAlign: 'center', fontSize: _scale(26)}}>
          خوش آمدید
        </Text>

        <View style={{paddingTop: 60}}>
          <Text style={{color: 'white', fontSize: _scale(16)}}>
            در حال هدایت به برنامه
          </Text>
        </View>
      </View>
    );
  }

  _verifyTokenCall() {
    var rs = new RegisterService();
    return rs.ValidateToken();
  }

  redirectToInsideApp() {
    SettingSingleton.navigator.navigate('CategoryScreen');
  }

  redirectToRegister() {
    SettingSingleton.navigator.navigate('SignInByUsernamePasswordScreen');
  }

  redirectToSelectMyCarScreen() {
    SettingSingleton.navigator.navigate('SelectMyCarScreen');
  }
}
