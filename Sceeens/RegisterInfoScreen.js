import React, {Component} from 'react';

import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import {DataBean} from '../Service/DataBean';
import {AbstractScreen} from "./AbstractScreen";

export default class RegisterInfoScreen extends  AbstractScreen {

  constructor(state: { text: string }) {
    super();

  }
  state = {
    text: '',
  };
  render() {
    return (
      <View style={styles.container}>
        <LinearGradient
          style={styles.linearBG}
          colors={['#fdbb13', '#fbe70f', '#fdbb13']}>
          <Text style={styles.text}>عضویت</Text>

          <TextInput
            placeholderTextColor="#929699"
            placeholder="نام کامل (به فارسی) "
            style={styles.textinput}
          />
          <TextInput
            placeholderTextColor="#929699"
            placeholder="شماره همراه"
            autoCompleteType="tel"
            style={styles.textinput}
          />
          <TextInput
            placeholderTextColor="#929699"
            placeholder="ایمیل"
            autoCompleteType="email"
            style={styles.textinput}
          />
          <TextInput
            placeholderTextColor="#929699"
            placeholder="شهر"
            style={styles.textinput}
          />
          <TextInput
            placeholderTextColor="#929699"
            placeholder="نوع خودرو"
            style={styles.textinput}
          />
          <TextInput
            placeholderTextColor="#929699"
            placeholder="کلمه عبور"
            secureTextEntry={true}
            style={styles.textinput}
          />

          <TouchableOpacity style={styles.registerButton}>
            <Text style={styles.registerText}>عضویت</Text>
          </TouchableOpacity>
        </LinearGradient>
      </View>
    );
  }
}
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbb717',
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  linearBG: {
    height: windowHeight,
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  text: {
    paddingTop: 200,
    width: windowWidth,
    textAlign: 'center',
    fontSize: 50,
    fontFamily: 'Harmattan-Regular',
    color: 'white',
  },

  textinput: {
    width: 300,
    borderColor: 'black',
    backgroundColor: 'white',
    borderRadius: 10,
    height: 50,
    marginTop: 10,
    padding: 10,
    fontFamily: 'Harmattan-Regular',
    fontSize: 25,
    textAlign: 'right',
  },

  registerButton: {
    backgroundColor: '#72bf45',
    width: 150,
    height: 50,
    borderRadius: 5,
    marginTop: 50,
  },

  registerText: {
    color: 'white',
    fontSize: 40,
    lineHeight: 55,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
    fontWeight: '900',
  },

  restoreAccButton: {
    backgroundColor: 'white',
    width: 300,
    height: 50,
    borderRadius: 10,
    marginTop: 25,
  },

  restoreAccText: {
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
    fontSize: 30,
  },

  test: {
    width: 200,
    height: 40,
    backgroundColor: 'white',
  },
});
