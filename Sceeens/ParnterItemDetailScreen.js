import React from 'react';
import {
  Alert,
  Button,
  default as Colors,
  Image,
  Picker,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';

import {SettingSingleton} from './NavigateManagerScreen';
import {MyResponse, MyResponseType} from '../Service/Myglobal';
import {City, PartnersItems} from '../Service/models';
import {DataBean, DataBeanService} from '../Service/DataBean';
import SelectRelateItemsModal from './SelectRelateItemsModal';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import srch from '../assets/img/FirstPage/searchTool.png';
import GrayDivider from '../Components/GreyDivider';
import Title from '../Components/Title';
import fltr from '../assets/img/FirstPage/fltr.png';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ServiceRelatedGoodsModal from './ServiceRelatedGoodsModal';
import Benzin from '../assets/img/fuel.png';
import Taviz from '../assets/img/change.png';
import Balans from '../assets/img/balance.png';
import SuggestedTireInEmptyCart from '../Service/SuggestedTireInEmptyCart';
import sampletire from '../assets/img/index.jpg';


import FixCustomer from '../assets/img/shop-icons/FIX1.png';
import PTTshop from '../assets/img/shop-icons/PTT1.png';
import PTT3 from '../assets/img/shop-icons/PTT3.png';
import SuggestedShop from '../Components/SuggestedShop';
import cart from '../assets/img/cart.png';
import iconDelete from '../assets/img/delete.png';

function numberWithCommas(x) {
  y = Number(x);
  return y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

// import FixCustomer from '../assets/img/FirstPage/FixCustomer.png';
// import PTTshop from '../assets/img/FirstPage/PTTshop.png';
import {AbstractScreen} from "./AbstractScreen";
import GreyDivider from '../Components/GreyDivider';



export class ParnterItemDetailScreen extends AbstractScreen {
    constructor(state: {text: string}) {
        super();

        DataBeanService.setShowModal(false);

    }
  addToBasketService(item: PartnersItems) {
    DataBeanService.setSelectedServiceTypeTblItem(item);
    DataBeanService.setShowServiceRelatedGoodsModal(true);
      DataBeanService.setCallback(null);

  }

    callbackForRelateItemsModal(isOk: boolean, item: PartnersItems) {
        if (isOk) {
            //this.showConfirm(item);
            this.selectThisItem(item);
            DataBeanService.setShowModal(false);
        }
    }
  addToBasketGood(item: PartnersItems) {
    item.muliplyPrice = null;
    if (item.relatedBasketPartnerItems) {
      // تنها در اضافه کردن پیغام می دهد
      if (!item.selected) {
        // آیا آیتم اصلی است ؟ اضافه کن و چیزی نگو و الی اگر آیتم اصلی نیست ؟ پس خود آن را اضافه کن و کار دیگری نکن

        var xodash = item.relatedBasketPartnerItems.find(r => r.id == item.id);

        for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
          item.relatedBasketPartnerItems[i].muliplyPrice = null;
        }

     //   console.log('item.isMainBasketItem');
      //  console.log(xodash.isMainBasketItem);
        if (xodash.isMainBasketItem) {
          item.isMainBasketItem = true;
          this.showAlertForMainItem(item);
        } else {
          this.selectThisItem(item);
        }
      } else {
        this.selectThisItem(item);
      }
    } else {
      this.selectThisItem(item);
    }
  }

    showAlertForMainItem(item: PartnersItems) {
        DataBeanService.setShowModal(true);
        DataBeanService.setShowRelateAlertForItem(item);
        DataBeanService.setCallback(this, 'callbackForRelateItemsModal');
    }
    selectThisItem(item) {
        DataBeanService.pushOrPopToBasket(item);

    }

  render() {
    return (
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          {/* MAIN LOOP Details of Selected Item  */}

          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
           {/* <Image
              style={{
                width: windowWidth * 0.96,
                height: (windowWidth * 0.96 * 62) / 345,
              }}
              source={srch}
            />*/}
            {/*
            <View style={style.searchBoxView}>
              <TouchableOpacity style={style.searchBoxTouchableOpacity}><Image style={style.searchBoxImage} source={search} /></TouchableOpacity>
              <TextInput style={style.searchBox} placeholder="جست و جو در کاچار" />
            </View>
            <Image style={{width: windowWidth*0.96, height:windowWidth*0.96*188/345 ,}}  source={slid} />
            */}
          </View>


          {/* <View style={{height: 10}} /> */}
          <View
            style={{
              flex: 1,
              flexDirection: 'row-reverse',
              justifyContent: 'space-around',
            }}>
            <TouchableOpacity>
              <Image source={fltr} style={style.icons} />
            </TouchableOpacity>
            <Text style={{fontSize: 20}}>فیلترها</Text>
            <Text style={{fontSize: 20}}>فیلتر ضروری</Text>
            {/* <View style={{width: 40}} /> */}
          </View>


          {/* <View style={{justifyContent: 'center'}}> */}
            {/* <View style={{height: 5}} /> */}
            <GrayDivider />
            {/* <View style={{height: 25}} /> */}
          {/* </View> */}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              marginLeft: 20,
            }}>


            <Title title="محصول انتخاب شده" />
          </View>

          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'space-around',
                // padding:20
            }}>

              <View style={{flexDirection: 'row',justifyContent:'space-between'}}>
                  {/* <TouchableOpacity
                      onPress={() => {
                          let item = DataBean.goDetailPartnerItem;
                          //service & motory
                          if (item.tblItem.serviceOrGood === 1 && item.sendType == 1) {
                              this.addToBasketService(item);
                          } else {
                              this.addToBasketGood(item);
                          }
                      }}
                      style={{
                          backgroundColor: '#0cc61d',
                          color: 'white',
                          padding: 5,
                          borderRadius: 10,

                      }}>
                      {!DataBean.goDetailPartnerItem.selected && <View style={{flexDirection: 'row'}}>
                          <Text style={{color: 'white'}}>
                              <FontAwesome name="cart-plus" size={23} />
                          </Text>
                          <Text style={{color: 'white', marginLeft: 4}}>
                              افزودن به سبد خرید
                          </Text>
                      </View>}

                      {DataBean.goDetailPartnerItem.selected && <View style={{flexDirection: 'row'}}>
                          <Text style={{color: 'white'}}>
                              <FontAwesome name="check" size={15} />
                          </Text>

                      </View>}

                  </TouchableOpacity> */}
                  <Text style={{fontWeight:'bold', fontSize:16, textAlign: "right", width: "100%", paddingRight: 40, marginTop: 10}}>

                      {DataBean.goDetailPartnerItem.itemComNameByPartner
                          ? DataBean.goDetailPartnerItem.itemComNameByPartner
                          : DataBean.goDetailPartnerItem.tblItem.name}
                  </Text>
              </View>
              </View>

    <View style={{
      //  width: windowWidth * .75,
      //    marginLeft: windowWidth * .125,
      //    marginRight: windowWidth * .125,
    }}>
        {/* <Text style={{fontWeight:'bold', fontSize:16, textAlign: "right", width: windowWidth * .8,}}>
              {DataBean.goDetailPartnerItem.itemComNameByPartner
                  ? DataBean.goDetailPartnerItem.itemComNameByPartner
                  : DataBean.goDetailPartnerItem.tblItem.name}
        </Text> */}


         <View
              style={{
                flex: 1,
                flexDirection: 'row-reverse',
                justifyContent: 'flex-start',
                alignItems: 'center',
               
              }}>

                

              <View
                style={{
                  flex: 1,
                  flexDirection: 'row-reverse',
                  flexWrap: 'wrap',
                  // padding: 5,
                  marginTop: 10,
                  marginBottom: 20,
                  justifyContent: 'space-around',
                  // width: "80%",
                  // marginHorizontal: "10%",
                  // alignItems: "center",
                  
                }}>
                   

                 <View style={{flex: 1}}>
                    <View style={style.cartTireImageView}>
                      <Image
                      source={require('../assets/img/index.jpg')}
                      style = {
                        {
                          width: '90%',
                          height: '90%',
                          margin: '5%',
                        }
                      }
                      /> 
                    </View>
                 </View>
                  <View style={style.cartProductInfoViewParent}>
                  <View style={style.cartProductInfoView}>
                      <Text style={{textAlign: 'right', color: '#808285'}}>مشخصات:</Text>
                      <Text numberOfLines={1} style={{textAlign: 'left', color: '#808285'}}>{DataBean.goDetailPartnerItem.tblItem.name}</Text>
                  </View>

                  <View style={style.cartProductInfoView}>
                        <Text style={{textAlign: 'right', color: '#808285'}}>قیمت:</Text>
                        <Text style={{textAlign: 'left', color: '#808285'}}>{DataBean.goDetailPartnerItem.muliplyPrice
                          ? numberWithCommas(DataBean.goDetailPartnerItem.muliplyPrice)
                          : numberWithCommas(DataBean.goDetailPartnerItem.price)}{' '}  
                          تومان
                          </Text>
                  </View>

                  <View style={style.cartProductInfoView}>
                    <Text style={{textAlign: 'right', color: '#808285'}}>فروشنده:‌ </Text>
                      <Text style={{textAlign: 'left', color: '#808285'}}>{DataBean.goDetailPartnerItem.partner.name}</Text>
                  </View>

                  <View style={style.cartProductInfoView}>
                      <Text style={{textAlign: 'right', color: '#808285'}}>آدرس: </Text>
                      <Text style={{textAlign: 'left', color: '#808285'}}>{DataBean.goDetailPartnerItem.partner.city.name}، {DataBean.goDetailPartnerItem.partner.adress}</Text>
                  </View>
                  </View>   


              </View>
            </View>
    </View>
      <View style={{
              flexDirection: 'column',
              justifyContent: 'flex-start',
              marginLeft: 20,
          }}>
             
              {/* <TouchableOpacity
                onPress={() => {
                    let item = DataBean.goDetailPartnerItem;
                    //service & motory
                    if (item.tblItem.serviceOrGood === 1 && item.sendType == 1) {
                        this.addToBasketService(item);
                    } else {
                        this.addToBasketGood(item);
                    }
                }}
                style={{
                    backgroundColor: "#8ec63f",
                    color: 'white',
                    padding: 5,
                    borderRadius: 5,
                    marginLeft: windowWidth * .05,
                    width: 155,
                    height: 35,
                    marginBottom: 5,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"

                }}> */}
                  {/* <View style={{flexDirection: 'row', justifyContent: "space-between",  alignItems: "center"}}> */}
                  {!DataBean.goDetailPartnerItem.selected && 
                   <TouchableOpacity
                onPress={() => {
                    let item = DataBean.goDetailPartnerItem;
                    //service & motory
                    if (item.tblItem.serviceOrGood === 1 && item.sendType == 1) {
                        this.addToBasketService(item);
                    } else {
                        this.addToBasketGood(item);
                    }
                }}
                style={{
                    backgroundColor: "#8ec63f",
                    color: 'white',
                    padding: 5,
                    borderRadius: 5,
                    marginLeft: windowWidth * .05,
                    width: 155,
                    height: 35,
                    marginBottom: 5,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"

                }}>
                      <Image source={cart} style={{width: 20, height: 20}}/>
                      <Text style={{color: 'white', textAlign: "center"}}>
                          افزودن به سبد خرید
                      </Text>
                       </TouchableOpacity>
                  }
                  {/* </View> */}
                  {/* <View style={{flexDirection: 'row'}}> */}
                  {DataBean.goDetailPartnerItem.selected && <View style={{ 
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  // padding: 5,
                  marginLeft: windowWidth * .05,
                  width: 155,
                  height: 35,
                  marginBottom: 5,
                  }}>

                  <TouchableOpacity

                style={{
                    backgroundColor: "#8ec63f",
                    color: 'white',
                    padding: 5,
                    borderRadius: 5,
                    // marginLeft: windowWidth * .05,
                    width: 40,
                    height: 35,
                    // marginBottom: 5,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"

                }}
                
                
                onPress={() => {
                  let item = DataBean.goDetailPartnerItem;
                  //service & motory
                  if (item.tblItem.serviceOrGood === 1 && item.sendType == 1) {
                      this.addToBasketService(item);
                  } else {
                      this.addToBasketGood(item);
                  }                
                
                }
               
              }>
                      <Image source={iconDelete} style={{width: 30, height: 30}}/>
                  </TouchableOpacity>


                   <View
                  
                style={{
                    backgroundColor: "#8ec63f",
                    color: 'white',
                    // padding: 5,
                    borderRadius: 5,
                    width: 110,
                    height: 35,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"

                }}>
                    <Text style={{color: 'white', textAlign: "center", fontSize: 11, width: "100%"}}>به سبد خرید افزوده شد</Text>
                  </View>

                  </View>
                  }
                  {/* </View> */}

              {/* </TouchableOpacity> */}
                 
              <TouchableOpacity  style={{
                  backgroundColor: '#0084cb',
                  color: 'white',
                  padding: 5,
                  borderRadius: 5,
                  marginLeft: windowWidth * .05,
                  width: 155,
                  height: 35,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"

              }}  onPress={() => {this.goSellerScreen(DataBean.goDetailPartnerItem)}}>
                  <Image source={PTT3} style={{width: 30, height: 30}}/>
                  <Text style={{color: 'white', textAlign: "center", width: 110}}>
                      صفحه فروشنده
                  </Text>
              </TouchableOpacity>
                 


            </View>

              <GrayDivider />

              <Title title="شناسه کالا" />






              <Text style={{textAlign: "right", width: "80%", marginHorizontal: "10%", marginVertical: 10, fontSize: 18, color: "grey"}}>
                  این کالا منحصر به فرد بوده و قابل استرداد نمی باشد.
                  ولی شما می توانید با تماس با مسئول این شرکت وجه خود را دریافت فرمایید
              </Text>

              <TouchableOpacity style={{
                backgroundColor: "#fbb717",
                marginTop: 5,
                borderRadius: 5,
                marginLeft: windowWidth * .1,
                width: 155,
                height: 35,
                justifyContent: "center",
                alignItems: "center"
              }}>
                <Text style={{color: "white", textAlign: "center", width: "100%"}}>بیشتر</Text>
              </TouchableOpacity>

              <GreyDivider />

            {/* <View style={{height: 15}} /> */}

            <Title title = "فروشندگان دیگر" />


            <View style={{flexDirection:'column',top:20}}>

              <SuggestedShop img={FixCustomer} shop="فروشگاه جوادی" info="" price="" score="7/8" days="۲"/>

              <GrayDivider h="1"/>

              <SuggestedShop img={PTTshop} shop="لاستیک فروشی بهمن" info="" price="" score="2/8" days="۲"/>

            </View>

            <GrayDivider />

            <Title title = "سرویس مربوطه" bgColor="#f68b1f"/>
             <View style={{

                flexDirection: 'row-reverse',
                justifyContent: 'space-around',
                width: "90%",
                marginHorizontal: "5%"

              }}>
            <View style={{alignItems: 'center',}}>
              <Image style={{  width: 90 , height: 90, }} source={Balans} />
              <Text>بالانس</Text>
            </View>
            <View style={{alignItems: 'center',}}>
              <Image style={{  width: 90 , height: 90, }} source={Taviz} />
              <Text>تعویض</Text>
            </View>
            <View style={{alignItems: 'center',}}>
              <Image style={{  width: 90 , height: 90, }} source={Benzin} />
              <Text>بنزین</Text>
            </View>
          </View>

       
           <GrayDivider />

          <Title title = "محصولات مشابه" bgColor="#f68b1f"/>
          <View style={{flex:1, flexDirection:'row'}}>
            <View style={style.cartSuggestionTiresView}>
                <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} bgColor="#f68b1f"/>
                <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} bgColor="#f68b1f"/>
                <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} bgColor="#f68b1f"/>
            </View>

        </View>

          <View style={{height: 325}} />


            <View style={{flex:1}}>
                {DataBean.showModal && <SelectRelateItemsModal />}
                {DataBean.showServiceRelatedGoodsModal && (
                    <ServiceRelatedGoodsModal />
                )}
            </View>








        </ScrollView>
      </SafeAreaView>
    );
  }
    goSellerScreen(item) {
        SettingSingleton.navigator.navigate('SellerScreen');
        //console.log('goSellerScreen', item.partner.id);

        DataBeanService.setPartner(item.partner);
    }
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const style = StyleSheet.create({
  groupingText: {
    textAlign: 'right',
    fontSize: 22,
    paddingRight: 5,
  },

  groupingBox: {
    height: 5,
    width: 50,
    backgroundColor: 'orange',
    top: 16,
  },

  icons: {
    height: 50,
    width: 50,
  },

  sectionerView: {
    fontSize: 30,
    height: 40,
    width: windowWidth,
    bottom: 10,
  },

  firstRow: {
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'space-around',
    height: 80,
    width: windowWidth,
    position: 'relative',
    bottom: 0,
    padding: 10,
  },
  cartSuggestionTiresView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap",
    position: "absolute",
    width: "90%",
    marginHorizontal: "5%"
},

cartProductInfoView: {
  flex: 1,
  flexDirection: 'row-reverse',
  justifyContent: 'space-between',
  marginBottom: 5,
},

cartProductInfoViewParent: {
  // flex: 1.2,
  flexDirection: 'column',
  // paddingLeft: windowWidth * 0.05,
  paddingRight: windowWidth * 0.05,
  width: windowWidth * .48,
  marginLeft: windowWidth * .1
},

cartTireImageView: {
  borderColor: '#fbb717',
  borderWidth: 4,
  borderRadius: 3,
  // width: 140,
  // height: 140,
  width: windowWidth * .32,
  height: windowWidth * .32,
  marginRight: windowWidth * .1
},

});
