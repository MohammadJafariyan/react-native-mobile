// @flow
import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Button,
  Picker,
  ScrollView,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import {AbstractScreen} from './AbstractScreen';
import {_scale} from '../Helpers/SizeHelper';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {ZZTitle} from '../Components/ZZTitle';
import {ImageCarousel} from '../Components/ImageCarousel';
import slidepic from '../assets/img/slidepic.png';
import slidepic2 from '../assets/img/slidepic2.png';
import slidepic3 from '../assets/img/slidepic3.png';
import {OnTheFlyCategoryScreen} from './OnTheFlyCategoryScreen';

export class AllCarsHomeScreen extends AbstractScreen {
  render() {
    return (
      <View style={{flex: 1}}>
        <OnTheFlyCategoryScreen />
        {/*<ZZTitle title="تایر های مخصوص" />*/}
        <View style={{flex: 1, flexDirection: 'row-reverse'}}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../assets/img/roadstone.jpg')}
              style={{resizeMode: 'contain'}}
            />
          </View>

          <View style={{flex: 1}}>
            <View>
              <Image
                source={require('../assets/img/roadstone.jpg')}
                style={{
                  resizeMode: 'contain',
                  width: _scale(100),
                  height: _scale(100),
                }}
              />
              <Image
                source={require('../assets/img/roadstone.jpg')}
                style={{
                  resizeMode: 'contain',
                  width: _scale(100),
                  height: _scale(100),
                }}
              />
              <Image
                source={require('../assets/img/roadstone.jpg')}
                style={{
                  resizeMode: 'contain',
                  width: _scale(100),
                  height: _scale(100),
                }}
              />
            </View>
          </View>
        </View>

        {/* <View style={{margin: _scale(20)}}>
          <ZZTitle title="خدمات مخصوص" />
        </View>

        <View style={{height: 200}}>
          <ImageCarousel images={[slidepic, slidepic2, slidepic3]} />
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../assets/img/s1.jpg')}
              style={{resizeMode: 'contain'}}
            />
          </View>

          <View style={{flex: 1}}>
            <View>
              <Image
                source={require('../assets/img/s2.jpg')}
                style={{
                  resizeMode: 'contain',
                  width: _scale(100),
                  height: _scale(100),
                }}
              />
              <Image
                source={require('../assets/img/s3.jpg')}
                style={{
                  resizeMode: 'contain',
                  width: _scale(100),
                  height: _scale(100),
                }}
              />
              <Image
                source={require('../assets/img/s1.jpg')}
                style={{
                  resizeMode: 'contain',
                  width: _scale(100),
                  height: _scale(100),
                }}
              />
            </View>
          </View>
        </View>*/}
      </View>
    );
  }
}
