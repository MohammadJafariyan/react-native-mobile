import React, {Component} from 'react';

import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  SafeAreaView, BackHandler,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {SettingSingleton} from './NavigateManagerScreen';

import {DataBean, DataBeanService} from '../Service/DataBean';
import RegisterService from '../Service/RegisterService';
import {MyResponse} from '../Service/Myglobal';
import {MobileStorageService} from '../Service/MobileStorageService';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import BasketPreviewBeforeBuyScreen, {FactorScreen} from "./BasketPreviewBeforeBuyScreen";
import {AbstractScreen} from "./AbstractScreen";

export default class BankPaymentSuccessScreen extends AbstractScreen {


  constructor(props, state, context: *) {
    super(props, state, context);
    this._KFactorScreen=DataBeanService.getActiveComponent('BasketPreviewBeforeBuyScreen');
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={<RefreshControl enabled={false} refreshing={DataBean.refreshing} />}>
          {this.renbodyForCode()}
        </ScrollView>
      </SafeAreaView>
    );
  }



  renbodyForCode() {
    return (
      <View style={{flex:1}}>
        <View style={{flex:1}} >
          <Text>پرداخت با موفقیت انجام شد</Text>
          <View>
            <FactorScreen items={DataBean.selectedItems}></FactorScreen>
          </View>
        </View>
      </View>
    );
  }
}
