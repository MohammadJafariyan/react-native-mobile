import React from 'react';
import {AbstractScreen} from './AbstractScreen';
import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  Badge,
} from 'react-native-paper';
import {ScrollView, StyleSheet, View} from 'react-native';
import {_scale} from '../Helpers/SizeHelper';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {global_Colors} from '../Components/MyGlobal';
import {TouchableOpacity} from 'react-native-ui-lib';

export class ServiceRequestStepsScreen extends AbstractScreen {
  showForthIcon() {
    return (
      <Avatar.Text
        style={
          DataBean.currentTab == 3 ? styles.selectedStep : styles.stepColor
        }
        label="4"
        size={_scale(30)}
      />
    );
  }
  showFirstIcon() {
    return (
      <Avatar.Text
        style={
          DataBean.currentTab == 0 ? styles.selectedStep : styles.stepColor
        }
        label="1"
        size={_scale(30)}
      />
    );
  }
  showScondIcon() {
    return (
      <Avatar.Text
        style={
          DataBean.currentTab == 1 ? styles.selectedStep : styles.stepColor
        }
        label="2"
        size={_scale(30)}
      />
    );
  }
  showTirdIcon() {
    return (
      <Avatar.Text
        style={
          DataBean.currentTab == 2 ? styles.selectedStep : styles.stepColor
        }
        label="3"
        size={_scale(30)}
      />
    );
  }
  render() {
    return (
      <ScrollView
        contentContainerStyle={{}}
        horizontal={true}
        contentInset={{justifyContent: 'space-between'}}
        showsHorizontalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => {
            DataBeanService.setCurrentTab(0);
          }}>
          <Card.Title
            title="درخواست"
            subtitle="خدمات در محل"
            left={this.showFirstIcon}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            DataBeanService.setCurrentTab(1);
          }}>
          <Card.Title
            title="عکس و آدرس"
            subtitle="از خرابی ماشین"
            left={this.showScondIcon}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            DataBeanService.setCurrentTab(2);
          }}>
          <Card.Title
            title="بررسی"
            subtitle="توسط خدمات کاران"
            left={this.showTirdIcon}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            DataBeanService.setCurrentTab(3);
          }}>
          <Card.Title
            title="انتخاب"
            subtitle="بررسی توسط شما"
            left={this.showForthIcon}
          />
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  stepColor: {
    backgroundColor: '#d0d0d0',
    color: 'white',
  },
  selectedStep: {
    backgroundColor: global_Colors.mainColor,
    color: 'white',
  },
});
