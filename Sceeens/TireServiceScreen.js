import React from 'react';
import {
  Dimensions,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {DataTable, Button} from 'react-native-paper';

import {DataBean, DataBeanService} from '../Service/DataBean';
import {AbstractScreen} from './AbstractScreen';
import ServicesService from '../Service/ServicesService';
import {MyResponseType} from '../Service/Myglobal';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {_scale} from '../Helpers/SizeHelper';
import {OnFlyFilterScreen} from './OnFlyFilterScreen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {TblCategory} from '../Models/tblCategory';
import {TblService} from '../Models/tblCar';
import {Badge, Caption, Chip} from 'react-native-paper';
import {global_Colors} from '../Components/MyGlobal';
import {numberWithCommas} from './BasketPreviewBeforeBuyScreen';
import {PartnersItems} from '../Models/partnersItems';
import {Avatar} from 'react-native-paper';
import {UploadRequestServicePhotosModal} from './UploadRequestServicePhotosModal';
import {ServiceRequestStepsScreen} from './ServiceRequestStepsScreen';
import {_renderTopHeader} from './YourSpecificCarHomeScreen';
import {UploadImagesSubScreen} from './UploadImagesSubScreen';
import {ServiceWorkersScreen} from './ServiceWorkersScreen';
import {ServiceFactorBeforeBuyScreen} from './ServiceFactorBeforeBuyScreen';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export class TireServiceScreen extends AbstractScreen {
  servicesService = new ServicesService();

  componentWillUnmount() {
    DataBeanService.setShowModal(false);
  }

  componentDidMount() {
    DataBeanService.setCurrentStep();
    this.servicesService
      .getAllServices()
      .then(res => {
        if (res.type == MyResponseType.Success) {
          DataBeanService.setServiceServices(res.list);
        } else {
          alert(res.message);
        }
      })
      .catch(e => {
        _MyLog(e);
        alert('خطا در اتصال به اینترنت');
      });
  }

  render() {
    return (
      <>
        <SafeAreaView style={{flex: 1}}>
          <ScrollView contentInsetAdjustmentBehavior="automatic">
            {this.renderBody()}
          </ScrollView>
          {DataBean.currentTab == 0 && this.okButton()}
        </SafeAreaView>
        {/* <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={{flexGrow: 1}}
          />
          {this.renderBody()}
        </SafeAreaView>*/}
      </>
    );
  }

  renderBody() {
    return (
      <View style={{flex: 1}}>
        {this.theader()}

        <View style={{flex: 7}}>
          {DataBean.currentTab == 0 && this.renderServices()}
          {DataBean.currentTab == 1 && <UploadImagesSubScreen />}
          {DataBean.currentTab == 2 && <ServiceWorkersScreen />}
          {DataBean.currentTab == 3 && <ServiceFactorBeforeBuyScreen />}
        </View>

        <View style={{height: _scale(150)}} />

        {/*{DataBean.showModal && <UploadRequestServicePhotosModal />}*/}
      </View>
    );
  }

  renderServices() {
    if (!DataBean.serviceServices) {
      return <></>;
    }
    return DataBean.serviceServices.map((value, index, array) => {
      return (
        <View>
          <TouchableOpacity
            style={{
              marginRight: _scale(20),
              marginLeft: _scale(20),

              marginTop: _scale(10),
              borderRadius: _scale(5),
              backgroundColor: '#efebeb',
              borderLeft: 2,
              borderLeftColor: 'orange',
            }}
            onPress={() => {
              this._selectCategory(value);
            }}>
            <View
              style={{
                marginRight: _scale(20),
                marginLeft: _scale(20),

                height: 50,
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <FontAwesome name={value.icon} size={_scale(25)} />
                <Text>{value.name}</Text>
              </View>
              {this.showSelectedPrice(value)}

              {this.showSelectedCount(value)}
            </View>
          </TouchableOpacity>
          {this.renderSubItemsForService(value)}
        </View>
      );
    });
  }

  renderSubItemsForService(value) {
    if (value.selected) {
      return (
        <View style={tireServiceScreenStyles.subItemForService}>
          {this.renderServiceItemTemplate1(value)}
        </View>
      );
    } else {
      return <></>;
    }
  }

  renderServiceItemTemplate1(tblCategory: TblCategory) {
    if (!tblCategory.services) {
      return <></>;
    }

    return (
      <View style={tireServiceScreenStyles.t1}>
        {tblCategory.services.map((v1, index, array) => {
          if (index == 0) {
            return (
              <>
                <View style={tireServiceScreenStyles.t1row}>
                  {this.renderSubItemSingle(v1, tblCategory)}
                </View>
                <View style={tireServiceScreenStyles.space} />
              </>
            );
          }
          if (index % 2 == 0) {
            return <></>;
          }

          return (
            <View style={tireServiceScreenStyles.t1row2}>
              {index && this.renderSubItemSmallSingle(v1, tblCategory)}
              {index + 1 < array.length &&
                this.renderSubItemSmallSingle(array[index + 1], tblCategory)}
            </View>
          );
        })}
      </View>
    );
  }

  _selectCategory(value) {
    var toggle = value.selected ? true : false;
    var i = DataBean.serviceServices.findIndex(f => f == value);
    for (let j = 0; j < DataBean.serviceServices.length; j++) {
      DataBean.serviceServices[j].selected = false;
    }

    DataBean.serviceServices[i].selected = !toggle;
    DataBeanService.setState();
  }

  renderSubItemSingle(value: TblService, tblCategory) {
    return (
      <TouchableOpacity
        style={{
          height: _scale(100),
          backgroundColor: value.bgColor ? value.bgColor : '#111FD2CC',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => {
          this.checkItem(value, tblCategory);
        }}>
        {this.showServiceItemSingleHelper(value)}
      </TouchableOpacity>
    );
  }

  renderSubItemSmallSingle(value, tblCategory) {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={() => {
            this.checkItem(value, tblCategory);
          }}
          style={{
            height: _scale(20),
            flex: 1,
            marginBottom: 3,
            backgroundColor: value.bgColor ? value.bgColor : '#CC11D248',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {this.showServiceItemSingleHelper(value)}
        </TouchableOpacity>
      </View>
    );
  }

  showServiceItemSingleHelper(value: PartnersItems) {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{color: value.color ? value.color : 'white'}}>
          {value.itemComNameByPartner}
        </Text>
        {value.checked && (
          <Text
            style={{
              color: value.color ? value.color : 'white',
            }}>
            <FontAwesome name="check" size={_scale(25)} />
          </Text>
        )}
      </View>
    );
  }

  checkItem(value, tblCategory: TblCategory) {
    value.checked = value.checked ? false : true;
    tblCategory.selectedCount = tblCategory.services.filter(
      f => f.checked,
    ).length;
    DataBeanService.setState();
    this.getTotalFee();
  }

  showSelectedCount(value) {
    if (!value.selectedCount || value.selectedCount == 0) {
      return <></>;
    }

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Badge
          style={{backgroundColor: global_Colors.secondColor, color: 'white'}}>
          {value.selectedCount}
        </Badge>
      </View>
    );
  }
  showSelectedCountBackup(value) {
    if (!value.selectedCount || value.selectedCount == 0) {
      return <></>;
    }

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View
          style={{
            borderRadius: 50,
            padding: 2,
            marginLeft: 4,
            backgroundColor: 'orange',
          }}>
          <Text
            style={{
              color: 'white',
            }}>
            {value.selectedCount}
          </Text>
        </View>
      </View>
    );
  }

  okButton() {
    return (
      <View
        style={{
          position: 'absolute',
          bottom: 20,
          width: windowWidth,
          backgroundColor: 'white',
        }}>
        <View style={{flex: 1}}>
          <View style={{flex: 1}}>
            <DataTable>
              <DataTable.Row>
                <DataTable.Title>مجموع</DataTable.Title>
                <DataTable.Cell numeric>
                  {numberWithCommas(DataBean.serviceTotalFee)}
                  {DataBean.serviceNeedReview}
                </DataTable.Cell>
              </DataTable.Row>
            </DataTable>
          </View>
          <View style={{flex: 1}}>
            <Button
              icon="phone"
              mode="contained"
              onPress={() => {
                var anySelected =
                  DataBean.serviceServices.filter(
                    (s: TblCategory) =>
                      s.services.filter(sub => sub.checked).length > 0,
                  ).length > 0;

                if (!anySelected) {
                  alert('لطفا ابتدا خدمات مورد نیاز خود را انتخاب نمایید');
                  return;
                }

                var needPhotoTaken =
                  DataBean.serviceServices.filter(
                    (s: TblCategory) =>
                      s.services.filter(
                        sub => sub.isPriceNeedReview && sub.checked,
                      ).length > 0,
                  ).length > 0;

                _MyLog(needPhotoTaken);

                if (needPhotoTaken) {
                  DataBeanService.setCurrentTab(DataBean.currentTab + 1);
                } else {
                  DataBeanService.setCurrentTab(DataBean.currentTab + 2);
                }
              }}>
              درخواست سرویس
            </Button>
            {/*<Button
              onPress={() => {
                DataBeanService.toggleShowOnFlyMenu();
                DataBeanService.setShowModal(true);
              }}
              title="ارسال درخواست سرویس"
            />*/}
          </View>
        </View>
      </View>
    );
  }

  showSelectedPrice(value: TblCategory) {
    if (!value.selectedCount || value.selectedCount == 0) {
      return <></>;
    }

    var totalPrice = 0;
    var needReview;
    for (let i = 0; i < value.services.length; i++) {
      if (value.services[i].checked && !value.services[i].isPriceNeedReview) {
        totalPrice += value.services[i].price;
      } else if (
        value.services[i].checked &&
        value.services[i].isPriceNeedReview
      ) {
        needReview = '+نظر کارشناس';
      }
    }
    if (!totalPrice) {
      return <Text>{needReview}</Text>;
    }

    return (
      <Text>
        {numberWithCommas(totalPrice)}
        {needReview}
      </Text>
    );
  }

  theader() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <ServiceRequestStepsScreen />
        </View>
        {/*  {_renderTopHeader()}*/}
        {/*<View style={{flex: 1, height: _scale(10)}}>
          <OnFlyFilterScreen />
        </View>*/}
        <View
          style={{
            height: _scale(5),
            backgroundColor: global_Colors.secondColor,
            borderBottomRightRadius: 80,
            borderBottomLeftRadius: 80,
          }}>
          {/*
          <Chip>چه خدمتی از ما ساخته است ؟</Chip>
*/}
        </View>
      </View>
    );
  }

  getTotalFee() {
    var totalFee = 0;
    var needReview;

    for (let j = 0; j < DataBean.serviceServices.length; j++) {
      var value = DataBean.serviceServices[j];

      for (let i = 0; i < value.services.length; i++) {
        if (value.services[i].checked && !value.services[i].isPriceNeedReview) {
          totalFee += value.services[i].price;
        } else if (
          value.services[i].checked &&
          value.services[i].isPriceNeedReview
        ) {
          needReview = '+نظر کارشناس';
        }
      }
    }
    DataBeanService.setServiceTotalFee(totalFee);
    DataBeanService.setServiceNeedReview(needReview);
  }
}

const tireServiceScreenStyles = StyleSheet.create({
  space: {
    width: _scale(5),
  },
  footer: {
    position: 'absolute',
    bottom: 80,
  },
  t1Title: {
    color: 'white',
  },
  t1row: {
    flex: 1,
  },
  t1row2: {
    flex: 1,
    flexDirection: 'column',
  },
  t1row3: {
    flex: 1,
  },
  t1: {
    flex: 1,
    flexDirection: 'row',
    padding: _scale(15),
  },
  subItemForService: {
    height: 150,
    borderWidth: 0.5,
    marginRight: _scale(20),
    marginLeft: _scale(20),
    borderColor: '#e0dfdf',
  },
  t1BigRectangle: {
    height: _scale(100),
    backgroundColor: '#111FD2CC',
    justifyContent: 'center',
    alignItems: 'center',
  },
  t1smallRectangle: {
    height: _scale(20),
    flex: 1,
    marginBottom: 3,
    backgroundColor: '#CC11D248',
    justifyContent: 'center',
    alignItems: 'center',
  },
  t1smallRectangle2: {
    height: _scale(20),
    flex: 1,
    marginBottom: 3,
    backgroundColor: '#015bec',
    justifyContent: 'center',
    alignItems: 'center',
  },
  t1smallRectangle3: {
    height: _scale(20),
    marginBottom: 3,
    flex: 1,
    backgroundColor: '#B7005AEA',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
