import React from 'react';
import {
  Dimensions,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {DataTable, Button, Checkbox} from 'react-native-paper';

import {DataBean, DataBeanService} from '../Service/DataBean';
import {AbstractScreen} from './AbstractScreen';
import ServicesService from '../Service/ServicesService';
import {MyResponseType} from '../Service/Myglobal';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {_scale} from '../Helpers/SizeHelper';
import {OnFlyFilterScreen} from './OnFlyFilterScreen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {TblCategory} from '../Models/tblCategory';
import {TblService} from '../Models/tblCar';
import {Badge, Caption, Chip} from 'react-native-paper';
import {global_Colors} from '../Components/MyGlobal';
import {numberWithCommas} from './BasketPreviewBeforeBuyScreen';
import {PartnersItems} from '../Models/partnersItems';
import {Avatar} from 'react-native-paper';
import {UploadRequestServicePhotosModal} from './UploadRequestServicePhotosModal';
import {ServiceRequestStepsScreen} from './ServiceRequestStepsScreen';
import {_renderTopHeader} from './YourSpecificCarHomeScreen';
import {UploadImagesSubScreen} from './UploadImagesSubScreen';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import {List} from 'react-native-paper';

export class ServiceWorkersScreen extends AbstractScreen {
  star() {
    return (
      <View style={{flexDirection: 'row'}}>
        <FontAwesome name="star" size={_scale(15)} />
        <FontAwesome name="star" size={_scale(15)} />
        <FontAwesome name="star" size={_scale(15)} />
      </View>
    );
  }
  truck() {
    return <FontAwesome name="truck" size={_scale(25)} />;
  }

  phone() {
    return (
      <Badge>
        <FontAwesome name="phone" size={_scale(18)} />{' '}
      </Badge>
    );
  }

  basket() {
    return (
      <Badge>
        <FontAwesome name="wrench" size={_scale(18)} />
      </Badge>
    );
  }

  message() {
    return (
      <Badge>
        <FontAwesome name="message" size={_scale(18)} />
      </Badge>
    );
  }
  motorcycle() {
    return <FontAwesome name="motorcycle" size={_scale(25)} />;
  }
  render() {
    return (
      <>
        <List.Section>
          <List.Subheader style={{textAlign: 'right'}}>
            خدمات کاران خبر دار شده
          </List.Subheader>
          <List.Accordion
            title="لاستیک کاران برتر"
            description={numberWithCommas(15000025) + ' تومان '}
            left={() => <List.Icon icon={this.truck} />}
            right={() => <List.Icon icon={this.star} />}>
            {this.chooseItems()}
          </List.Accordion>

          <List.Accordion
            title="تعویض روغنی حمید"
            description={numberWithCommas(35000025) + ' تومان '}
            left={() => <List.Icon icon={this.motorcycle} />}
            right={() => <List.Icon icon={this.star} />}>
            {this.chooseItems()}
          </List.Accordion>
        </List.Section>
        <Button
          icon="phone"
          mode="contained"
          onPress={() => {
            DataBeanService.setCurrentTab(3);
          }}>
          درخواست سرویس
        </Button>
      </>
    );
  }

  chooseItems() {
    return (
      <>
        {/* <List.Item
          title="انتخاب"
          left={() => <List.Icon icon={this.message} />}
        />
*/}
        <View style={{flexDirection: 'row-reverse'}}>
          {this.renderSingleCheckBox('تعویض روغن')}
          {this.renderSingleCheckBox('بالانس')}
        </View>

        {/*
        <List.Item title="تماس" left={() => <List.Icon icon={this.phone} />} />
*/}
      </>
    );
  }

  renderSingleCheckBox(s, check) {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>{s}</Text>
        <Checkbox status={check ? 'checked' : 'unchecked'} onPress={() => {}} />
      </View>
    );
  }
}
