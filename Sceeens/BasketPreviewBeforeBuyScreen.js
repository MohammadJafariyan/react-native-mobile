// @flow
import React from 'react';
import {
  Button,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Alert,
  Image,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  Dimensions,
  RefreshControl,
} from 'react-native';
import {SettingSingleton} from '../Sceeens/NavigateManagerScreen';
import ServiceRelatedGoodsModal from '../Sceeens/ServiceRelatedGoodsModal';
import GreyDivider from '../Components/GreyDivider';
import {PurchaseItemService, PurchaseService} from '../Service/purchaseService';
import {PartnersItems} from '../Service/models';
import {DataBeanService, DataBean} from '../Service/DataBean';
import EmptyCart from '../Service/EmptyCart';
import {MyResponseType} from '../Service/Myglobal';
import {TokenService} from '../Service/tokenService';
import {MobileStorageService} from '../Service/MobileStorageService';
import {WebView} from 'react-native-webview';
import {getAndValidateProfile, ProfileScreen} from './ProfileScreen';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {AbstractScreen} from './AbstractScreen';

export function numberWithCommas(x) {
  if (!x) {
    return 0;
  }
  var y = Number(x);
  return y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export default class BasketPreviewBeforeBuyScreen extends AbstractScreen {
  constructor(props, state, context: *) {
    super(props, state, context);
    DataBeanService.setShowWebView(false);
  }

  componentDidMount(): void {
    // for test

    DataBeanService.refreshTotalFee();
    empty = DataBean.totalFee == 0;
    //console.log(empty);

    DataBeanService.setTotay();
  }
  bankPaymentCallback() {
    DataBeanService.retRefreshing(true);

    MobileStorageService._retrieveData('token')
      .then(tok => {
        DataBeanService.retRefreshing(false);
        _MyLog('token', tok);

        DataBeanService.retRefreshing(true);
        var ps = new PurchaseService();
        ps.getPurchaseStatus(DataBean.purchaseId, tok)
          .then(res => {
            DataBeanService.retRefreshing(false);

            if (res.type == MyResponseType.Success) {
              DataBeanService.setPaymentStatus(res);
              // پرداخت بانک موفق بوده است
              if (res.businessType == MyResponseType.Success) {
                SettingSingleton.navigator.navigate('BankPaymentSuccessScreen');
              } else {
                SettingSingleton.navigator.navigate('BankPaymentFailScreen');
              }
            } else {
              alert(res.message);
            }
          })
          .catch(e => {
            DataBeanService.retRefreshing(false);
            alert('خطا در اتصال به اینترنت');
          });
      })
      .catch(e => {
        DataBeanService.retRefreshing(false);
        alert(
          'خطا در دسترسی به توکن بایستی دوباره وارد شوید یا مجددا نصب نمایید',
        );
      });
  }

  render() {
    /*
    this.bankPaymentCallback();
*/

    return (
      <>
        {this.RenderWebView()}

        {!DataBean.showWebView && (
          <>
            <StatusBar barStyle="dark-content" />

            <SafeAreaView>
              <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                refreshControl={
                  <RefreshControl
                    enabled={false}
                    refreshing={DataBean.refreshing}
                  />
                }>
                {this.renderRedirectToBankAndSummary()}

                {this.renderFactor(DataBean.selectedItems)}

                <View style={{height: 250}} />
              </ScrollView>
            </SafeAreaView>
          </>
        )}
      </>
    );
  }

  buyBackup() {
    var surchaseService = new PurchaseService();

    var purchase = {};
    var d = new Date();
    purchase.PurchaseDate = `${d.getFullYear()}-${d.getMonth()}-${d.getDay()}T${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;

    //console.log('surchaseService.Save(purchase)============>', purchase);
    surchaseService
      .Save(purchase)
      .then(res => {
        /*console.log(
          'surchaseService\n' +
            '      .Save(purchase)\n' +
            '      .then(res => { \n',
          res,
        );*/
        if (res) {
          return res.json();
        } else {
          return res;
        }
      })
      .then(id => {
        // console.log('.then(id =>============>', id);
        var selectedItems = this.GetSelectedItems(id);

        /*console.log(
          'purchaseItemService.SaveAll(selectedItems)============>',
          selectedItems,
        );*/

        var purchaseItemService = new PurchaseItemService();
        purchaseItemService
          .SaveAll(selectedItems)
          .then(res => {
            SettingSingleton.navigator.navigate('AfterBuySuccessScreen');
          })
          .catch(e => {
            //  console.log('  .catch(e => {', e);
            alert('خطایی رخ داد');
            /*   alert(e);*!/*/
          });
      })
      .catch(e => {
        // console.log('  .catch(e => {', e);
        alert('خطایی رخ داد');
        /*   alert(e);*!/*/
      });
  }

  GetSelectedItems(id) {
    var arr = [];
    for (let i = 0; i < DataBean.selectedItems.length; i++) {
      var itemToPurchase: itemToPurchase = {};
      itemToPurchase.TblItemId = DataBean.selectedItems[i].tblItem.id;
      itemToPurchase.PurchaseId = id;
      arr.push(itemToPurchase);
    }
    return arr;
  }
  /* removeFromBasket(tmp) {
          var partneritem: PartnersItems = tmp;
          if (partneritem.isLeaf){
              Alert.alert('قابل حذف از سبد نیست', "این آیتم وابسته آیتم های دیگر است و نمی تواند حذف شود", [
                  {text: 'بله', onPress: () => {}},
              ]);
          }
          if (partneritem.relatedBasketPartnerItems){

          }

      }*/
  removeFromBasket(partneritem) {
    DataBeanService.pushOrPopToBasket(partneritem);
    DataBeanService.refreshTotalFee();

    // Removes the first element of fruits
  }

  backToTiresListScreen() {
    SettingSingleton.navigator.navigate('TblItemsListScreen');
  }

  showRelatedPartnerItemsInBasket(partneritem: PartnersItems) {
    return partneritem.relatedBasketPartnerItems.map((value, index, array) => {
      if (value.id == partneritem.id || !value.checked) {
        return <></>;
      }

      return this.getRelateView(value, partneritem);
    });
  }

  checkFromRelatedBasketPartnerItems(
    parent: PartnersItems,
    value: PartnersItems,
  ) {
    let selectedItems = [...DataBean.selectedItems];

    var index = selectedItems.findIndex(i => i.id == parent.id);

    var jindex = selectedItems[index].relatedBasketPartnerItems.findIndex(
      j => j.id == value.id,
    );

    for (
      let i = 0;
      i < selectedItems[index].relatedBasketPartnerItems.length;
      i++
    ) {
      selectedItems[index].relatedBasketPartnerItems[i].checked = false;
    }

    // انتخاب
    if (!selectedItems[index].relatedBasketPartnerItems[jindex].checked) {
      selectedItems[index].relatedBasketPartnerItems[jindex].checked = true;
    } else {
      selectedItems[index].relatedBasketPartnerItems[jindex].checked = !DataBean
        .selectedItems[index].relatedBasketPartnerItems[jindex].checked;
    }

    // پایان انتخاب
    DataBeanService.setSelectedItems(selectedItems);
    DataBeanService.refreshTotalFee();
  }

  renderCount(partneritem: PartnersItems) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'space-between',
          flexDirection: 'row-reverse',
        }}>
        <Text style={{color: '#fbb717'}}>تعداد :</Text>
        <Text
          style={{
            color: 'grey',
          }}>
          {partneritem.count ? partneritem.count : '0'}
        </Text>
      </View>
    );
  }

  decrease(partneritem: PartnersItems) {
    if (!partneritem.count) {
      return;
    }
    if (partneritem.count <= 1) {
      return;
    }
    partneritem.count--;

    partneritem.muliplyPrice = partneritem.price * partneritem.count;
    this.refreshItems(partneritem);
  }

  increase(partneritem: PartnersItems) {
    if (!partneritem.count) {
      partneritem.count = 0;
    }
    partneritem.count++;

    partneritem.muliplyPrice = partneritem.price * partneritem.count;
    this.refreshItems(partneritem);
  }

  refreshItems(partneritem: PartnersItems) {
    let selectedItems = [...DataBean.selectedItems];

    var index = selectedItems.findIndex(i => i.id == partneritem.id);

    selectedItems[index] = partneritem;

    DataBeanService.setSelectedItems(selectedItems);
    DataBeanService.refreshTotalFee();
  }

  getRelateView(value: PartnersItems, partneritem) {
    return (
      <View
        style={{
          flex: 1,
          borderRadius: 5,
          flexDirection: 'row',
          padding: 2,
          flexWrap: 'wrap',
        }}>
        <Text style={{flex: 1, flexWrap: 'wrap'}}>
          {value.itemComNameByPartner
            ? value.itemComNameByPartner
            : value.tblItem.name}
        </Text>
        <View />
      </View>
    );
  }

  showRelatedGoods(partneritem: PartnersItems) {
    if (!partneritem.relatedGoods || partneritem.relatedGoods.length <= 0) {
      return <></>;
    }

    return partneritem.relatedGoods.map((value, index, array) => {
      if (value.id == partneritem.id) {
        return <></>;
      }

      return this.getRelateGoodView(value, partneritem);
    });
  }

  getRelateGoodView(value, partneritem) {
    return (
      <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
        <View
          style={{
            borderRadius: 5,
            flexDirection: 'row-reverse',
            padding: 2,
            flexWrap: 'wrap',
          }}>
          <Text
            style={{
              flexWrap: 'wrap',
            }}>
            {value.itemComNameByPartner
              ? value.itemComNameByPartner
              : value.tblItem.name}
          </Text>

          {value.partner.id != partneritem.partner.id && (
            <Text>({value.partner.name})</Text>
          )}
          {/*   <View>
          {partneritem.relatedBasketPartnerItems &&
            partneritem.relatedBasketPartnerItems.length > 2 &&
            value.checked && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}
          {partneritem.relatedBasketPartnerItems &&
            partneritem.relatedBasketPartnerItems.length == 2 && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}
        </View>*/}
        </View>
      </View>
    );
  }

  buyPreview() {
    SettingSingleton.navigator.navigate('BasketPreviewBeforeBuyScreen');
  }

  getUnitPrise(partneritem: PartnersItems) {
    var p1 = partneritem.price;

    if (partneritem.relatedBasketPartnerItems) {
      var p2 = 0;
      for (let i = 0; i < partneritem.relatedBasketPartnerItems.length; i++) {
        if (partneritem.relatedBasketPartnerItems[i].checked) {
          p2 += partneritem.relatedBasketPartnerItems[i].price;
        }
      }
      p1 += p2;
    }

    if (partneritem.relatedGoods) {
      var p3 = 0;
      for (let i = 0; i < partneritem.relatedGoods.length; i++) {
        if (partneritem.relatedGoods[i].checked) {
          p3 += partneritem.relatedGoods[i].price;
        }
      }
      p1 += p3;
    }

    return p1;
  }

  buyInPreview() {
    DataBeanService.retRefreshing(true);
    var surchaseService = new PurchaseService();
    MobileStorageService._retrieveData('token').then(tok => {
      var token = tok;

      var arr = surchaseService.Extract(DataBean.selectedItems);
      console.log('arr sent ============>', arr);
      surchaseService.Purchase(DataBean.totalFee, arr, token).then(res => {
        DataBeanService.retRefreshing(false);
        console.log(
          'surchaseService\n' +
            '      .Save(purchase)\n' +
            '      .then(res => { \n',
          res,
        );
        debugger;
        if (res.type == MyResponseType.Success) {
          DataBeanService.setPurchaseId(res.single);
          DataBeanService.setShowWebView(true);
        } else if (
          res.type == MyResponseType.Fail ||
          res.type == MyResponseType.FakeFail
        ) {
        }
      });

      // SettingSingleton.navigator.navigate('AfterBuySuccessScreen');
    });
  }
  validateProfileBeforeBuy() {
    DataBeanService.retRefreshing(true);
    // متد در دوجا استفاده شده است و موقع تغیر باید بسیار مراقب بود
    getAndValidateProfile(() => {
      DataBeanService.retRefreshing(false);
      // پروفایل او ناقص است فقط با این پیغام که از سرور می آید می فهمیم
      if (DataBean.systemMessage) {
        Alert.alert(
          'پیغام ',
          'پروفایل شما کامل نیست بنابرین برای خرید ابتدا باید اطلاعات خود را کامل ثبت نمایید',
          [
            {
              text: 'خیر',
              onPress: () => console.log('NO Pressed'),
              style: 'cancel',
            },
            {
              text: 'تکمیل پروفایل',
              onPress: () =>
                SettingSingleton.navigator.navigate('ProfileScreen'),
            },
            //  {text: 'بله', onPress: () => this.addToBasketWithRelatedItems(item)},
          ],
        );
      }
      // پروفایل کاربر ناقص است زیرا پیغام برگشتی از سرور خطا دارد
      else {
        this.buyInPreview();
      }
    });
  }

  showMultyplyPrise(partneritem: PartnersItems) {
    var p1 = this.getUnitPrise(partneritem);
    var plast = p1 * partneritem.count;

    return <Text> {numberWithCommas(plast)} تومان </Text>;
  }
  showUnitPrise(partneritem: PartnersItems) {
    var p1 = this.getUnitPrise(partneritem);
    return <Text> {numberWithCommas(p1)} تومان </Text>;
  }

  RenderWebView() {
    return (
      DataBean.showWebView && (
        <View style={{flex: 1, backgroundColor: 'orange'}}>
          <Button
            title={'بستن'}
            onPress={() => {
              DataBeanService.setShowWebView(false);
              this.bankPaymentCallback();
            }}
          />
          <WebView
            onNavigationStateChange={e => {
              if (e.url.indexOf('end_of_buy') > -1) {
                DataBeanService.setShowWebView(false);

                this.bankPaymentCallback();
              }
              /** put your comdition here based here and close webview.
                         Like if(e.url.indexOf("end_url") > -1)
                         Then close webview
                         */
            }}
            source={{
              uri:
                'http://www.kachar-eshop.ir/bank/RedirectToBank?purchaseId=' +
                DataBean.purchaseId,
            }}
            style={{flex: 1, height: windowHeight, width: windowWidth}}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}
            scrollEnabled={true}
          />
        </View>
      )
    );
  }
  renderFactor(items) {
    return <FactorScreen items={items} />;
  }

  renderRedirectToBankAndSummary() {
    return (
      <View
        style={{
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 5,
        }}>
        <View>
          <Text style={{marginLeft: 10}}>
            {' '}
            مجموع {numberWithCommas(DataBean.totalFee)} تومان{' '}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            alignItems: 'flex-end',
            margin: 5,
          }}>
          <Button
            title="هدایت به صفحه بانک و پرداخت"
            onPress={() => this.validateProfileBeforeBuy()}
          />
          {/*  {DataBean.purchaseId &&
                <Button
                    title="برگشت از خرید"
                    onPress={() => this.bankPaymentCallback()}
                />}*/}
        </View>
        <View
          style={{
            margin: 5,
          }}
        />
      </View>
    );
  }
}

export class FactorScreen extends BasketPreviewBeforeBuyScreen {
  render() {
    return this.renderFactor(this.props.items);
  }
  renderFactor(items) {
    return (
      <View
        style={{
          flex: 9,
        }}>
        {items.map((partneritem: PartnersItems, i) => {
          return (
            <ScrollView>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row-reverse',
                  flexWrap: 'wrap',
                  padding: 5,
                  marginBottom: 5,
                  justifyContent: 'space-around',
                  borderWidth: 1,
                  borderRadius: 15,
                  borderColor: '#808285',
                  marginRight: 20,
                  marginLeft: 20,
                }}>
                <View style={styles.cartProductInfoViewParent}>
                  <View style={styles.cartProductInfoView}>
                    <Text style={{textAlign: 'left'}}>
                      فروشگاه : {partneritem.partner.name}
                    </Text>

                    <Text>{DataBean.todayDate}</Text>
                  </View>
                  <View style={styles.cartProductInfoView_WithLine}>
                    <Text style={{textAlign: 'left'}}>
                      آدرس :{partneritem.partner.city.name} -{' '}
                      {partneritem.partner.adress}
                    </Text>

                    <Text />
                  </View>
                  <View style={{flexDirection: 'row-reverse'}}>
                    <Text>
                      {partneritem.itemComNameByPartner
                        ? partneritem.itemComNameByPartner
                        : partneritem.tblItem.name}
                    </Text>
                  </View>
                  <View
                    style={{
                      marginBottom: 10,
                      borderBottomWidth: 0.5,
                    }}>
                    {partneritem.iHaveGoodsConfirmed && (
                      <View
                        style={{
                          justifyContent: 'flex-start',
                          flexDirection: 'row-reverse',
                        }}>
                        <View
                          style={{
                            padding: 5,
                          }}>
                          <Text
                            style={{
                              textAlign: 'left',

                              marginRight: 5,
                            }}>
                            بهمراه :
                          </Text>
                          <Text>اقلام مورد نیاز این سرویس را دارم</Text>
                        </View>
                      </View>
                    )}

                    {partneritem.isMainBasketItem &&
                      partneritem.relatedBasketPartnerItems && (
                        <View
                          style={{
                            marginTop: 10,
                            justifyContent: 'flex-start',
                            flexDirection: 'row-reverse',
                          }}>
                          <Text
                            style={{
                              textAlign: 'left',

                              marginRight: 5,
                            }}>
                            بهمراه :
                          </Text>
                          {this.showRelatedPartnerItemsInBasket(partneritem)}
                        </View>
                      )}

                    {partneritem.relatedGoods && (
                      <View
                        style={{
                          marginTop: 10,
                          justifyContent: 'flex-start',
                          flexDirection: 'row-reverse',
                        }}>
                        <Text
                          style={{
                            textAlign: 'left',

                            marginRight: 5,
                          }}>
                          بهمراه :
                        </Text>
                        {this.showRelatedGoods(partneritem)}
                      </View>
                    )}
                  </View>

                  <View style={styles.cartProductInfoView}>
                    <Text
                      style={{
                        textAlign: 'right',
                      }}>
                      {' '}
                      قیمت واحد:{' '}
                    </Text>
                    <Text
                      style={{
                        textAlign: 'right',
                      }}
                    />
                    <View style={{textAlign: 'left'}}>
                      {this.showUnitPrise(partneritem)}
                    </View>
                  </View>
                  <View style={styles.cartProductInfoView}>
                    <Text
                      style={{
                        textAlign: 'right',
                      }}>
                      {' '}
                      تعداد :{' '}
                    </Text>
                    <Text
                      style={{
                        textAlign: 'right',
                      }}
                    />
                    <Text style={{textAlign: 'left'}}>
                      {partneritem.count} عدد
                    </Text>
                  </View>
                  <View style={styles.cartProductInfoView}>
                    <Text
                      style={{
                        textAlign: 'right',
                      }}>
                      {' '}
                      قیمت کل:{' '}
                    </Text>
                    <Text
                      style={{
                        textAlign: 'right',
                      }}
                    />
                    <View style={{textAlign: 'left'}}>
                      {this.showMultyplyPrise(partneritem)}
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          );
        })}
      </View>
    );
  }
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  cartTireImageView: {
    borderColor: '#fbb717',
    borderWidth: 4,
    borderRadius: 3,
    width: 140,
    height: 140,
  },

  cartTireImage: {
    width: '90%',
    height: '90%',
    margin: '5%',
  },

  cartTireName: {
    fontSize: 25,
    textAlign: 'right',
    marginRight: windowWidth * 0.05,
    marginTop: 10,
  },

  cartProductInfoView: {
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  footer: {
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  cartProductInfoView_WithLine: {
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginBottom: 5,
    borderBottomWidth: 0.5,
  },

  cartProductInfoViewParent: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 5,
    paddingRight: 5,
  },
});
