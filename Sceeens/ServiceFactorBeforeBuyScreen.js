import React, {Component, useState} from 'react';
import {
  Alert,
  Modal,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableHighlight,
  View,
} from 'react-native';
import {AbstractScreen} from './AbstractScreen';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {Divider, Text} from 'react-native-paper';
import {TblCategory} from '../Models/tblCategory';
import {PartnersItems} from '../Models/partnersItems';

import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';
import {DataTable} from 'react-native-paper';
import {numberWithCommas} from './BasketPreviewBeforeBuyScreen';

export class ServiceFactorBeforeBuyScreen extends AbstractScreen {
  render() {
    return (
      <>
        <DataTable>
          <DataTable.Header>
            <DataTable.Title>عنوان</DataTable.Title>
            <DataTable.Title>خدمات کار</DataTable.Title>
            <DataTable.Title numeric>مبلغ</DataTable.Title>
          </DataTable.Header>

          <DataTable.Row>
            <DataTable.Cell>تعویض لاستیک</DataTable.Cell>
            <DataTable.Cell numeric>لاستیک کاران</DataTable.Cell>
            <DataTable.Cell numeric>
              {numberWithCommas(15000000)}
            </DataTable.Cell>
          </DataTable.Row>

          <DataTable.Row>
            <DataTable.Cell>تعویض روغن</DataTable.Cell>
            <DataTable.Cell numeric>تعویض روغنی حمید</DataTable.Cell>
            <DataTable.Cell numeric>{numberWithCommas(600000)}</DataTable.Cell>
          </DataTable.Row>

          <DataTable.Row>
            <DataTable.Cell>جمع </DataTable.Cell>
            <DataTable.Cell numeric>
              {numberWithCommas(15000000 + 600000)}
            </DataTable.Cell>
          </DataTable.Row>

          {/* <DataTable.Pagination
          page={1}
          numberOfPages={3}
          onPageChange={page => {
            console.log(page);
          }}
          label="1-2 of 6"
        />*/}
        </DataTable>
        <Button
          icon="credit-card"
          mode="contained"
          onPress={() => {
            DataBeanService.setCurrentTab(3);
          }}>
          پرداخت
        </Button>
      </>
    );
  }
}
