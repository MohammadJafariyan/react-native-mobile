import React from 'react';
import { Dimensions, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import BottomNavigationBar from '../Components/BottomBar';
import {DataBean} from '../Service/DataBean';
import search from '../assets/img/search.png';
import GeneralStatusBarColor from '../Zaman/GeneralStatusBarColor';
import {_scale} from '../Helpers/SizeHelper';
import CategoryScreen from './CategoryScreen';
import {ActiveComponent} from '../Service/models';


export class BottomNavigationWithoutSearchBox extends React.Component {

  constructor(state: {text: string, externalUse: boolean}) {
    super();
  }

  render(){

    return (
      <>
        
        <View style={{flex:1, marginBottom: _scale(60)}}>
          {this.props.children}
        </View>
        <BottomNavigationBar />
      
      </>
     
    );

   }


}

const windowWidth = Dimensions.get('window').width;

const style = StyleSheet.create({

  iconsTouchableOpacity: {
    width: 55,
    height: 55,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },

  searchBoxImage: {
    height: windowWidth*.1,
    width: windowWidth*.1,
  },

  searchBoxViewL: {
    backgroundColor: '#cfcfcf',
    flexDirection:'row',
    justifyContent:'flex-end',
    alignItems:'center',
    color: '#303030',
    height: 50,
    fontSize: 15,
    padding: 10,
    borderRadius: 4,
    width: "90%",
    marginHorizontal: "5%",
    marginBottom: 10,
  },

  searchBox: {
    fontSize: 17,
    paddingRight: 10,
    textAlign: 'right',
    width: windowWidth*.75 - 88,
    height: 30,
    borderLeftColor: "white",
    borderLeftWidth: 2
  },

  cityName: {
    fontSize: 20, 
    color: "grey", 
    textAlign: "center", 
    width: "95%"
  }
});

