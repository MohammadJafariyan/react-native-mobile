import {AbstractScreen} from './AbstractScreen';
// @flow
import React from 'react';

import {
  Text,
  View,
  Image,
  Button,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  SafeAreaView,
  BackHandler,
} from 'react-native';
import {ZZTitle} from '../Components/ZZTitle';
import {PurchaseService} from '../Service/purchaseService';
import {MyResponseType} from '../Service/Myglobal';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {AlreadyPurchasedViewModel} from '../Service/models';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {SettingSingleton} from './NavigateManagerScreen';

export class MyPurchasesScreen extends AbstractScreen {
  componentDidMount() {
    DataBeanService.retRefreshing(true);
    var s = new PurchaseService();
    s.GetMyPurchases()
      .then(res => {
        DataBeanService.retRefreshing(false);
        _MyLog('MyPurchasesScreen==>s.GetMyPurchases()', res);
        if (res.type == MyResponseType.Success) {
          DataBeanService.setAlreadyPurchasedPartnerItems(res.list);
        } else {
          alert(res.message);
        }
      })
      .catch(e => {
        DataBeanService.retRefreshing(false);
        alert('خطا در اتصال به اینترنت');
      });
  }
  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={
            <RefreshControl enabled={false} refreshing={DataBean.refreshing} />
          }>
          {this.renderBody()}
          <View style={{height: 180}} />
        </ScrollView>
      </SafeAreaView>
    );
  }

  renderBody() {
    return (
      <View>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text>خرید های من</Text>
        </View>
        <View>
          <View style={{marginBottom: 15}}>
            <ZZTitle title="خرید های موفق" />
          </View>

          <View style={_styles.listHolder}>{this.showSucceedPurchases()}</View>
        </View>
        <View>
          <View style={{marginBottom: 15, marginTop: 15}}>
            <ZZTitle title="تلاش های ناموفق" />
          </View>

          <View style={_styles.listHolder}>{this.showFailedPurchases()}</View>
        </View>
      </View>
    );
  }

  showSucceedPurchases() {
    if (
      DataBean.alreadyPurchasedPartnerItems &&
      DataBean.alreadyPurchasedPartnerItems.successPurchased &&
      DataBean.alreadyPurchasedPartnerItems.successPurchased.length
    ) {
    } else {
      return <></>;
    }

    return DataBean.alreadyPurchasedPartnerItems.successPurchased.map(
      (value, index, array) => {
        if (index == 0) {
          return this.showHeaders(value);
        } else {
          return this.showSinglePurchase(value);
        }
      },
    );
  }

  showHeaders(value) {
    return (
      <View
        style={{
          borderWidth: 1,
          borderColor: '#c4c3c3',
          flexDirection: 'row-reverse',
          justifyContent: 'space-around',
          padding: 10,
        }}>
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>توضیحات</Text>
        </View>
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>وضعیت </Text>
        </View>
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text> جمع قیمت </Text>
        </View>
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>تاریخ</Text>
        </View>
      </View>
    );
  }

  showSinglePurchase(value: AlreadyPurchasedViewModel) {
    return (
      <TouchableOpacity
        style={{
          borderWidth: 1,
          borderColor: '#c4c3c3',
          flexDirection: 'row-reverse',
          justifyContent: 'space-around',
          padding: 10,
          marginTop: 2,
        }}
        onPress={() => {
          DataBeanService.setSelectedAlreadyPurchyasedViewModel(value);
          SettingSingleton.navigator.navigate('MyPurchaseDetailScreen');
        }}>
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{border: 5}}>{value.title}</Text>
        </View>
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{value.paymentStatusText}</Text>
        </View>
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{value.totalFee}</Text>
        </View>
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{value.date}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  showFailedPurchases() {
    if (
      DataBean.alreadyPurchasedPartnerItems &&
      DataBean.alreadyPurchasedPartnerItems.failPurchased &&
      DataBean.alreadyPurchasedPartnerItems.failPurchased.length
    ) {
    } else {
      return <></>;
    }

    return DataBean.alreadyPurchasedPartnerItems.failPurchased.map(
      (value, index, array) => {
        if (index == 0) {
          return this.showHeaders(value);
        } else {
          return this.showSinglePurchase(value);
        }
      },
    );
  }
}

const _styles = StyleSheet.create({
  listHolder: {
    paddingRight: 20,
    paddingLeft: 20,
  },
});
