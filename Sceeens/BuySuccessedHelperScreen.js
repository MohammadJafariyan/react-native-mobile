import React from 'react';
import ServiceRelatedGoodsModal from './ServiceRelatedGoodsModal';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {SettingSingleton} from './NavigateManagerScreen';
import LinearGradient from 'react-native-linear-gradient';
import {gradientStyles} from './CodeScreen';
import {
  Button,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Alert,
  Image,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import {AbstractScreen} from "./AbstractScreen";

export class BuySuccessedHelperScreen extends AbstractScreen {


  constructor(props, state, context: *) {
    super(props, state, context);
    DataBeanService.setCallback(this.callbackfu);

  }

  render() {
    return (
      <View>
        <LinearGradient
          style={gradientStyles.linearBG}
          colors={['#fdbb13', '#fbe70f', '#fdbb13']}>
          <ServiceRelatedGoodsModal />
        </LinearGradient>
      </View>
    );
  }
  callbackfu() {
    SettingSingleton.navigator.navigate('BuySuccessedScreen');
  }
}
