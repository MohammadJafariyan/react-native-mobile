import React from 'react';

import {RefreshControl, SafeAreaView, ScrollView, Text, View,} from 'react-native';
import {AbstractScreen} from "./AbstractScreen";
import {DataBean} from "../Service/DataBean";
import {FactorScreen} from "./BasketPreviewBeforeBuyScreen";


export class MyPurchaseDetailScreen extends AbstractScreen {

    constructor(props, state, context: *) {
        super(props, state, context);

    }

    render() {
        return (
            <SafeAreaView>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    refreshControl={<RefreshControl enabled={false} refreshing={DataBean.refreshing}/>}>
                    {this.renderBody()}
                </ScrollView>
            </SafeAreaView>
        );
    }

    renderBody() {
        return <View>
            <View style={{flexDirection:'row' , justifyContent:'space-around'}}>
                <Text>99/05/25</Text>
                <Text>جزئیات خرید تاریخ : </Text>
            </View>
            <FactorScreen items={DataBean.selectedAlreadyPurchyasedViewModel.partnerItems}></FactorScreen>

            <View style={{height: 180}}></View>
        </View>
    }
}
