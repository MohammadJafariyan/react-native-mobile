import React from 'react';
import {DataBean, DataBeanService} from "../Service/DataBean";
import {_MyLog} from "../MyGlobal/ApiUrls";


export class AbstractScreen extends  React.Component{


    constructor(props,state, context: any) {
        super(props, context);
        this.state = state;
        DataBean.currentScreen = this;
        this.state = DataBean;

        _MyLog('activate',this.constructor.name);
        DataBeanService.pushToActiveComponents({
            compnent:this,
            componentName:this.constructor.name
        });

    }


    componentWillUnmount() {
        DataBeanService.popActiveComponent(this.constructor.name)
    }
}
