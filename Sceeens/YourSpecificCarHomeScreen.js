import _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, Image, ScrollView, Dimensions} from 'react-native';
import {Colors, Typography, View, TabBar, Text} from 'react-native-ui-lib';
import {global_Colors} from '../Components/MyGlobal';
import {ZZTitle} from '../Components/ZZTitle';

import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  Headline,
  Badge,
} from 'react-native-paper';
import {_scale} from '../Helpers/SizeHelper';
import {_getSupportIcon} from './OnTheFlyCategoryScreen';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const labelsArray = [
  [
    'ONE TWO',
    'THREE',
    'THREEEEEEEE',
    'FOUR',
    'FIVE FIVE',
    'SIX',
    'SEVEN-ELEVEN',
  ],
  [
    'ONE TWO',
    'THREE',
    'THREEEEEEEE',
    'FOUR',
    'FIVE FIVE',
    'SIX',
    'SEVEN-ELEVEN',
  ],
  [
    'SEVEN-ELEVEN',
    'ONE TWO',
    'THREE',
    'THREEEEEEEE',
    'FOUR',
    'FIVE FIVE',
    'SIX',
  ],
  [
    'SIX',
    'ONE TWO',
    'THREE',
    'THREEEEEEEE',
    'FOUR',
    'FIVE FIVE',
    'SEVEN-ELEVEN',
  ],
  [
    'FIVE FIVE',
    'ONE TWO',
    'THREE',
    'THREEEEEEEE',
    'FOUR',
    'SIX',
    'SEVEN-ELEVEN',
  ],
];
const ADD_ITEM_ICON = require('../assets/img/roadstone.jpg');
const themeColors = [
  Colors.violet30,
  Colors.green30,
  Colors.red30,
  Colors.blue30,
  Colors.yellow30,
];

export function _renderTopHeader() {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/*<Image
        source={require('../assets/img/405.png')}
        style={{flex: 1, width: 100, height: 110}}
      />*/}
      <Card style={{flex: 1}}>
        <Card.Content>
          <Title style={{textAlign: 'right', fontWeight: 'bold'}}>تایر</Title>
          <Paragraph style={{textAlign: 'right', fontWeight: 'bold'}}>
            مخصوص خودروی
          </Paragraph>
          <Badge size={_scale(31)}> پژو 405</Badge>
        </Card.Content>
      </Card>
    </View>
  );
}

export class YourSpecificCarHomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: 1,
      selectedIndex1: 1,
      labels: labelsArray[0],
      currentTabs: [],
      themeColor: themeColors[0],
    };

    this.counter = 0;
    this.colorCounter = 0;
  }
  /*enableShadow bg-dark80*/
  renderMyTabs() {
    return (
      <ScrollView
        style={{overflow: 'visible'}}
        showsVerticalScrollIndicator={false}>
        <View flex>
          <TabBar
            style={styles.tabbar}
            selectedIndex={0}
            ref={r => (this.tabbar = r)}
            indicatorStyle={{backgroundColor: global_Colors.secondColor}}>
            <TabBar.Item
              label="تایر"
              badge={{size: 'pimpleSmall'}}
              icon={_getSupportIcon}
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="روغن موتور"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              icon="home"
              label="رینگ"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="باتری"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="فیلتر های خودرو"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="لوازم یدکی"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="روغن ترمز"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="روغن هیدرولیک"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="روغن دنده"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="واکس خودرو"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="مکمل ها"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
            <TabBar.Item
              label="قطعات خودرو"
              selectedLabelStyle={{color: global_Colors.secondColor}}
            />
          </TabBar>
        </View>
      </ScrollView>
    );
  }
  /** Index change */
  changeIndex = () => {
    let index;

    do {
      index = Math.floor(Math.random() * this.tabbar.props.children.length);
    } while (index === this.state.selectedIndex);

    this.setState({selectedIndex: index});
  };

  /** Labels change */
  count() {
    if (this.counter < labelsArray.length - 1) {
      this.counter++;
    } else {
      this.counter = 0;
    }
  }

  changeLabels = () => {
    this.count();
    this.setState({labels: labelsArray[this.counter]});
  };

  /** Colors change */
  countColors() {
    if (this.colorCounter < themeColors.length - 1) {
      this.colorCounter++;
    } else {
      this.colorCounter = 0;
    }
  }

  changeColors = () => {
    this.countColors();
    this.setState({themeColor: themeColors[this.colorCounter]});
  };

  /** Children Count change */
  addTab = () => {
    const random = Math.floor(Math.random() * 100000);
    const newTabs = this.state.currentTabs;

    newTabs.push({
      id: random,
      displayLabel: `tab #${this.state.currentTabs.length}`,
    });
    this.setState({currentTabs: newTabs});
  };

  removeTab = () => {
    const index = this.state.selectedIndex;
    const newTabs = this.state.currentTabs;

    if (newTabs.length >= 0) {
      newTabs.splice(index, 1);
    }
    this.setState({currentTabs: newTabs});
  };

  /** Actions */
  getTabs(showAddTab) {
    const tabs = _.map(this.state.currentTabs, tab => this.renderTabs(tab));

    if (showAddTab) {
      tabs.push(this.renderAddTabsTab());
    } else {
      tabs.push(
        this.renderTabs({
          id: this.state.currentTabs.length,
          displayLabel: `tab #${this.state.currentTabs.length}`,
        }),
      );
    }
    return tabs;
  }

  /** Renders */
  renderTabs(tab) {
    return (
      <TabBar.Item
        key={tab.id}
        label={tab.displayLabel}
        onPress={tab.onPress}
      />
    );
  }

  renderAddTabsTab() {
    return (
      <TabBar.Item
        key={'ADD_TABS'}
        label={this.state.currentTabs.length >= 2 ? undefined : 'ADD TABS'}
        width={this.state.currentTabs.length >= 2 ? 48 : undefined}
        onPress={this.addTab}
        icon={ADD_ITEM_ICON}
        ignore
      />
    );
  }

  render() {
    return (
      <View style={{backgroundColor: 'white'}}>
        <View>{_renderTopHeader()}</View>

        {this.renderMyTabs()}
        {this.showMainScrollProducts()}

        <View style={{marginTop: 40}}>
          <ZZTitle title="پر طرفدارترین ها" />
        </View>

        {this.showRelatedScrollProducts()}

        <View style={{height: 50}} />
      </View>
    );
  }

  /*style={{textAlign: 'right'}}*/

  showMainScrollProducts() {
    return (
      <ScrollView
        style={{overflow: 'visible', flexDirection: 'row', marginTop: 10}}
        horizontal={true}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        {this.renderProducts()}
        {this.renderProducts()}
        {this.renderProducts()}
        {this.renderProducts()}
      </ScrollView>
    );
  }

  renderProducts() {
    return this.renderSingleProduct();
  }

  renderSingleProduct() {
    return (
      <View style={styles.singleProduct}>
        <Card>
          <Card.Cover
            source={require('../assets/img/roadstone.jpg')}
            style={{height: windowHeight * 0.4, resizeMode: 'cover'}}
          />
          {/* <Card.Content>
            <Title>Card title</Title>
            <Paragraph>Card content</Paragraph>
          </Card.Content>*/}
          {/* <Card.Actions>
            <Button>Cancel</Button>
            <Button>Ok</Button>
          </Card.Actions>*/}
        </Card>
      </View>
    );
  }

  showRelatedScrollProducts(text) {
    return this.renderSingleRelateProduct();
  }

  renderSingleRelateProduct() {
    return (
      <ScrollView
        style={{overflow: 'visible', flexDirection: 'row', marginTop: 10}}
        horizontal={true}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
      </ScrollView>
    );
  }

  showRelateSingleProduct() {
    return (
      <View style={styles.singleRelateProduct}>
        <Card>
          <Card.Cover
            source={require('../assets/img/roadstone.jpg')}
            style={{height: windowHeight * 0.2, resizeMode: 'contain'}}
          />
          {/* <Card.Content>
            <Title>Card title</Title>
            <Paragraph>Card content</Paragraph>
          </Card.Content>*/}
          {/* <Card.Actions>
            <Button>Cancel</Button>
            <Button>Ok</Button>
          </Card.Actions>*/}
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabbar: {
    marginVertical: 10,
  },
  mainProducts: {
    height: 60,
  },
  singleProduct: {
    marginRight: 15,
    marginLeft: 15,
    width: windowWidth * 0.6,
  },
  singleRelateProduct: {
    marginRight: 5,
    marginLeft: 5,
    width: windowWidth * 0.3,
  },
});
