import React, {Component, useState} from 'react';
import {
  Alert,
  Modal,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableHighlight,
  View,
} from 'react-native';
import {AbstractScreen} from './AbstractScreen';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {Divider, Text} from 'react-native-paper';
import {TblCategory} from '../Models/tblCategory';
import {PartnersItems} from '../Models/partnersItems';

import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';

const LeftContent = props => <Avatar.Icon {...props} icon="folder" />;
export class UploadRequestServicePhotosModal extends AbstractScreen {
  _showModal = () => this.setState({visible: true});
  _hideModal = () => this.setState({visible: false});

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={true}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            {this.showSelectedServicesForUpload()}
            <ScrollView contentInsetAdjustmentBehavior="automatic">
              <View style={styles.content}>
                <Text style={styles.modalText}>Hello World!</Text>
                <Card>
                  <Card.Content>
                    <Title>Card title</Title>
                    <Paragraph>Card content</Paragraph>
                  </Card.Content>
                  <Card.Cover source={{uri: 'https://picsum.photos/700'}} />
                  <Card.Actions>
                    <Button>x</Button>
                  </Card.Actions>
                </Card>
                <TouchableHighlight
                  style={{...styles.openButton, backgroundColor: '#2196F3'}}
                  onPress={() => {
                    DataBeanService.setShowModal(false);
                  }}>
                  <Text style={styles.textStyle}>Hide Modal</Text>
                </TouchableHighlight>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }

  showSelectedServicesForUpload() {
    if (!DataBean.serviceServices || DataBean.serviceServices.length == 0) {
      return <></>;
    }

    return DataBean.serviceServices.map((value, index, array) => {
      return value.services.map((item, j, arr) => {
        if (item.checked) {
          return this.showEverySelectedServiceForUpload(value, item);
        } else {
          return <></>;
        }
      });
    });
  }

  showEverySelectedServiceForUpload(
    category: TblCategory,
    item: PartnersItems,
  ) {
    return (
      <Card>
        <Card.Title
          title="Card Title"
          subtitle="Card Subtitle"
          left={LeftContent}
        />
        <Card.Content>
          <Title>Card title</Title>
          <Paragraph>Card content</Paragraph>
        </Card.Content>
        <Card.Cover source={{uri: 'https://picsum.photos/700'}} />
        <Card.Actions>
          <Button>Cancel</Button>
          <Button>Ok</Button>
        </Card.Actions>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '100%',
    height: '90%',
    margin: 20,
    borderRadius: 20,
    alignItems: 'center',
    shadowColor: '#000',
    backgroundColor: 'white',

    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  content: {
    flex: 1,
  },
});
