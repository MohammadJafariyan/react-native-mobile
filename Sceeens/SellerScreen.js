import {
  Text,
  View,
  Button,
  Image,
  StatusBar,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  RefreshControl,
  Dimensions,
} from 'react-native';
import React from 'react';
import {SettingSingleton} from './NavigateManagerScreen';
import SellerService from '../Service/SellerService';
import {MyResponse, MyResponseType} from '../Service/Myglobal';
import {Media, Partner} from '../Service/models';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import {showFixTypes} from './TiresListScreen';
import {ZZTitle} from '../Components/ZZTitle';
import GrayDivider from '../Components/GreyDivider';
import {AbstractScreen} from './AbstractScreen';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class SellerScreen extends AbstractScreen {
  constructor(state: {text: string}) {
    super();
  }

  componentDidMount(): void {
    // دیتای ذخیره شده در خود موبایل را برمیگرداند
    this._retrieveData().then(res => {
      if (res.type == MyResponseType.Success) {
        /*console.log(
          '=====================partner====================',
          res.single,
        );*/

        DataBeanService.setPartner(res.single);
      } else {
        alert(res.message);
      }
    });

    this._retriveImagesData().then(res => {
      console.log('_retriveImagesData');
      console.log(res);
      if (res.type == MyResponseType.Success) {
        res.list.map(v => {
          v.name = ApiUrls.getImageUrl(v.name);
        });
        DataBeanService.setPartnerImages(res.list);
      } else {
        alert(res.message);
      }
    });
  }

  _retrieveData(): Promise<MyResponse<Partner>> {
    var s = new SellerService();
    return s.getById(DataBean.partner.id);
  }

  _retriveImagesData(): Promise<MyResponse<Media[]>> {
    var s = new SellerService();
    return s.getImagesByPartnerId(DataBean.partner.id);
  }

  render() {
    // todo:در این متد صفحه اصلی اسکرین نمایش داده می شود
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView contentInsetAdjustmentBehavior="automatic">
            {this.renbody()}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
  renbody() {
    return (
      <View style={{flex: 1, paddingRight: 5, paddingLeft: 5}}>
        <View style={{flex: 1}}>
          <Button title="بازگشت" onPress={() => this.Back()} />
        </View>

        <View style={{flex: 9}}>
          <View style={{alignItems: 'center'}}>
            <Text style={{fontWeight: 'bold', fontSize: 24}}>
              {DataBean.partner && DataBean.partner.name}
            </Text>
          </View>

          <View
            style={{
              flex: 4,
              marginBottom: 60,
              marginTop: 40,
            }}>
            <Text style={{fontSize: 20}}>
              آدرس : {this.state.partner && DataBean.partner.adress}
            </Text>

            {!DataBean.partner && <Text>در حال لود</Text>}
            <Text style={{fontSize: 18}}>
              از نظر موتوری :
              {DataBean.partner && DataBean.partner.rezMotortypedefTitle}
            </Text>

            <Text>و</Text>

            <Text>
              {DataBean.partner &&
                DataBean.partner.rateMotorType != 0 &&
                DataBean.partner.rezVehicleforTitle}
            </Text>

            {showFixTypes(DataBean.partner)}
          </View>

          <ZZTitle title="گالری تصاویر" />

          <ScrollView horizontal={true} contentInset={{left: 20}}>
            <View style={styles.partnerCarsPlaceHolder} />
            <View style={styles.partnerCarsPlaceHolder} />
            <View style={styles.partnerCarsPlaceHolder} />

            <View style={styles.partnerCarsPlaceHolder} />
            <View style={styles.partnerCarsPlaceHolder} />
            <View style={styles.partnerCarsPlaceHolder} />
          </ScrollView>

          <View style={{justifyContent: 'center'}}>
            <GrayDivider />
            <View style={{height: 25}} />
          </View>
        </View>

        {DataBean.partnerImages && DataBean.partnerImages.length > 0 && (
          <View
            style={{
              flex: 5,
              flexDirection: 'row',
              flexWrap: 'wrap',
              borderColor: '#eebd00',
              borderWidth: 5,
              borderRadius: 20,
            }}>
            {this.renderPartnerImages()}
          </View>
        )}

        <View style={{height: 150}} />
      </View>
    );
  }

  Back() {
    SettingSingleton.navigator.navigate('TblItemsListScreen');
  }

  renderPartnerImages() {
    return DataBean.partnerImages.map((value, index, array) => {
      return (
        <Image
          source={{uri: value.name}}
          style={{
            width: windowWidth / 2 - 15,
            height: 150,
            resizeMode: 'contain',
            borderWidth: 1,
            borderColor: '#e5e5e5',
          }}
        />
      );
    });
  }
}

const styles = StyleSheet.create({
  partnerCarsPlaceHolder: {
    borderWidth: 5,
    borderColor: '#F44336',
    height: windowWidth * 0.25,
    width: windowWidth * 0.25,
  },
});
