// @flow
import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Button,
  Picker,
} from 'react-native';

import {RefreshControl, SafeAreaView, ScrollView} from 'react-native';

import {DataBean, DataBeanService} from '../Service/DataBean';
import {AbstractScreen} from './AbstractScreen';
import {UselessTextInput} from './ProfileScreen';
import {CarTip, TblCar} from '../Models/tblCar';
import {CustomerService} from '../Service/CustomerService';
import {CarService} from '../Service/CarService';
import {MyResponse, MyResponseType} from '../Service/Myglobal';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {_scale} from '../Helpers/SizeHelper';
import {CommonService} from '../Service/CommonService';
import {MobileStorageService} from '../Service/MobileStorageService';
import {SettingSingleton} from './NavigateManagerScreen';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class SelectMyCarDetailScreen extends AbstractScreen {
  profileService: CustomerService = new CustomerService();
  commonService: CommonService = new CommonService();

  carService: CarService = new CarService();
  componentDidMount() {
    DataBean.selectedCar = new TblCar();
    _MyLog(this);
    _MyLog('componentDidMount');

    DataBeanService.retRefreshing(true);

    this.commonService.getYears().then(r => {
      DataBeanService.setYears(r.list);
    });
    MobileStorageService._retrieveData('token')
      .then(tok => {
        this.carService
          .getTipsForMyCar(DataBean.selectedCar.id, tok)
          .then((res: MyResponse<CarTip>) => {
            DataBeanService.retRefreshing(false);

            _MyLog('this.carService\n' + '      .getTipsForMyCar');
            _MyLog(res);

            if (res.type == MyResponseType.Success) {
              DataBeanService.setSelectedCarTips(res.list);
            } else {
              alert(res.message);
            }
          })
          .catch(e => {
            DataBeanService.retRefreshing(false);
          });
      })
      .catch(e => {
        alert('خطا در دسترسی به توکن لطفا دوباره وارد شوید');
        DataBeanService.retRefreshing(false);
      });
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={
            <RefreshControl enabled={false} refreshing={DataBean.refreshing} />
          }>
          {this.renBody()}
        </ScrollView>
        <View style={styles.footer}>
          <Button
            onPress={() => {
              this._save();
            }}
            title="تایید اطلاعات"
          />
        </View>
      </SafeAreaView>
    );
  }

  renBody() {
    return (
      <View
        style={{
          marginRight: _scale(50),
          marginLeft: _scale(50),
        }}>
        {/*    <View>
          <View style={styles.car}>
            <Image
              // source={{uri: DataBean.selectedCar.image}}
              source={require('../assets/img/405.png')}
              style={{
                width: '100%',
                height: _scale(windowHeight * 0.3),
                resizeMode: 'contain',
              }}
            />
            <Text>پژو 405</Text>
          </View>
        </View>*/}
        <View
          style={{
            flex: 1,
            flexDirection: 'row-reverse',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>سال ساخت :</Text>
          {this.yearsPicker()}
        </View>
        <View
          style={{
            flex: 1,

            flexDirection: 'row-reverse',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>تیپ :</Text>
          {this.tipsPicker()}
        </View>
        <View style={{height: 100}} />
      </View>
    );
  }

  showYears() {
    // todo: تابع نمایش دهنده شهر ها
    return DataBean.years.map((m, i) => {
      return <Picker.Item label={m.id} key={m.id} value={m.id} />;
    });
  }
  showSearchCars() {
    if (!DataBean.searchCars || !DataBean.searchCars.length) {
      return <></>;
    }
    return (
      <>
        {DataBean.searchCars.map((value: TblCar, index, array) => {
          return (
            <TouchableOpacity
              style={value.selected ? styles.selectedcar : styles.car}
              onPress={() => {
                this.selectCar(value);
              }}>
              <Image
                source={{uri: value.image}}
                style={{
                  width: '100%',
                  height: _scale(windowHeight * 0.2),
                  resizeMode: 'contain',
                }}
              />
              <Text>{value.name}</Text>
            </TouchableOpacity>
          );
        })}
      </>
    );
  }

  selectCar(value: TblCar) {
    for (let i = 0; i < DataBean.searchCars.length; i++) {
      if (DataBean.searchCars[i] != value) {
        DataBean.searchCars[i].selected = false;
      }
    }

    var i = DataBean.searchCars.findIndex(s => s == value);

    if (!value.selected) {
      DataBean.searchCars[i].selected = true;
    } else {
      DataBean.searchCars[i].selected = !DataBean.searchCars[i].selected;
    }
    DataBeanService.setSearchCars(DataBean.searchCars);

    if (DataBean.searchCars.filter(f => f.selected).length > 0) {
      DataBeanService.setshowOkButton(true);
    } else {
      DataBeanService.setshowOkButton(false);
    }
  }

  yearsPicker() {
    return (
      <Picker
        selectedValue={DataBean.selectedCar.builtYear}
        style={{height: 35, width: '100%'}}
        onValueChange={(itemValue, itemIndex, arr) => {
          DataBean.selectedCar.builtYear = itemValue;
          DataBeanService.setSelectedCar(DataBean.selectedCar);
        }}>
        {DataBean.years && DataBean.years.length && this.showYears()}
      </Picker>
    );
  }

  tipsPicker() {
    return (
      <Picker
        selectedValue={DataBean.selectedCar.carTip}
        style={{height: 35, width: '100%'}}
        onValueChange={(itemValue, itemIndex, arr) => {
          DataBean.selectedCar.carTip = itemValue;
          DataBeanService.setSelectedCar(DataBean.selectedCar);
        }}>
        {DataBean.selectedCarTips &&
          DataBean.selectedCarTips.length &&
          this.showTips()}
      </Picker>
    );
  }

  showTips() {
    // todo: تابع نمایش دهنده شهر ها
    return DataBean.selectedCarTips.map((m, i) => {
      return <Picker.Item label={m.name} key={m.id} value={m} />;
    });
  }

  _save() {
    DataBeanService.retRefreshing(true);
    _MyLog('_save');
    this.carService
      .save(DataBean.selectedCar)
      .then(res => {
        DataBeanService.retRefreshing(false);

        _MyLog('this.carService\n' + '      .save(DataBean.selectedCar)');
        if (res.type == MyResponseType.Success) {
          SettingSingleton.navigator.navigate('CategoryScreen');
        } else {
          alert(res.message);
        }
      })
      .catch(e => {
        _MyLog('catch');
        _MyLog(e);
        DataBeanService.retRefreshing(false);
      });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 9,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start', // if you want to fill rows left to right
    justifyContent: 'space-around',
  },
  selectedcar: {
    borderWidth: 2,
    borderColor: 'red',
    width: '28%', // is 50% of container width
    marginTop: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  car: {
    borderWidth: 2,
    borderColor: '#202D2D2D',
    width: '100%',
    marginTop: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    position: 'absolute',
    bottom: 40,
    right: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
