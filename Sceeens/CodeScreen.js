import React, {Component} from 'react';

import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  SafeAreaView, BackHandler,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {SettingSingleton} from './NavigateManagerScreen';

import {DataBean, DataBeanService} from '../Service/DataBean';
import RegisterService from '../Service/RegisterService';
import {MyResponse} from '../Service/Myglobal';
import {MobileStorageService} from '../Service/MobileStorageService';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import {AbstractScreen} from "./AbstractScreen";

export default class CodeScreen extends AbstractScreen {
  constructor(state: {text: string}) {
    super();
    this.state = state;
    DataBean.currentScreen = this;
    this.state = DataBean;
  }
  state = {
    text: '',
  };

  backHandler;


  componentWillUnmount() {
    super.componentWillUnmount();
    this.backHandler.remove();
  }

  componentDidMount(): void {
    this.backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        ()=>   {

          SettingSingleton.navigator.navigate('SignInScreen');
          return true;

        },
    );
  }


  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={<RefreshControl refreshing={DataBean.refreshing} />}>
          {this.renbodyForCode()}
        </ScrollView>
      </SafeAreaView>
    );
  }

  goNextPage1() {
    if (ApiUrls.IsDebug) {
      SettingSingleton.navigator.navigate('CategoryScreen');
      return;
    }
    if (!this.state.code) {
      alert('کد را صحیح وارد نمایید');
      return;
    }
    if (this.state.code.length < 4) {
      alert('کد باید 4 رقم باشد');
      return;
    }

    DataBeanService.retRefreshing(true);
    this._validateRegisterNumber()
      .then(res => {
        // success
        DataBeanService.retRefreshing(false);
        if (res.type == 2) {
          MobileStorageService._storeData('token', res.single).then(res => {
            SettingSingleton.navigator.navigate('CategoryScreen');
          });
        } else {
          alert(res.message);
        }
      })
      .catch(re => {
        DataBeanService.retRefreshing(false);
      });
  }

  _validateRegisterNumber(): Promise<MyResponse<string>> {
    var rs = new RegisterService();
    return rs.ValidateRegisterNumber(this.state.code, DataBean.customerId);
  }

  navBack() {
    SettingSingleton.navigator.navigate('SignInScreen');
  }

  renbodyForCode() {
    return (
      <View style={gradientStyles.container}>
        <LinearGradient
          style={gradientStyles.linearBG}
          colors={['#fdbb13', '#fbe70f', '#fdbb13']}>
         {/* <View style={{flex: 1}}>
            <TouchableOpacity
              style={gradientStyles.registerButton}
              onPress={() => this.navBack()}>
              <Text style={gradientStyles.registerText}>بازگشت</Text>
            </TouchableOpacity>
          </View>*/}
          <View
            style={{
              flex: 9,
              textAlign: 'center',
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={gradientStyles.text}>کد را وارد کنید</Text>
            <TextInput
              style={gradientStyles.textinput}
              maxLength={4}
              keyboardType={'numeric'}
              placeholder="- - - -"
              onChangeText={code => this.setState({code: code})}
              value={this.state.code}
            />
            <TouchableOpacity
              style={gradientStyles.registerButton}
              onPress={() => this.goNextPage1()}>
              <Text style={gradientStyles.registerText}>ورود</Text>
            </TouchableOpacity>
          </View>
        </LinearGradient>
      </View>
    );
  }
}
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const gradientStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbb717',
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  linearBG: {
    height: windowHeight,
  },

  text: {
    paddingTop: 200,
    width: windowWidth,
    textAlign: 'center',
    fontSize: 50,
    fontFamily: 'Harmattan-Regular',
  },

  textinput: {
    width: 300,
    borderColor: 'black',
    backgroundColor: 'white',
    borderRadius: 10,
    height: 50,
    marginTop: 10,
    padding: 10,
    fontFamily: 'Harmattan-Regular',
    fontSize: 25,
    borderWidth: 1,
    textAlign: 'center',
  },

  registerButton: {
    backgroundColor: '#72bf45',
    width: 150,
    height: 50,
    borderRadius: 5,
    marginTop: 50,
  },

  registerText: {
    color: 'white',
    fontSize: 40,
    lineHeight: 55,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
    fontWeight: '900',
  },
});
