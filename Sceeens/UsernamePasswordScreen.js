import React from 'react';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {Customer} from '../Models/customer';
import {getAndValidateProfile, UselessTextInput} from './ProfileScreen';
import {
  Alert,
  Button,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  TextInput,
  View,
} from 'react-native';
import {SettingSingleton} from './NavigateManagerScreen';
import GreyDivider from '../Components/GreyDivider';
import {CustomerService} from '../Service/CustomerService';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {MobileStorageService} from '../Service/MobileStorageService';
import {MyResponseType} from '../Service/Myglobal';
import {_scale} from '../Helpers/SizeHelper';
import {AbstractScreen} from "./AbstractScreen";

export class UsernamePasswordScreen extends AbstractScreen {
  constructor(state: {text: string}) {
    super();
    this.state = state;
    DataBean.currentScreen = this;
    this.state = DataBean;

    DataBeanService.showSuccessMessage(false);
    DataBeanService.setCustomerProfile(new Customer());
  }

  saveUsernamePassword() {
    var cs = new CustomerService();

    _MyLog('saveProfile call');
    DataBeanService.retRefreshing(true);
    DataBeanService.showSuccessMessage(false);
    MobileStorageService._retrieveData('token')
      .then(tok => {
        _MyLog('token', tok);

        DataBeanService.retRefreshing(true);
        cs.SaveUsernamePassword(
          DataBean.customer.username,
          DataBean.customer.password,
          tok,
        )
          .then(res => {
            _MyLog('SaveProfile response', res);

            DataBeanService.retRefreshing(false);

            if (res.type == MyResponseType.Success) {
              DataBeanService.showSuccessMessage(true);
              MobileStorageService._storeData('token', res.single)
                .then(s => {
                  this.componentDidMount();
                })
                .catch(err => {
                  alert('خطا در ثبت توکن');
                });
            } else {
              alert(res.message);
            }
          })
          .catch(e => {
            DataBeanService.retRefreshing(false);
            console.error(e);
            alert('خطا در اتصال به اینترنت');
          });
      })
      .catch(e => {
        DataBeanService.retRefreshing(false);
        alert(
          'خطا در دسترسی به توکن بایستی دوباره وارد شوید یا مجددا نصب نمایید',
        );
      });
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={{flex: 1}}>
          <ScrollView
            style={{flex: 1}}
            refreshControl={<RefreshControl refreshing={DataBean.refreshing} />}
            contentInsetAdjustmentBehavior="automatic">
            <View style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                    marginTop:15,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize:_scale(16)}}>تعریف نام کاربری و رمز عبور</Text>
              </View>
              {DataBean.customer && (
                <View style={{flex: 1, padding: 20}}>
                  <View style={{flex: 1}}>
                    <Text>نام کاربری</Text>
                    <UselessTextInput
                      editable
                      maxLength={20}
                      onChangeText={text => {
                        DataBean.customer.username = text;
                        DataBeanService.setCustomerProfile(DataBean.customer);
                      }}
                      value={DataBean.customer.username}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <Text>رمز عبور</Text>
                    <UselessTextInput
                      editable
                      maxLength={20}
                      onChangeText={text => {
                        DataBean.customer.password = text;
                        DataBeanService.setCustomerProfile(DataBean.customer);
                      }}
                      value={DataBean.customer.password}
                    />
                  </View>

                  <GreyDivider />
                  {(DataBean.systemMessage ? true : false) && (
                    <View style={{flex: 1}}>
                      <Text>نواقص پروفایل شما :</Text>
                      <TextInput
                        editable={false}
                        textAlign="right"
                        multiline
                        style={{color: 'red', flex: 1}}
                        value={DataBean.systemMessage}
                        numberOfLines={4}
                      />
                    </View>
                  )}

                  {(DataBean.successMessage ? true : false) && (
                    <View style={{flex: 1}}>
                      <Text style={{color: 'green'}}>با موفقیت ثبت گردید</Text>
                    </View>
                  )}
                </View>
              )}
              <View style={{flex: 1}}>
                <Button
                  title="ثبت"
                  onPress={() => {
                    this.saveUsernamePassword();
                  }}
                />
              </View>
             {/* <View style={{flex: 1, marginTop: 20}}>
                <Button
                  title="خروج"
                  onPress={() => {
                    this.myExit();
                  }}
                />
              </View>*/}
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }

  componentDidMount(): void {
    getAndValidateProfile();
  }
}

