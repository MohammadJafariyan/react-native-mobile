// @flow
import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Button,
  Picker,
  ScrollView,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import {AbstractScreen} from './AbstractScreen';
import {_scale} from '../Helpers/SizeHelper';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {global_Colors} from '../Components/MyGlobal';

export class MultiTabScreen extends AbstractScreen {
  componentDidMount() {
    DataBeanService.setCurrentTab(0);
  }

  /*refreshControl={
            <RefreshControl enabled={false} refreshing={DataBean.refreshing} />
          }*/
  render() {
    return (
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          {this._renderBody()}
        </ScrollView>
      </SafeAreaView>
    );
  }

  _renderBody() {
    return (
      <View
        style={{
          marginTop: _scale(10),
        }}>
        <View
          style={{
            flexDirection: 'row-reverse',
            justifyContent: 'space-around',
          }}>
          {this.renderTitles()}
        </View>
        <View style={{flex: 1, marginBottom: _scale(10)}} />
        {this.renderChilds()}
      </View>
    );
  }

  renderTitles() {
    return this.props.names.map((child, i, arr) => {
      if (DataBean.currentTab == i) {
        return (
          <TouchableOpacity>
            <Text style={mutlilTabScreenStyles.categoryText}>{child}</Text>
          </TouchableOpacity>
        );
      } else {
        return (
          <TouchableOpacity
            onPress={() => {
              DataBeanService.setCurrentTab(i);
            }}>
            <Text style={mutlilTabScreenStyles.categoryNonText}>{child}</Text>
          </TouchableOpacity>
        );
      }
    });
  }

  renderChilds() {
    return this.props.children.map((child, i, arr) => {
      if (DataBean.currentTab == i) {
        return <>{child}</>;
      } else {
        return <></>;
      }
    });
  }
}

const mutlilTabScreenStyles = StyleSheet.create({
  categoryContainer: {
    marginTop: _scale(10),
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  categoryNonText: {
    color: '#bfbdbd',
  },
  categoryText: {
    color: global_Colors.secondColor,
    borderBottomColor: global_Colors.secondColor,
    borderBottomWidth: 3,
  },
  category: {
    color: global_Colors.secondColor,
  },
});
