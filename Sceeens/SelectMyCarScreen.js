// @flow
import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Button,
} from 'react-native';

import {RefreshControl, SafeAreaView, ScrollView} from 'react-native';

import {DataBean, DataBeanService} from '../Service/DataBean';
import {AbstractScreen} from './AbstractScreen';
import {UselessTextInput} from './ProfileScreen';
import {TblCar} from '../Models/tblCar';
import {CustomerService} from '../Service/CustomerService';
import {CarService} from '../Service/CarService';
import {MyResponse, MyResponseType} from '../Service/Myglobal';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {_scale} from '../Helpers/SizeHelper';
import {SettingSingleton} from './NavigateManagerScreen';
import {MobileStorageService} from '../Service/MobileStorageService';
import {LKBButton} from '../Components/LKBButton';
import {global_Colors} from '../Components/MyGlobal';
import {LKBInput} from '../Components/LKBInput';
import {Card, Paragraph, Title} from 'react-native-paper';
import {ZZTitle} from '../Components/ZZTitle';
import SelectCarModal from './SelectCarModal';
import SelectMyCarDetailScreen from './SelectMyCarDetailScreen';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class SelectMyCarScreen extends AbstractScreen {
  profileService: CustomerService = new CustomerService();

  carService: CarService = new CarService();
  componentDidMount() {
    this.isSelected();

    DataBeanService.retRefreshing(true);

    MobileStorageService._retrieveData('token')
      .then(tok => {
        this.carService
          .getAllAsPaging(DataBean.citySearchTerm, 0, 6, tok)
          .then((res: MyResponse<TblCar>) => {
            DataBeanService.retRefreshing(false);

            if (res.type == MyResponseType.Success) {
              DataBeanService.setSearchCars(res.list);
            } else {
              alert(res.message);
            }
          })
          .catch(e => {
            DataBeanService.retRefreshing(false);
          });
      })
      .catch(e => {
        DataBeanService.retRefreshing(false);
        alert('خطا در دسترسی به توکن لطفا دوباره وارد شوید');
      });
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={
            <RefreshControl enabled={false} refreshing={DataBean.refreshing} />
          }>
          {this.renBody()}
        </ScrollView>
        <View style={styles.footer}>
         {/* <LKBButton
            onPress={() => {
              this._Ok();
            }}
            color={global_Colors.secondColor}
            title="رد شو انتخاب نمی کنم"
          />*/}

          <LKBButton
            color={global_Colors.mainColor}
            onPress={() => {
              /* if (!DataBean.showOkButton) {
                alert('لطفا ابتدا ماشین خود را انتخاب نمایید');
                return;
              }*/

              DataBeanService.setShowModal(true);
            }}
            title="انتخاب ماشین من"
          />
        </View>
      </SafeAreaView>
    );
  }

  renBody() {
    return (
      <View>
        <View
          style={{
            height: _scale(100),
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: global_Colors.mainColor,
          }}>
          <Text style={{color: 'white'}}>انتخاب ماشین من</Text>
        </View>
        {/*<View style={{flex: 1, marginBottom: _scale(20)}}>
          <LKBInput
            editable={true}
            placeholder="برای جستجو نام ماشین خود را وارد نمایید "
            onChangeText={text => {
              DataBean.carSearchTerm = text;
              DataBeanService.setCarSearchTerm(DataBean.carSearchTerm);
            }}
            value={DataBean.carSearchTerm}
          />
        </View>*/}

        {DataBean.showModal && (
          <View style={{flex: 1}}>
            <SelectCarModal>
              <SelectMyCarDetailScreen />
            </SelectCarModal>
          </View>
        )}

        <View style={{flex: 1}}>
          <ZZTitle title="ایران خودرو" />
          {this.renderSingleRelateProduct()}
        </View>
        <View style={{flex: 1}}>
          <ZZTitle title="سایپا" />
          {this.renderSingleRelateProduct()}
        </View>

        <View style={{flex: 1}}>
          <ZZTitle title="ford" />
          {this.renderSingleRelateProduct()}
        </View>

        <View style={{height: 150}} />
        {/*   <View style={styles.container}>
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
          {this.showSearchCars()}
        </View>*/}
      </View>
    );
  }

  renderSingleRelateProduct() {
    return (
      <ScrollView
        style={{
          overflow: 'visible',
          flexDirection: 'row',
          marginTop: 10,
        }}
        horizontal={true}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
        {this.showRelateSingleProduct()}
      </ScrollView>
    );
  }

  showRelateSingleProduct() {
    return (
      <View style={{paddingRight: 10}}>
        <Card>
          <Card.Cover
            source={require('../assets/img/405.png')}
            style={{
              width: windowWidth * 0.3,
              height: windowHeight * 0.2,
              resizeMode: 'cover',
            }}
          />
          <Card.Content>
            {/*
            <Title>پژو 405</Title>
*/}
            <Paragraph>پژو 405</Paragraph>
          </Card.Content>
          {/* <Card.Actions>
            <Button>Cancel</Button>
            <Button>Ok</Button>
          </Card.Actions>*/}
        </Card>
      </View>
    );
  }
  showSearchCarsBackup() {
    if (!DataBean.searchCars || !DataBean.searchCars.length) {
      return <></>;
    }
    return (
      <>
        {DataBean.searchCars.map((value: TblCar, index, array) => {
          return (
            <TouchableOpacity
              style={value.selected ? styles.selectedcar : styles.car}
              onPress={() => {
                this.selectCar(value);
              }}>
              <Image
                source={{uri: value.image}}
                style={{
                  width: '100%',
                  height: _scale(windowHeight * 0.2),
                  resizeMode: 'contain',
                }}
              />
              <Text>{value.name}</Text>
            </TouchableOpacity>
          );
        })}
      </>
    );
  }

  selectCar(value: TblCar) {
    for (let i = 0; i < DataBean.searchCars.length; i++) {
      if (DataBean.searchCars[i] != value) {
        DataBean.searchCars[i].selected = false;
      }
    }

    var i = DataBean.searchCars.findIndex(s => s == value);

    if (!value.selected) {
      DataBean.searchCars[i].selected = true;
    } else {
      DataBean.searchCars[i].selected = !DataBean.searchCars[i].selected;
    }
    DataBeanService.setSearchCars(DataBean.searchCars);

    DataBeanService.setSelectedCar(DataBean.searchCars[i]);

    this.isSelected();
  }

  isSelected() {
    if (!DataBean.searchCars || !DataBean.searchCars.length) {
      DataBeanService.setshowOkButton(false);
      return;
    }
    if (DataBean.searchCars.filter(f => f.selected).length > 0) {
      DataBeanService.setshowOkButton(true);
    } else {
      DataBeanService.setshowOkButton(false);
    }
  }

  _Ok() {
    //SettingSingleton.navigator.navigate('SelectMyCarDetailScreen');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 9,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start', // if you want to fill rows left to right
    justifyContent: 'space-around',
  },
  selectedcar: {
    borderWidth: 2,
    borderColor: 'red',
    width: '28%', // is 50% of container width
    marginTop: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  car: {
    borderWidth: 2,
    borderColor: '#202D2D2D',
    width: '28%',
    marginTop: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    position: 'absolute',
    bottom: 40,
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
