// @flow
import React from 'react';
import {AbstractScreen} from './AbstractScreen';
import {Badge, Text} from 'react-native-paper';
import {DataBean} from '../Service/DataBean';
import {TouchableOpacity, StyleSheet, View} from 'react-native';
import {_scale} from '../Helpers/SizeHelper';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {TblCategory} from '../Models/tblCategory';
import {ZZTitle} from '../Components/ZZTitle';
import {Avatar} from 'react-native-paper';
import {Button, Card, Title, Paragraph} from 'react-native-paper';
export class UploadImagesSubScreen extends React.Component {
  constructor(props: Props, context: *) {
    super(props, context);
    this.state = {
      template: 'http://c.tile.openstreetmap.org/{z}/{x}/{y}.png',
    };
  }
  render() {
    return (
      <View>
        <Card>
          <Card.Content>
            <Title style={{textAlign: 'right'}}>تصاویر مربوط به خرابی</Title>
            <Paragraph style={{textAlign: 'right'}}>
              کارشناسان با تصاویری که اضافه می نمایید می توانند خدمات بهتری
              ارائه کنند
            </Paragraph>
          </Card.Content>
          <Card.Actions>
            <Text style={{textAlign: 'right'}} />
            {/*
              <Avatar.Icon style={{textAlign: 'right'}} size={35} icon="plus" />
*/}
            <Avatar.Icon
              style={{textAlign: 'right'}}
              size={_scale(50)}
              icon="camera"
            />
          </Card.Actions>
        </Card>
        {/*        {!DataBean.showModal && this.renderMapInfo()}
        {DataBean.showModal && this.renderMap()}*/}

        {this.renderMap()}
        {this.renderMapInfo()}
        {this.renderServices()}
      </View>
    );
  }

  renderServices() {
    if (!DataBean.serviceServices) {
      return <></>;
    }
    return DataBean.serviceServices
      .filter(
        (s: TblCategory) =>
          s.services.filter(sub => sub.isPriceNeedReview && sub.checked)
            .length > 0,
      )
      .map((value: TblCategory, index, array) => {
        return (
          <Card>
            <ZZTitle title={value.name} />
            <Card.Content />
            <Card.Actions>
              <Text style={{textAlign: 'right'}} />
              {/*
              <Avatar.Icon style={{textAlign: 'right'}} size={35} icon="plus" />
*/}
              <Avatar.Icon
                style={{textAlign: 'right'}}
                size={_scale(50)}
                icon="camera"
              />
            </Card.Actions>
          </Card>
        );
      });
  }

  renderMapInfo() {
    return (
      <Card>
        <Card.Content>
          <Title style={{textAlign: 'center'}}>آدرس</Title>
          <Paragraph style={{textAlign: 'right'}}>
            لطفا از روی نقشه مکان درخواست خدمات را مشخص نمایید
          </Paragraph>
        </Card.Content>
        <Card.Actions>
          <Text style={{textAlign: 'right'}} />
          {/*
              <Avatar.Icon style={{textAlign: 'right'}} size={35} icon="plus" />
*/}
          <Avatar.Icon
            style={{textAlign: 'center'}}
            size={_scale(50)}
            icon="map-marker"
          />
        </Card.Actions>
      </Card>
    );
  }
  getInitialState() {
    return {
      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
  }

  onRegionChange(region) {
    this.setState({region});
  }

  renderMap() {
    return (
      <View style={styles.container}>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 800,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
