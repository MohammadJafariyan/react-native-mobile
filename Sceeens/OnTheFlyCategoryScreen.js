import React from 'react';
import {AbstractScreen} from './AbstractScreen';
import Chip from 'react-native-paper/src/components/Chip';
import {ScrollView, Text, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {_scale} from '../Helpers/SizeHelper';

export class OnTheFlyCategoryScreen extends AbstractScreen {
  render() {
    return (
      <ScrollView
        contentContainerStyle={{
          marginTop: _scale(20),
          marginBottom: _scale(20),
        }}
        horizontal={true}
        contentInset={{justifyContent: 'space-between'}}
        showsHorizontalScrollIndicator={false}>
        <Chip icon={_getSupportIcon} onPress={() => console.log('Pressed')}>
          تایر
        </Chip>
        <Chip icon="information" onPress={() => console.log('Pressed')}>
          روغن
        </Chip>
        <Chip icon="bell" onPress={() => console.log('Pressed')}>
          لامپ
        </Chip>
        <Chip icon="wrench" onPress={() => console.log('Pressed')}>
          آهن
        </Chip>
        <Chip icon="information" onPress={() => console.log('Pressed')}>
          چوب
        </Chip>
        <Chip icon="information" onPress={() => console.log('Pressed')}>
          تایر
        </Chip>
      </ScrollView>
    );
  }
}

export function _getSupportIcon() {
  return <FontAwesome name="support" size={_scale(15)} />;
}
