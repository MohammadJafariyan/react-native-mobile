import React from 'react';

import {
  Alert,
  Button,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import {MobileStorageService} from '../Service/MobileStorageService';
import {SettingSingleton} from './NavigateManagerScreen';
import {DataBean, DataBeanService} from '../Service/DataBean';
import {Customer} from '../Models/customer';
import {CustomerService} from '../Service/CustomerService';
import {MyResponseType} from '../Service/Myglobal';
import {_MyLog} from '../MyGlobal/ApiUrls';
import GreyDivider from '../Components/GreyDivider';

import {AbstractScreen} from "./AbstractScreen";
import chat from '../assets/img/chat.png';
import settingIcon from '../assets/img/setting.png';
import yellowIcon from "../assets/img/yellow-icon.png";
import myFavorites from '../assets/img/my-favorites.png';
import comments from '../assets/img/comments.png';
import address from '../assets/img/address.png';
import profileInfo from '../assets/img/profile-info.png';
import ExitProfile from '../assets/img/Exit.png';

const windowWidth = Dimensions.get('window').width;


export function UselessTextInput(props, text) {
  return (
    <TextInput
      {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
      style={{borderWidth: 1, borderRadius: 5, flex: 1}}
    />
  );
}

export class ProfileScreen extends AbstractScreen {
  constructor(state: {text: string}) {
    super();

    DataBeanService.showSuccessMessage(false);
    DataBeanService.setCustomerProfile(new Customer());
  }

  saveProfile(callback) {
    var cs = new CustomerService();

    _MyLog('saveProfile call');
    DataBeanService.retRefreshing(true);
    DataBeanService.showSuccessMessage(false);
    MobileStorageService._retrieveData('token')
      .then(tok => {
        _MyLog('token', tok);

        DataBeanService.retRefreshing(true);
        cs.SaveProfile(DataBean.customer, tok)
          .then(res => {
            _MyLog('SaveProfile response', res);

            DataBeanService.retRefreshing(false);

            if (res.type == MyResponseType.Success) {
              DataBeanService.showSuccessMessage(true);
              MobileStorageService._storeData('token', res.single)
                .then(s => {
                  getAndValidateProfile(callback);
                })
                .catch(err => {
                  alert('خطا در ثبت توکن');
                });
            } else {
              alert(res.message);
            }
          })
          .catch(e => {
            DataBeanService.retRefreshing(false);
            console.error(e);
            alert('خطا در اتصال به اینترنت');
          });
      })
      .catch(e => {
        DataBeanService.retRefreshing(false);
        alert(
          'خطا در دسترسی به توکن بایستی دوباره وارد شوید یا مجددا نصب نمایید',
        );
      });
  }

  componentDidMount(): void {
    getAndValidateProfile();
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={{flex: 1}}>
          <ScrollView
            style={{flex: 1}}
            refreshControl={
              <RefreshControl
                enabled={false}
                refreshing={DataBean.refreshing}
              />
            }
            contentInsetAdjustmentBehavior="automatic">
              <View style={{flex: 1, flexDirection: "row", justifyContent: "space-between"}}>
                <TouchableOpacity style={{margin: 20}}>
                  <Image source={chat} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
                <TouchableOpacity style={{margin: 20}}>
                  <Image source={settingIcon} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
              </View>
            <View style={{flex: 1}}>
              <View style={{flex: 1, flexDirection: "column", width: "100%", alignItems: "center", marginBottom: 20}}> 
                <Image source={yellowIcon} style={{width: 80, height: 80}}/>
                <Text style={{textAlign: "center", color: "grey", fontSize: 25, marginVertical: 5}}>صبا دلخون</Text>
                <Text style={{textAlign: "center", color: "grey", fontSize: 18}}>۰۹۱۴۴۰۷۹۳۸۱</Text>
              </View>

              <View style={{width: windowWidth*.85, marginHorizontal: windowWidth*.075, alignItems: "center"}}>
                <View style={{flex: 1, flexDirection: "row"}}>
                 <View style={{flex: 1, justifyContent: "center", paddingRight: 10}}>
                    <View style = {
                    {
                      borderColor: "grey",
                      borderBottomWidth: 2,
                      borderLeftWidth: 2,
                      width: 13,
                      height: 13,
                      transform: [{
                        rotate: "45deg"
                      }],
                      alignSelf: "flex-end",
                    }
                  }
                  />
                 </View>
                  <View style={{flex: 9, marginVertical: 10}}>
                    <Text style={{textAlign: "right", color: "grey", fontSize: 20, marginBottom: 5}}>آدرس:‌تبریز، ولیعصر، خیابان فروغی، کوچه سوم، پلاک ۲۳</Text>
                    <Text style={{textAlign: "right", color: "grey", fontSize: 20}}>کد پستی:‌</Text>
                  </View>
                </View>

                <GreyDivider />

                <View style={{flex: 1, width: "100%", alignItems: "flex-end"}}>
                    <Text style={{textAlign: "right", color: "grey", fontSize: 20}}>ماشین های من:</Text>
                    <Text style={{textAlign: "right", color: "grey", fontSize: 20, marginVertical: 10}}>مزدا ۳</Text>
                    <TextInput placeholder="افزودن ماشین" placeholderTextColor="grey" style={{borderColor: "#fbb717", borderWidth: 1, borderRadius: 3, width: 150, height: 40, textAlign: "right", paddingRight: 5, marginVertical: 10}}></TextInput>
                    <TextInput placeholder="افزودن ماشین" placeholderTextColor="grey" style={{borderColor: "#fbb717", borderWidth: 1, borderRadius: 3, width: 150, height: 40, textAlign: "right", paddingRight: 5}}></TextInput>

                </View>

                <GreyDivider />

                <View style={{flex: 1, width: "100%"}}>
                  <View style={{flex: 1, flexDirection: "row"}}>
                    <View style={{flex: 1, justifyContent: "center", paddingRight: 10}}>
                      <View style = {
                      {
                        borderColor: "grey",
                        borderBottomWidth: 2,
                        borderLeftWidth: 2,
                        width: 13,
                        height: 13,
                        transform: [{
                          rotate: "45deg"
                        }],
                        alignSelf: "flex-end",
                      }
                    }
                    />
                    </View>
                    <View style={{flex: 9, marginVertical: 10, flexDirection: "row-reverse", justifyContent: "flex-start"}}>
                      <Image resizeMode="contain" source={myFavorites} style={{width: 20, height: 20, margin: 3}}/>
                      <Text style={{textAlign: "right", color: "grey", fontSize: 20}}>لیست مورد علاقه</Text>
                    </View>
                  </View>
                   <View style={{flex: 1, flexDirection: "row"}}>
                    <View style={{flex: 1, justifyContent: "center", paddingRight: 10}}>
                      <View style = {
                      {
                        borderColor: "grey",
                        borderBottomWidth: 2,
                        borderLeftWidth: 2,
                        width: 13,
                        height: 13,
                        transform: [{
                          rotate: "45deg"
                        }],
                        alignSelf: "flex-end",
                      }
                    }
                    />
                    </View>
                    <View style={{flex: 9, marginVertical: 10, flexDirection: "row-reverse", justifyContent: "flex-start"}}>
                      <Image resizeMode="contain" source={comments} style={{width: 20, height: 20, margin: 3}}/>
                      <Text style={{textAlign: "right", color: "grey", fontSize: 20}}>نقد و نظرات</Text>
                    </View>
                  </View>
                   <View style={{flex: 1, flexDirection: "row"}}>
                    <View style={{flex: 1, justifyContent: "center", paddingRight: 10}}>
                      <View style = {
                      {
                        borderColor: "grey",
                        borderBottomWidth: 2,
                        borderLeftWidth: 2,
                        width: 13,
                        height: 13,
                        transform: [{
                          rotate: "45deg"
                        }],
                        alignSelf: "flex-end",
                      }
                    }
                    />
                    </View>
                    <View style={{flex: 9, marginVertical: 10, flexDirection: "row-reverse", justifyContent: "flex-start"}}>
                      <Image resizeMode="contain" source={address} style={{width: 20, height: 20, margin: 3}}/>
                      <Text style={{textAlign: "right", color: "grey", fontSize: 20}}>آدرس ها</Text>
                    </View>
                  </View>

                  

                   <View style={{flex: 1, flexDirection: "row"}}>
                    <View style={{flex: 1, justifyContent: "center", paddingRight: 10}}>
                      <View style = {
                      {
                        borderColor: "grey",
                        borderBottomWidth: 2,
                        borderLeftWidth: 2,
                        width: 13,
                        height: 13,
                        transform: [{
                          rotate: "45deg"
                        }],
                        alignSelf: "flex-end",
                      }
                    }
                    />
                    </View>

                    <View style={{flex: 9, marginVertical: 10, flexDirection: "row-reverse", justifyContent: "flex-start"}}>
                      <Image resizeMode="contain" source={profileInfo} style={{width: 20, height: 20, margin: 3}}/>
                      <Text style={{textAlign: "right", color: "grey", fontSize: 20}}>اطلاعات حساب کاربری</Text>
                    </View>
                   
                  </View>
                  



                  <TouchableOpacity style={{flex: 1, flexDirection: "row"}}
			onPress={() => {this.myExit();}}>


                    <View style={{flex: 9, marginVertical: 10, flexDirection: "row-reverse", justifyContent: "flex-start"}}>
                      <Image resizeMode="contain" source={ExitProfile} style={{width: 20, height: 20, margin: 3}}/>
                      <Text style={{textAlign: "right", color: "grey", fontSize: 20}}>خروج</Text>
                    </View>
                   

                  </TouchableOpacity>
                  


                </View>

              </View>









              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
               
                <Button
                  title={'نام کاربری و رمز عبور'}
                  onPress={() => {
                    SettingSingleton.navigator.navigate(
                      'UsernamePasswordScreen',
                    );
                  }}
                />

                <Button
                  title={'تغییر ماشین من'}
                  onPress={() => {
                    var s = new CarService();
                    s.clearMyCar().then(r => {
                      SettingSingleton.navigator.navigate('SelectMyCarScreen');
                    });
                  }}
                />
              </View>
              {/* {DataBean.customer && (
                <View style={{flex: 1, padding: 20}}>
                  <View style={{flex: 1}}>
                    <Text>شماره تلفن</Text>
                    <UselessTextInput
                      editable={false}
                      keyboardType={'numeric'}
                      maxLength={11}
                      onChangeText={text => {
                        DataBean.customer.phone = text;
                        DataBeanService.setCustomerProfile(DataBean.customer);
                      }}
                      value={DataBean.customer.phone}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <Text>کد ملی</Text>
                    <UselessTextInput
                      editable
                      keyboardType={'numeric'}
                      maxLength={10}
                      onChangeText={text => {
                        DataBean.customer.milicode = text;
                        DataBeanService.setCustomerProfile(DataBean.customer);
                      }}
                      value={DataBean.customer.milicode}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <Text>نام و نام خانوادگی</Text>
                    <UselessTextInput
                      editable
                      textAlign="right"
                      onChangeText={text => {
                        DataBean.customer.name = text;
                        DataBeanService.setCustomerProfile(DataBean.customer);
                      }}
                      value={DataBean.customer.name}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <Text>آدرس</Text>
                    <UselessTextInput
                      editable
                      textAlign="right"
                      multiline
                      onChangeText={text => {
                        DataBean.customer.address = text;
                        DataBeanService.setCustomerProfile(DataBean.customer);
                      }}
                      value={DataBean.customer.address}
                      numberOfLines={4}
                    />
                  </View>

                  <GreyDivider />
                  {(DataBean.systemMessage ? true : false) && (
                    <View style={{flex: 1}}>
                      <Text>نواقص پروفایل شما :</Text>
                      <TextInput
                        editable={false}
                        textAlign="right"
                        multiline
                        style={{color: 'red', flex: 1}}
                        value={DataBean.systemMessage}
                        numberOfLines={4}
                      />
                    </View>
                  )}

                  {(DataBean.successMessage ? true : false) && (
                    <View style={{flex: 1}}>
                      <Text style={{color: 'green'}}>با موفقیت ثبت گردید</Text>
                    </View>
                  )}
                </View>
              )} */}
              <View style={{flex: 1}}>
                <Button
                  title="ثبت"
                  onPress={() => {
                    this.saveProfile(() => {
                      if (
                        DataBean.selectedItems &&
                        DataBean.selectedItems.length > 0
                      ) {
                        if (!DataBean.systemMessage) {
                          Alert.alert(
                            'پروفایل شما تکمیل شد ',
                            'سبد خرید شما آماده پرداخت است!',
                            [
                              {
                                text: 'خیر',
                                onPress: () => console.log('NO Pressed'),
                                style: 'cancel',
                              },
                              {
                                text: 'برو به صفحه پرداخت',
                                onPress: () =>
                                  SettingSingleton.navigator.navigate(
                                    'BasketPreviewBeforeBuyScreen',
                                  ),
                              },
                              //  {text: 'بله', onPress: () => this.addToBasketWithRelatedItems(item)},
                            ],
                          );
                        }
                      }
                    });
                  }}
                />
              </View>
              <View style={{flex: 1, marginTop: 20}}>
                <Button
                  title="خروج"
                  onPress={() => {
                    this.myExit();
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }

  myExit() {
    MobileStorageService._removeItem('token');
    SettingSingleton.navigator.navigate('HelloScreen');
  }
}
// متد در چند جا استفاده شده است و موقع تغیر باید بسیار مراقب بود
// وقتی کاربر را فراخوانی می کند ، یک systemMessage همراه داریم که اگر پروفایل او خطا داشت این پروپرتی مقدار خواهد داشت
// پروفایل کاربر سمت سرور validation می شود
export function getAndValidateProfile(callback) {
  var cs = new CustomerService();

  // لودینگ را فعال می کند
  DataBeanService.retRefreshing(true);

  // لاگ است
  _MyLog('componentDidMount');

  // اطلاعات توکن را بر میگرداند
  MobileStorageService._retrieveData('token')
    .then(tok => {
      if (!tok) {
        _MyLog('token is null forwarding to SignInByUsernamePasswordScreen ');
        SettingSingleton.navigator.navigate('SignInByUsernamePasswordScreen');
        return;
      }

      _MyLog('token', tok);
      _MyLog('GetCustomerProfileWithValidation call');

      // متد در چند جا استفاده شده است و موقع تغیر باید بسیار مراقب بود
      // وقتی کاربر را فراخوانی می کند ، یک systemMessage همراه داریم که اگر پروفایل او خطا داشت این پروپرتی مقدار خواهد داشت
      // پروفایل کاربر سمت سرور validation می شود
      cs.GetCustomerProfileWithValidation(tok)
        .then(res => {
          DataBeanService.retRefreshing(false);

          _MyLog('GetCustomerProfileWithValidation', res);
          if (res.type == MyResponseType.Success) {
            DataBeanService.setCustomerProfile(res.single);
            DataBeanService.setSystemMessage(res.message);

            if (callback) {
              callback();
            }
          } else {
            alert(res.message);
          }
        })
        .catch(e => {
          DataBeanService.retRefreshing(false);
          console.error(e);
          alert('خطا در اتصال به اینترنت');
        });
    })
    .catch(e => {
      DataBeanService.retRefreshing(false);
      alert(
        'خطا در دسترسی به توکن بایستی دوباره وارد شوید یا مجددا نصب نمایید',
      );
    });
}
