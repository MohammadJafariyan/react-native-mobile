import React from 'react';
import {
  Alert,
  Button,
  default as Colors,
  Image,
  Picker,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
import TblItemService from '../Service/TblItemService';
import {SettingSingleton} from './NavigateManagerScreen';
import {MyResponse, MyResponseType} from '../Service/Myglobal';
import {City, PartnersItems} from '../Service/models';
import {DataBean, DataBeanService} from '../Service/DataBean';
import SelectRelateItemsModal from './SelectRelateItemsModal';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import ServiceRelatedGoodsModal from './ServiceRelatedGoodsModal';
import {BasketValidatorService} from '../Service/BasketValidatorService';
import {ZZTitle} from '../Components/ZZTitle';
import {AbstractScreen} from './AbstractScreen';
import CityService from '../Service/ServicesService';
import {OnTheFlyCategoryScreen} from './OnTheFlyCategoryScreen';

import FIX from '../assets/img/FirstPage/FIX.png';
import PTT from '../assets/img/FirstPage/PTT.png';
import MOT from '../assets/img/FirstPage/MOT.png';
import PMM from '../assets/img/FirstPage/PMM.png';
import PartnersCategory from  '../Components/PartnersCategory';
import GreyDivider from '../Components/GreyDivider';
import MyFilter from '../Components/MyFilter';
import ItemPresentation from '../Components/ItemPresentation';
import ItemPresentation1 from '../Components/ItemPresentation1';


function numberWithCommas(x) {
  y = Number(x);
  return y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}



/*
*
*
*  SingletonHolder.navigator.selectedCityId = this.state.selectedCityId;
    SingletonHolder.navigator.selectedCategoryId = this.state.selectedCategoryId;
*/
export default class TblItemsListScreen extends AbstractScreen {
  constructor(state: {text: string}) {
    super();

    DataBeanService.setShowModal(false);
    this.state = state;
    DataBean.currentScreen = this;
    this.state = DataBean;
  }
  componentWillUnmount() {
    super.componentWillUnmount();

    this._isMounted = false;
  }

  getAllTblItems(
    getAll,
    selectedCategoryId,
  ): Promise<MyResponse<PartnersItems>> {
    // console.log('--------------------------calling--------------------------');
    var s = new TblItemService();
    return s.getAll(getAll, selectedCategoryId);
  }

  getAllCities(): Promise<MyResponse<City>> {
    let s = new CityService();
    return s.getAllCity();
  }

  loadTblitems() {
    /* console.log('loadTblitems , selectedCityId=>', DataBean.selectedCityId);
    console.log(
      'loadTblitems , selectedCategoryId=>',
      DataBean.selectedCategoryId,
    );*/
    this.getAllTblItems(
        DataBean.selectedCityId ? DataBean.selectedCityId : 1,
        DataBean.selectedCategoryId?  DataBean.selectedCategoryId :1,
    ).then(res => {
      // console.log(res)
      if (res.type === MyResponseType.Success) {
        if (DataBean.selectedItems.length > 0) {
          // this.setState({selectedItems: SingletonHolder.selectedItems});
        }

        if (DataBean.selectedItems && DataBean.selectedItems.length > 0) {
          for (var i = 0; i < res.list.length; i++) {
            if (
              DataBean.selectedItems.find(s => s.id == res.list[i].id) != null
            ) {
              res.list[i].selected = true;
            }
          }
        }

        DataBeanService.setItemList(res.list);
      } else {
        alert(ApiUrls.showErrorMsg(res.message));
      }

      DataBeanService.retRefreshing(false);
    });
  }

  componentDidMount() {
    // todo: در این تابع ، وب سرویس ها فراخوانی می شوند
    this._isMounted = true;

    DataBeanService.retRefreshing(true);
    if (DataBean.isCategoryChanged == true) {
      //  console.log('---------------------refresh tirelistst------------');
      DataBeanService.setItemList([]);
      DataBean.isCategoryChanged = false;
    }
    this.loadTblitems();
  }

  renbody() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'space-around',
          flexDirection: 'column',
        }}>
        <View
          style={{
            flex: 9,
          }}>
          <OnTheFlyCategoryScreen />


          {!DataBean.itemList ||
            (DataBean.itemList.length <= 0 && !DataBean.refreshing && (

              <Text style={{textAlign: 'center', marginTop: 100}}>
                {' '}
                بازار انتخاب شده در این شهر است
              </Text>
            ))}

          {/* {DataBean.itemList &&
            DataBean.itemList.length > 0 &&
            this.show_TireList_ZZ_threeColumn()
            } */}

          <ScrollView style={{marginTop: -20, paddingBottom: 100}}>
            {this.showTires()}
            {this.showServices()}
          </ScrollView>
        </View>
        <View style={{height: 100}} />

        {/* <View
          style={{
            flex: 1,
            backgroundColor: '#8dfcff',
          }}>
          {DataBean.selectedItems && DataBean.selectedItems.length > 0 && (
            <Button title="خرید" onPress={() => this.goBuyScreen()} />
          )}
        </View>*/}
      </View>
    );
  }

  render() {
    // todo:در این متد صفحه اصلی اسکرین نمایش داده می شود
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={tireList_styles.scrollView}
            refreshControl={
              <RefreshControl
                refreshing={DataBean.refreshing}
                onRefresh={() => this.refreshComp()}
              />
            }>
            {this.renbody()}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }

  showTireListSingleList() {
    return DataBean.itemList.map((m, i) => {
      return (
        <View
          style={{flex: 1, justifyContent: 'flex-start', flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              borderRadius: 50,
              borderWidth: 1,
              borderColor: '#ffb915',
              margin: 5,
            }}>
            {m && this.showTblItem(m)}
          </View>
        </View>
      );
    });
  }

  show_TireList_ZZ_threeColumn(serviceOrGood: number) {
    return DataBean.itemList
      .filter(f => f.tblItem.serviceOrGood == serviceOrGood)
      .map((m, i) => {
        return (


          <TouchableOpacity key={m.id} onPress={() => this.addToBasket(m)}>

            {m.partner.movable===0 && (<ItemPresentation itemComNameByPartner={m.itemComNameByPartner}
            itemComNameByPartner={m.itemComNameByPartner}
            tblItemName={m.tblItem.name}
            price={m.price}
            partnerName={m.partner.name}
            img={require('../assets/img/FirstPage/FIX.png')}/>)}

            {m.partner.movable===1 && (<ItemPresentation itemComNameByPartner={m.itemComNameByPartner}
            itemComNameByPartner={m.itemComNameByPartner}
            tblItemName={m.tblItem.name}
            price={m.price}
            partnerName={m.partner.name}
            img={require('../assets/img/FirstPage/PTT.png')}/>)}

           {m.partner.movable===2 && (<ItemPresentation itemComNameByPartner={m.itemComNameByPartner}
            itemComNameByPartner={m.itemComNameByPartner}
            tblItemName={m.tblItem.name}
            price={m.price}
            partnerName={m.partner.name}
            img={require('../assets/img/FirstPage/MOT.png')}/>)}

           {m.partner.movable===3 && (<ItemPresentation itemComNameByPartner={m.itemComNameByPartner}
            itemComNameByPartner={m.itemComNameByPartner}
            tblItemName={m.tblItem.name}
            price={m.price}
            partnerName={m.partner.name}
            img={require('../assets/img/FirstPage/PMM.png')}/>)}






            {/*


            /////////////////////
            <View style={tireList_styles.tireWithPriceView}>
              <Image
                style={tireList_styles.tireWithPriceImg}
                resizeMode="contain"
                source={require('../assets/img/index.jpg')}
              />
              <Text numberOfLines={1} style={tireList_styles.tireWithPriceName}>
                {m.itemComNameByPartner
                  ? m.itemComNameByPartner
                  : m.tblItem.name}
              </Text>

              <Text style={tireList_styles.tireWithPriceInfo}>قیمت: {numberWithCommas(m.price)}</Text>
              <Text numberOfLines={1} style={tireList_styles.tireWithPriceInfo}>

                فروشنده: {m.partner.name}
              </Text>
            </View>
                */}
          </TouchableOpacity>
        );
      });
  }

  showTireList(Rlist, Llist) {
    // todo: تابع نمایش دهنده ی لیست بصورت دو ستونی
    return Rlist.map((m, i) => {
      let l_item = Llist.length <= i ? null : Llist[i];
      let r_item = Rlist.length <= i ? null : Rlist[i];

      return (
        <View
          style={{flex: 1, justifyContent: 'flex-start', flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              alignItems: 'stretch',
              height: 150,
            }}>
            {l_item && this.showTblItem(l_item)}
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'stretch',
              height: 150,
            }}>
            {r_item && this.showTblItem(r_item)}
          </View>
        </View>
      );
    });
  }

  showCitiesItems() {
    // todo: تابع نمایش دهنده شهر ها
    return DataBean.cities.map((m, i) => {
      return <Picker.Item label={m.name} key={m.id} value={m.id} />;
    });
  }

  showTblItem(item) {
    return (
      <TouchableOpacity
        key={item.id}
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingRight: 40,
          paddingLeft: 10,
          paddingTop: 10,
          paddingBottom: 5,
        }}
        onPress={() => this.addToBasket(item)}>
        <View style={{flex: 1}}>
          <Image
            source={require('../assets/img/index.jpg')}
            style={{flex: 1, opacity: 0.4}}
          />
        </View>
        <View style={{flex: 9, flexDirection: 'column'}}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            {item.itemComNameByPartner
              ? item.itemComNameByPartner
              : item.tblItem.name}
          </Text>

          <Text style={{}}>{item.partner.name}</Text>
          <Text style={{}}>
            {item.partner.city.name} - {item.partner.adress}
          </Text>

          <View style={{flex: 1, height: 60}} />
          <View style={{flexDirection: 'row', alignItems: 'stretch'}}>
            {item.selected && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}

            <Text style={{flex: 1, textAlign: 'left'}}>
              {' '}
              {item.price} تومان{' '}
            </Text>
            {/* <Button
              title={item.partner.name}
              onPress={() => this.TestNavigate()}
            />*/}

            <View>
              <Button
                title="صفحه فروشنده"
                onPress={() => this.goSellerScreen(item)}
              />
            </View>
          </View>

          {showFixTypes(item.partner)}
        </View>
      </TouchableOpacity>
    );
  }

  goBuyScreen() {
    SettingSingleton.selectedItems = DataBean.selectedItems;
    SettingSingleton.navigator.navigate('BuySuccessedScreen');
  }

  refreshComp() {
    DataBeanService.retRefreshing(true);
    this.componentDidMount();
  }

  goHelloScreen() {
    SettingSingleton.navigator.navigate('HelloScreen');
  }

  TestNavigate() {
    SettingSingleton.navigator.navigate('CarInfo');
  }

  goSellerScreen(item) {
    SettingSingleton.navigator.navigate('SellerScreen');
    // console.log('goSellerScreen', item.partner.id);

    DataBeanService.setPartner(item.partner);
  }

  goCategory() {
    SettingSingleton.navigator.navigate('CategoryScreen');
  }

  addToBasket(item: PartnersItems) {
    SettingSingleton.navigator.navigate('ParnterItemDetailScreen');
    DataBeanService.setGoDetailPartnerItem(item);

    /*
    //service & motory
    if (item.tblItem.serviceOrGood === 1 && item.sendType == 1) {
      this.addToBasketService(item);
    } else {
      this.addToBasketGood(item);
    }*/
  }

  addToBasketWithRelatedItems(tmp) {
    var item: PartnersItems = tmp;
    if (!item.selected) {
      item.selected = true;
      for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
        item.relatedBasketPartnerItems[i].selected = true;
      }
    } else {
      item.selected = false;
      for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
        item.relatedBasketPartnerItems[i].selected = false;
      }
    }

    // todo : آپدیت انتخاب شده ها
    let selectedItems = [...DataBean.selectedItems];

    if (!selectedItems) {
      selectedItems = [];
    }

    // todo: اگر انتخاب شده باشد
    if (item.selected) {
      selectedItems.push(item);
      for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
        selectedItems.push(item.relatedBasketPartnerItems[i]);
      }
    } else {
      // todo : اگر انتخاب نشده باشد ان را حذف کن
      var i = selectedItems.findIndex(s => s.id == item.id);
      selectedItems.splice(i, 1);

      for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
        var j = selectedItems.findIndex(
          s => s.id == item.relatedBasketPartnerItems[i].id,
        );
        selectedItems.splice(j, 1);
      }
    }

    DataBeanService.setSelectedItems(selectedItems);
  }

  showConfirm(item: PartnersItems) {
    var text = '';
    for (var i = 0; i < item.relatedBasketPartnerItems.length; i++) {
      text += item.relatedBasketPartnerItems[i].tblItem.name + '\n';
    }

    text += '\n';
    text += 'در این صورت مایلید اضافه شود ؟';
    Alert.alert('این آیتم باید همراه آیتم های زیر خرید شود ', text, [
      {
        text: 'خیر',
        onPress: () => console.log('NO Pressed'),
        style: 'cancel',
      },
      {text: 'بله', onPress: () => this.selectThisItem(item)},
      //  {text: 'بله', onPress: () => this.addToBasketWithRelatedItems(item)},
    ]);
  }

  showTires() {
    var name = DataBean.selectedCategoryName
      ? DataBean.selectedCategoryName + ' ها '
      : '';
    return (
      <>

      {/*zeini*/}

      <GreyDivider/>


      <View style={{flexDirection:"row-reverse", justifyContent:'space-between',marginBottom:20}}>
          <PartnersCategory title="فروشگاه های ثابت" detail="زمان فعالیت مشخص خرید حضوری" img={FIX}  />
          <PartnersCategory title="فروشگاه های آنلاین" detail="ارسال کالا از طریق پست جمهوری IR" img={PTT}  />
          <PartnersCategory title="فروشگاه های آنلاین" detail="ارسال با پیک موتوری داخل شهر" img={MOT}  />
          <PartnersCategory title="فروشگاه های آنلاین" detail="مجموعه ای از امکاناتPTT و MOT" img={PMM}  />


      </View>


        <ZZTitle title={name} />

        <View style={tireList_styles.tireWithPriceContainer}>
          {DataBean.itemList &&
            DataBean.itemList.length > 0 &&
            this.show_TireList_ZZ_threeColumn(0)}
        </View>
      </>
    );
  }

  showServices() {
    return <>

    </>;
    /*return (
      <>
        <Title title="خدمات مرتبط" />
        <View style={styles.tireWithPriceContainer}>
          {DataBean.itemList &&
            DataBean.itemList.length > 0 &&
            this.show_TireList_ZZ_threeColumn(1)}
        </View>
      </>
    );*/
  }
}

export function showFixTypes(partner) {
  return (
    <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch'}}>
      {partner.movable === 0 && (
        <Text style={{flex: 1, textAlign: 'right', color: 'blue'}}> FIX</Text>
      )}
      {partner.movable === 1 && (
        <Text style={{flex: 1, textAlign: 'right', color: 'red'}}> MOT</Text>
      )}

      {partner.movable === 2 && (
        <Text style={{flex: 1, textAlign: 'right', color: 'yellow'}}> PTT</Text>
      )}
      {partner.movable === 3 && (
        <Text style={{flex: 1, textAlign: 'right', color: 'green'}}> MTT</Text>
      )}
    </View>
  );
}

const windowWidth = Dimensions.get('window').width;

export const tireList_styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },

  tireWithPriceView: {
    width: 0.3 * windowWidth,
    height: 0.4 * windowWidth,
    marginTop: 0.03 * windowWidth,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: '#feb816',
    borderRadius: 5,
    textAlign: 'right',
    position: 'relative',
  },

  tireWithPriceName: {
    color: 'black',
    fontSize: 16,
    textAlign: 'right',
    paddingRight: 5,
    marginTop: 5
  },

  tireWithPriceInfo: {
    color: 'white',
    fontSize: 13,
    textAlign: 'right',
    paddingRight: 5,
  },

  tireWithPriceImg: {
    width: '95%',
    height: '50%',
    borderRadius: 5,
    marginLeft: '2.5%',
    marginRight: '2.5%',
    marginTop: 5,
    backgroundColor: 'white',
  },

  tireWithPriceContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
    paddingBottom: 50,
  },

});
