import React from 'react';

import {
  Dimensions,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  CheckBox,
  Alert,
  BackHandler,
  Linking,
} from 'react-native';

import {WebView} from 'react-native-webview';

import LinearGradient from 'react-native-linear-gradient';
import {SettingSingleton} from './NavigateManagerScreen';

import {DataBean, DataBeanService} from '../Service/DataBean';
import RegisterService from '../Service/RegisterService';
import {MyResponse} from '../Service/Myglobal';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import {gradientStyles} from './CodeScreen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {_scale} from '../Helpers/SizeHelper';
import {AbstractScreen} from "./AbstractScreen";

export default class SignInScreen extends AbstractScreen {
  state = {
    text: '',
  };
  backHandler;


  componentWillUnmount() {
    super.componentWillUnmount();

    this.backHandler.remove();
  }

  componentDidMount(): void {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      ()=>   {

        if (DataBean.showWebView){
          DataBeanService.setShowWebView(false);
          return true;
        }
        SettingSingleton.navigator.navigate('SignInByUsernamePasswordScreen');
        return true;

      },
    );
  }

  constructor(state: {text: string}) {
    super();
    this.state = state;
    DataBean.currentScreen = this;
    this.state = DataBean;
  }

  render() {
    return (

    <>
      {DataBean.showWebView && (

          <LinearGradient
              style={styles.linearBG}
              colors={['#fdbb13', '#fbe70f', '#fdbb13']}>

            <TouchableOpacity style={{justifyContent:'center',alignItems:'center'}}
             onPress={()=>DataBeanService.setShowWebView(false)}>
              <Text style={{color:'white',fontSize:windowWidth*0.1,textAlign:'left'}}>بستن</Text>
            </TouchableOpacity>
            <View style={{flex: 1, alignItems: 'center'}}>

              <WebView
                  source={{uri: 'http://38.132.99.139:180/home/about'}}
                  style={{flex:1,height:windowHeight,width:windowWidth}}
                  javaScriptEnabled={true}
                  domStorageEnabled={true}
                  startInLoadingState={true}
                  scrollEnabled={true}
              />
            </View>
          </LinearGradient>

      )}

      {
        !DataBean.showWebView && (
            <>

              <SafeAreaView>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    refreshControl={<RefreshControl refreshing={DataBean.refreshing}/>}>
                  {this.renbodyForSignIn()}
                </ScrollView>
              </SafeAreaView>
            </>

        )
      }
      </>

    );
  }

  goNextScreen() {
    if (ApiUrls.IsDebug) {
      SettingSingleton.navigator.navigate('CodeScreen');
      return;
    }

    if (!DataBean.mobileNumber) {
      alert('موبایل را وارد نمایید');
      return;
    }
    if (DataBean.mobileNumber.charAt(0) !== '0') {
      alert('موبایل اشتباه است');
      return;
    }
    if (DataBean.mobileNumber.charAt(1) !== '9') {
      alert('موبایل اشتباه است');
      return;
    }
    if (DataBean.mobileNumber.length !== 11) {
      alert('شماره موبایل باید 11 رقم باشد مانند مثال : 09145623547');
      return;
    }

    if (!DataBean.okcontract) {
      Alert.alert(
        'با تشکر از نصب کاچار  ',
        'برای استفاده از کاچار ابتدا لازم است با قوانین موافقیت نمایید',
        [
          {
            text: 'لغو',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'موافقم',
            onPress: () => {
              DataBeanService.setIsOkcontract(true);
              this.DoSignInHelper();
            },
          },
          ,
          {
            text: 'مطالعه قوانین',
            onPress: () => this.handleClick(),
          },
        ],
      );
    }
    if (!DataBean.okcontract) {
      return;
    }

    this.DoSignInHelper();
  }

  sendMobileNumber(): Promise<MyResponse<number>> {
    var rs = new RegisterService();
    return rs.SendMobile(DataBean.mobileNumber);
  }

  handleClick = () => {
    DataBeanService.setShowWebView(true);
    //Linking.openURL("http://38.132.99.139:180/home/about");
  };
  renbodyForSignIn() {
    return (
      <View style={styles.container}>
        <LinearGradient
          style={styles.linearBG}
          colors={['#f4f4f4', '#f4f2ea', '#f4f4f4']}>




              <View style={{flex: 7, alignItems: 'center'}}>
                <Text style={styles.text}>شماره همراه خود را وارد نمایید</Text>
                <TextInput
                  style={styles.textinput}
                  underlineColorAndroid="transparent"
                  keyboardType={'numeric'}
                  onChangeText={text => {
                    DataBeanService.setMobileNumber(text);
                  }}
                  value={DataBean.mobileNumber}
                />

                <TouchableOpacity
                  style={styles.enterButton}
                  onPress={() => this.goNextScreen()}>
                  <Text style={styles.enterTextWhite}>ورود</Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 3, flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.enterText}>با قوانین کاچار موافقم</Text>

                  <CheckBox
                    value={DataBean.okcontract}
                    onValueChange={r =>
                      DataBeanService.setIsOkcontract(!DataBean.okcontract)
                    }
                    style={{marginTop: 10}}
                  />
                </View>
                <TouchableOpacity
                  style={{flexDirection: 'row', justifyContent: 'center'}}
                  onPress={() => this.handleClick()}>
                  <Text style={styles.smallText}>مطالعه قوانین</Text>
                  <FontAwesome name="location-arrow"  size={23} />
                </TouchableOpacity>
              </View>


          {/* <TouchableOpacity
              style={gradientStyles.registerButton}
              onPress={() => this.goCodeScreen()}>
            <Text style={gradientStyles.registerText}>ورود کد</Text>
          </TouchableOpacity>*/}
          {/*
          <TextInput secureTextEntry={true} style={styles.textinput} />
*/}

          {/*      <TouchableOpacity style={styles.restoreAccButton}>
            <Text style={styles.restoreAccText}>رمز عبورم را فراموش کردم</Text>
          </TouchableOpacity>*/}
          {/*  <TouchableOpacity style={styles.restoreAccButton}>
            <Text style={styles.restoreAccText}>
              اگر عضو نیستید، ثبت نام کنید
            </Text>
          </TouchableOpacity>*/}
        </LinearGradient>
      </View>
    );
  }

  goCodeScreen() {
    SettingSingleton.navigator.navigate('CodeScreen');
  }

  DoSignInHelper() {

    DataBeanService.retRefreshing(true);

    this.sendMobileNumber()
        .then(res => {
          // success
          DataBeanService.retRefreshing(false);
          if (res.type == 2) {
            DataBeanService.setCustomerId(res.single);

            SettingSingleton.navigator.navigate('CodeScreen');
          } else {
            alert(res.message);
          }
        })
        .catch(re => {
          DataBeanService.retRefreshing(false);
        });
  }
}
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbb717',
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  linearBG: {
    flex:1,
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },


  text: {
    paddingTop: 200,
    width: windowWidth,
    textAlign: 'center',
    fontSize: 30,
    fontFamily: 'Harmattan-Regular',
  },

  textinput: {
    width: 300,
    borderColor: 'black',
    backgroundColor: 'white',
    borderRadius: 10,
    height: 50,
    marginTop: 25,
    padding: 10,
    color: 'grey',
    textAlign: 'center',
  },

  enterButton: {
    backgroundColor: '#72bf45',
    width: 80,
    height: 50,
    borderRadius: 5,
    marginTop: 50,
  },

  enterText: {
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
  },
  enterTextWhite: {
    color: 'white',
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
  },
  smallText: {
    fontSize: _scale(20),
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
  },

  restoreAccButton: {
    backgroundColor: 'white',
    width: 300,
    height: 50,
    borderRadius: 10,
    marginTop: 25,
  },

  restoreAccText: {
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
    fontSize: 30,
  },

  test: {
    width: 200,
    height: 40,
    backgroundColor: 'white',
  },
});

