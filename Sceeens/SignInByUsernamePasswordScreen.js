import React from 'react';

import {
  Dimensions,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  CheckBox,
  Alert,
  BackHandler,
  Linking,
  Image,
} from 'react-native';

import {WebView} from 'react-native-webview';

import LinearGradient from 'react-native-linear-gradient';
import {SettingSingleton} from './NavigateManagerScreen';

import {DataBean, DataBeanService} from '../Service/DataBean';
import RegisterService from '../Service/RegisterService';
import {MyResponse} from '../Service/Myglobal';
import {ApiUrls, _MyLog} from '../MyGlobal/ApiUrls';
import {gradientStyles} from './CodeScreen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import HelloScreen from './HelloScreen';
import {MobileStorageService} from '../Service/MobileStorageService';
import {_scale} from '../Helpers/SizeHelper';
import {AbstractScreen} from "./AbstractScreen";

export default class SignInByUsernamePasswordScreen extends AbstractScreen {
  state = {
    text: '',
  };
  backHandler;

  componentWillUnmount() {
    super.componentWillUnmount();

    this.backHandler.remove();
  }

  componentDidMount(): void {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      DataBeanService.setShowWebView(false);
      return true;
    });
  }

  constructor(state: {text: string}) {
    super();
    this.state = state;
    DataBean.currentScreen = this;
    this.state = DataBean;
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentContainerStyle={{flex: 1}}
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={
            <RefreshControl enabled={false} refreshing={DataBean.refreshing} />
          }>
          {this.renbodyForSignIn()}
        </ScrollView>
      </SafeAreaView>
    );
  }

  goNextScreen() {
    debugger;
    if (ApiUrls.IsDebug) {
      SettingSingleton.navigator.navigate('CodeScreen');
      return;
    }

    if (!DataBean.mobileNumber) {
      alert('موبایل را وارد نمایید');
      return;
    }
    if (DataBean.mobileNumber.charAt(0) !== '0') {
      alert('موبایل اشتباه است');
      return;
    }
    if (DataBean.mobileNumber.charAt(1) !== '9') {
      alert('موبایل اشتباه است');
      return;
    }
    if (DataBean.mobileNumber.length !== 11) {
      alert('شماره موبایل باید 11 رقم باشد مانند مثال : 09145623547');
      return;
    }

    if (!DataBean.okcontract) {
      Alert.alert(
        'با تشکر از نصب کاچار  ',
        'برای استفاده از کاچار ابتدا لازم است با قوانین موافقیت نمایید',
        [
          {
            text: 'لغو',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'موافقم',
            onPress: () => DataBeanService.setIsOkcontract(true),
          },
          ,
          {
            text: 'مطالعه قوانین',
            onPress: () => this.handleClick(),
          },
        ],
      );
    }
    if (!DataBean.okcontract) {
      return;
    }

    DataBeanService.retRefreshing(true);

    this.sendMobileNumber()
      .then(res => {
        // success
        DataBeanService.retRefreshing(false);
        if (res.type == 2) {
          DataBeanService.setCustomerId(res.single);

          SettingSingleton.navigator.navigate('CodeScreen');
        } else {
          alert(res.message);
        }
      })
      .catch(re => {
        DataBeanService.retRefreshing(false);
      });
  }

  sendMobileNumber(): Promise<MyResponse<number>> {
    var rs = new RegisterService();
    return rs.SendMobile(DataBean.mobileNumber);
  }

  handleClick = () => {
    SettingSingleton.navigator.navigate('SignInScreen');

    //Linking.openURL("http://38.132.99.139:180/home/about");
  };
  renbodyForSignIn() {
    return (
      <View>
        <View style={styles.container}>
          <View
            style={{
              flex: 2,
            }}>
            <Image
              source={require('../assets/img/kachar_app_1.jpg')}
              style={{
                height: _scale(windowHeight * 0.2),
                width: windowWidth,
                resizeMode: 'contain',
                marginTop: 10,
              }}
            />
          </View>

          <View
            style={{flex: 7, alignItems: 'center', flexDirection: 'column'}}>
            <Text style={styles.textWithTop}>نام کاربری</Text>
            <TextInput
              style={styles.textinputNotop}
              underlineColorAndroid="transparent"
              onChangeText={text => {
                DataBeanService.setUsername(text);
              }}
              value={DataBean.username}
            />
            <Text style={styles.textWithTop}>رمز عبور</Text>
            <TextInput
              style={styles.textinputNotop}
              underlineColorAndroid="transparent"
              secureTextEntry={true}
              onChangeText={text => {
                DataBeanService.setPassword(text);
              }}
              value={DataBean.password}
            />

            <TouchableOpacity
              style={styles.enterButton}
              onPress={() => this.SignByUsernamePassword()}>
              <Text style={styles.enterTextWhite}>ورود</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex: 3, flexDirection: 'column'}}>
            <TouchableOpacity
              style={{flexDirection: 'row'}}
              onPress={() => this.handleClick()}>
              <Text style={styles.enterText}>
                اگر عضو نیستید ، ثبت نام کنید
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{flexDirection: 'row', justifyContent: 'center'}}
              onPress={() => this.handleClick()}>
              <Text style={styles.smallText}>رمز عبورم را فراموش کرده ام</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  goCodeScreen() {
    SettingSingleton.navigator.navigate('CodeScreen');
  }

  SignByUsernamePassword() {
    if (!DataBean.username) {
      alert('نام کاربری خالی است');
      return;
    }
    if (!DataBean.password) {
      alert('رمز عبور خالی است');
      return;
    }
    DataBeanService.retRefreshing(true);

    var s = new RegisterService();
    s.SignInWithUsernamePassword(DataBean.username, DataBean.password)
      .then(res => {
        console.log(res);
        // success
        DataBeanService.retRefreshing(false);
        /*   MobileStorageService._storeData('token', res.single).then(res => {
          SettingSingleton.navigator.navigate('CategoryScreen');
        });*/
        if (res.type == 2) {
          MobileStorageService._storeData('token', res.single).then(res => {
            SettingSingleton.navigator.navigate('CategoryScreen');
          });
        } else if (res.type == 3) {
          alert(res.message);
        } else {
          alert('نام کاربری یا رمز عبور اشتباه است');
        }
      })
      .catch(re => {
        DataBeanService.retRefreshing(false);
      });
  }
}
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  linearBG: {
    flex: 1,
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  text: {
    width: windowWidth,
    textAlign: 'center',
    fontSize: 30,
    fontFamily: 'Harmattan-Regular',
  },
  textWithTop: {
    width: windowWidth,
    marginTop: 25,
    textAlign: 'center',
    fontSize: 30,
    fontFamily: 'Harmattan-Regular',
  },

  textinput: {
    width: 300,
    borderColor: 'black',
    backgroundColor: 'white',
    borderRadius: 10,
    height: 50,
    marginTop: 25,
    padding: 10,
    color: 'grey',
    textAlign: 'center',
  },
  textinputNotop: {
    width: 300,
    borderColor: 'black',
    backgroundColor: 'white',
    borderRadius: 10,
    height: 50,
    padding: 10,
    color: 'grey',
    textAlign: 'center',
  },

  enterButton: {
    backgroundColor: '#72bf45',
    width: 80,
    height: 50,
    borderRadius: 5,
    marginTop: 50,
  },

  enterTextWhite: {
    color: 'white',
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
  },
  enterText: {
    color: '#26ac9b',
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
  },
  smallText: {
    color: '#26ac9b',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
  },

  restoreAccButton: {
    backgroundColor: 'white',
    width: 300,
    height: 50,
    borderRadius: 10,
    marginTop: 25,
  },

  restoreAccText: {
    textAlign: 'center',
    fontFamily: 'Harmattan-Regular',
    fontSize: 30,
  },

  test: {
    width: 200,
    height: 40,
    backgroundColor: 'white',
  },
});
