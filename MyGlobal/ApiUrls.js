import {MyResponse, MyResponseType} from '../Service/Myglobal';

export class ApiUrls {
  static GetAllTires = 'http://185.120.250.214:80/GetAllTires';
  static ImagesLocation = `${ApiUrls.getBaseUrl()}/pics/`;
  static GetAllCities = `${ApiUrls.getBaseUrl()}/CityApi/GetAsync`;
  static PurchaseSaveURL = `${ApiUrls.getBaseUrl()}/PurchaseApi/Save`;
  static PurchaseItemSaveAllURL = `${ApiUrls.getBaseUrl()}/PurchaseItemApi/SaveAll`;
  static IsDebug: boolean = false;

  static getPurchaseStatusURL(purchaseId, token) {
    return `${ApiUrls.getBaseUrl()}/PurchaseApi/GetPurchaseStatus?purchaseId=${purchaseId}&token=${token}`;
  }
  static PurchaseURL(cityId) {
    return `${ApiUrls.getBaseUrl()}/PurchaseApi/SaveAll`;
  }
  static GetAllCategories(cityId) {
    return `${ApiUrls.getBaseUrl()}/TblItemsApi/GetCategories?cityId=${cityId}&userId=1`;
  }

  static GetSellerByIdURL(id) {
    return `${ApiUrls.getBaseUrl()}/PartnerApi/GetByIdViewModelAsync?id=${id}`;
  }

  static GetAllTblitems(cityid, selectedCategoryId) {
    return `${ApiUrls.getBaseUrl()}/TblItemsApi/GetByCityIdAsync?cityId=${cityid}&categoryId=${selectedCategoryId}`;
  }

  static getBaseUrl() {
    //return 'https://localhost:5011' ;
    return 'http://38.132.99.139';
    // return 'http://www.kachar-eshop.ir';
  }

  static GetSellerImagesByIdURL(id) {
    return `${ApiUrls.getBaseUrl()}/ImagesApi/GetListByParterId?id=${id}`;
  }

  static getImageUrl(name) {
    return `${ApiUrls.getBaseUrl()}/Medias/GetFileById?name=${name}`;
  }

  static showErrorMsg(message) {
    if (message) {
      return message;
    } else {
      return 'خطا در اتصال به اینترنت';
    }
  }

  static GetServiceItemsForServiceByPartnerIdAPI(
    partnerItemId,
    partnerId,
    cityId,
  ) {
    return `${ApiUrls.getBaseUrl()}/ServiceItemsApi/GetServiceItemsForServiceByPartnerId?partnerItemId=${partnerItemId}&partnerId=${partnerId}&cityId=${cityId}`;
  }

  static SendMobileUrl() {
    return `${ApiUrls.getBaseUrl()}/Register/SendMobile`;
  }

  static ValidateRegisterNumberURL(registerNumber, customerId) {
    return `${ApiUrls.getBaseUrl()}/Register/ValidateRegisterNumber?registerNumber=${registerNumber}&customerId=${customerId}`;
  }

  static ValidateTokenURL(token) {
    return `${ApiUrls.getBaseUrl()}/Register/ValidateToken?token=${token}`;
  }

  static returnRandomFakeResponse() {
    return new Promise(function(resolve, reject) {
      //   console.log('returnRandomFakeResponse')

      var res = new MyResponse();
      res.type =
        (Math.floor(Math.random() * 6) + 1) % 2 == 0
          ? MyResponseType.Success
          : MyResponseType.FakeFail;

      res.message = 'خطا فیک رخ داده است';

      resolve(res);
    });
  }

  static SignInWithUsernamePasswordURL() {
    return `${ApiUrls.getBaseUrl()}/Register/SignInWithUsernamePassword`;
  }
  static purchaseURL(token, total) {
    return `${ApiUrls.getBaseUrl()}/PurchaseApi/SaveAllByToken?token=${token}&totalFee=${total}`;
  }

  static GetCustomerProfileWithValidation(token) {
    return `${ApiUrls.getBaseUrl()}/api/ProfileApi/GetCustomerProfileWithValidation?token=${token}`;
  }

  static SaveProfileURL(token) {
    return `${ApiUrls.getBaseUrl()}/api/ProfileApi/SaveCustomerProfile?token=${token}`;
  }

  static SaveUsernamePasswordURL(username, password, tok) {
    return `${ApiUrls.getBaseUrl()}/api/ProfileApi?token=${tok}&username=${username}&password=${password}`;
  }
}

export function _MyLog(m1, msg) {
  console.log('----', m1);
  if (msg) {
    console.log(msg);
  }
  console.log('-----------------next------------------------');
}

export function chunkArray(arr, n) {
  var chunkLength = Math.max(arr.length / n, 1);
  var chunks = [];
  for (var i = 0; i < n; i++) {
    if (chunkLength * (i + 1) <= arr.length) {
      chunks.push(arr.slice(chunkLength * i, chunkLength * (i + 1)));
    }
  }
  return chunks;
}
