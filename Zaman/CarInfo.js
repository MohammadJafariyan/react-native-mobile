﻿import React, { Component } from 'react';
import { Text, View, Button } from 'react-native';
import GeneralStatusBarColor from './GeneralStatusBarColor';
import { SettingSingleton } from '../Sceeens/NavigateManagerScreen';


export default class HelloWorldApp extends Component {

    render() {
        //showCitiesItems() {
        //    // todo: تابع نمایش دهنده شهر ها
        //    return this.state.cities.map((m, i) => {
        //        return <Picker.Item label={m.name} key={m.id} value={m.id} />;
        //    });
        //}
        return (

            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <GeneralStatusBarColor backgroundColor='#fbb717' barStyle="dark-content" />
                <Button title="go back" onPress={() => this.goback()}/>
                <Text>Helloooooo, world!</Text>
            </View>
        );
    }

    goback() {
        SettingSingleton.navigator.navigate('TblItemsListScreen')
    }

}

