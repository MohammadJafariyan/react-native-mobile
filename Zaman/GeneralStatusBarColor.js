import React from 'react';
import { View, SafeAreaView } from 'react-native';
import styles from './components/GeneralStatusBarColorStyles';
const GeneralStatusBarColor = ({ backgroundColor, ...props }) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <SafeAreaView translucent backgroundColor={backgroundColor} {...props} />
    </View>
);
export default GeneralStatusBarColor;