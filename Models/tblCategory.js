/**
 * My Title
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import {TblItem2} from './tblItem2';
import {TblService} from './tblCar';
import {PartnersItems} from './partnersItems';

export class TblCategory {
  constructor(name: string, icon: string) {
    this.icon = icon;
    this.name = name;
  }
  services: PartnersItems[];
  selectedCount: number;
  icon: string;
  id: number;
  name: string;
  pic: string;
  active: number;
  tblItem: Array<TblItem2>;
}
