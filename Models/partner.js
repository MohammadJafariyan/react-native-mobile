/**
 * My Title
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { Basket } from './basket';
import { City } from './city';
import { Media } from './media';
import { PartnersItems } from './partnersItems';


export class Partner {
    id: number;
    createDate: Date;
    expireDate: Date;
    prevId: number;
    nextId: number;
    name: string;
    adress: string;
    behaviour: number;
    cityId: number;
    movable: number;
    motorTypeDescription: string;
    city: City;
    basket: Array<Basket>;
    media: Array<Media>;
    partnersItems: Array<PartnersItems>;
}
