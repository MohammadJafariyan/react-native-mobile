import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const _scale = size => width / guidelineBaseWidth * size;
const _verticalScale = size => height / guidelineBaseHeight * size;
const _moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;

export {_scale, _verticalScale, _moderateScale};
