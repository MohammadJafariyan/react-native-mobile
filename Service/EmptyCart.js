import { Text, View, Image, SafeAreaView, StyleSheet, Dimensions, ScrollView } from 'react-native'; 
import SuggestedTireInEmptyCart from './SuggestedTireInEmptyCart'
import sampletire from '../assets/img/index.jpg';
import React, { Component } from 'react';
import cart2 from '../assets/img/cart2.png';
import Title from './Title';
import cart3 from '../assets/img/FirstPage/cart3.png';

export default class EmptyCart extends Component {
    render() {
        return(


        <SafeAreaView>
          <ScrollView contentInsetAdjustmentBehavior="automatic">


           <View style={styles.container}>
               <View style={{flexDirection:"row-reverse",right:15}}>
                   <Image source={cart3} style={{height:25,width:25,}}></Image>
                   <View style={{left:15,}}>
                   <Text >سبد خرید</Text>
                   </View>
               </View>


                <View style={styles.emptyCartView}>
                    <Image source={cart2} style={styles.emptyCartImage}></Image>
                    <Text style={styles.emptyCartText}>سبد خرید شما خالی است</Text>
                </View>

                
                <View style={{height:55}}/>


                <View >
                    
                    <Title title = 'پیشنهاد کاچار برای شما' />

                    <View style={styles.cartSuggestionTiresView}>
                        <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
                        <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
                        <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
                        <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
                        <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />
                        <SuggestedTireInEmptyCart name="تایر E23" img={sampletire} />               
                    </View>
                </View>

                                

           </View>
           <View style={{height:160}}></View>
        </ScrollView>
        </SafeAreaView>

        )
    }
}
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: "white",
        },

        emptyCartView: {
            
            top:30,
            width: windowWidth,
            alignItems: "center",
        },

        emptyCartImage: {
            width: 200, 
            height: 200, 
        },

        emptyCartText: {
            fontSize: 20
        },

        cartSuggestionView: {
        
            height: windowHeight*0.25,
            width: windowWidth,
            bottom: 6,
        },

        cartSuggestionText: {
            textAlign: "right",
            fontSize: 14,
            paddingRight: 5
        },

        cartSuggestionTextOrangeBox: {
            height: 5, 
            width: 50, 
            backgroundColor: "orange", 
            top: 9
        },

        cartSuggestionTiresView: {
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-around",
            flexWrap: "wrap",
            top: 30,
            
        },
       
    }
)