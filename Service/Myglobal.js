export class MyResponse<T> {
  type: MyResponseType;
  businessType:MyResponse;
  single: T;
  list: T[];
  message: string;


  constructor(type: MyResponseType, businessType: MyResponse, single: T, list: T[], message: string) {
    this.type = type;
    this.businessType = businessType;
    this.single = single;
    this.list = list;
    this.message = message;
  }
}

export const MyResponseType = {
  Success: 2,
  Fail: 3, FakeFail: 4,
};
