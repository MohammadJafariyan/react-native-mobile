import React, { Component } from 'react';
import { View, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import home from '../assets/img/home.png';
import cart from '../assets/img/cart.png';
import menu from '../assets/img/menu.png';
import user from '../assets/img/user.png';

export default class BottomBar extends Component {
    render() {
        return(
            <View style={styles.bottomRow}>
                <TouchableOpacity><Image source={user} style={styles.icons}/></TouchableOpacity>
                <TouchableOpacity><Image source={cart} style={styles.icons}/></TouchableOpacity>
                <TouchableOpacity><Image source={menu} style={styles.icons}/></TouchableOpacity>
                <TouchableOpacity><Image source={home} style={styles.icons}/></TouchableOpacity>
            </View>
        )
    }
}

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create(
    {
         icons: {
            height: 30,
            width: 30
        },

        bottomRow: {
            flex: 1, 
            flexDirection: "row", 
            justifyContent: "space-around", 
            height: 60,
            width: windowWidth,
            backgroundColor: '#fbb717',
            position: 'absolute',
            bottom: 0,
            padding: 10,
        },
    }
)

