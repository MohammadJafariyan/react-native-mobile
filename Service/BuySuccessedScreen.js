import React from 'react';
import {
  Button,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Alert,
  Image,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {SettingSingleton} from '../Sceeens/NavigateManagerScreen';
import {PurchaseItemService, PurchaseService} from './purchaseService';
import {PartnersItems, TblItem} from './models';
import {DataBean, DataBeanService, DataBeanStructure} from './DataBean';
import EmptyCart from './EmptyCart';
import ServiceRelatedGoodsModal from '../Sceeens/ServiceRelatedGoodsModal';
import GreyDivider from '../Components/GreyDivider';
import {MyResponseType} from './Myglobal';
import {AbstractScreen} from "../Sceeens/AbstractScreen";
import CheckBoxChecked from "../assets/img/FirstPage/checkboxChecked.png";
import CheckBoxEmpty from "../assets/img/FirstPage/checkboxEmpty.png";
import cart3 from '../assets/img/FirstPage/cart3.png';
import yellowCart from '../assets/img/yellow-cart.png';

function numberWithCommas(x) {
  y = Number(x);
  return y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function buy() {}
export default class BuySuccessedScreen extends AbstractScreen {
  constructor(state: {text: string}) {
    super();
    this.state = state;
    DataBean.currentScreen = this;
    this.state = DataBean;
  }

  componentDidMount(): void {
    DataBeanService.refreshTotalFee();
    empty = DataBean.totalFee == 0;
   // console.log(empty);
  }

  render() {
    if (DataBean.totalFee == 0) {
      return <EmptyCart />;
    }
    return (
      <>
        <StatusBar barStyle="dark-content" />

        <SafeAreaView>
          {/*
          
          
          
          <View
            style={{
              backgroundColor: '#fbb717',
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 5,
            }}>
            <View>
              <Text>قیمت کل {numberWithCommas(DataBean.totalFee)}  تومان  </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                margin: 5,
              }}>
              <Button
                title="پیش نمایش خرید"
                onPress={() => this.buyPreview()}
              />
            </View>



            

           <View
              style={{
                margin: 5,
              }}>
              <Button
                title="بازگشت"
                onPress={() => this.backToTiresListScreen()}
              />
            </View>
          </View>

        */}


          
              <View style={{flexDirection:"row-reverse", right:15, marginTop: 5}}>
                   <Image source={yellowCart} style={{height:25,width:25,}}></Image>
                   <View>
                   <Text style={{color: "grey", fontSize: 20, paddingRight: 5}}>سبد خرید</Text>
                   </View>
               </View>

          
           <View style={{padding: 20, marginTop: 10}}>
             <Text style={{textAlign: "right"}}>ارسال به آدرس : تبریز ولیعصر خیابان فروغی کوچه سوم بلاک ۳۴</Text> 
             <Text style={{textAlign: "right"}}>کد بستی:</Text>
           </View>

           {/* <GreyDivider /> */}


          <ScrollView contentInsetAdjustmentBehavior="automatic">
             <GreyDivider />
            <View
              style={{
                flex: 9,
              }}>
              {DataBean.selectedItems.map((partneritem: PartnersItems, i) => {
                return (
                  <ScrollView>
                    <Text style={styles.cartTireName}>
                      {partneritem.itemComNameByPartner
                        ? partneritem.itemComNameByPartner
                        : partneritem.tblItem.name}
                    </Text>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row-reverse',
                        flexWrap: 'wrap',
                        padding: 5,
                        marginTop: 10,
                        marginBottom: 20,
                        justifyContent: 'space-around',
                      }}>
                      <View
                        style={{
                          flex: 1,
                        }}>
                        <View style={styles.cartTireImageView}>
                          <Image
                            style={styles.cartTireImage}
                            source={require('../assets/img/index.jpg')}
                          />
                        </View>
                        <View
                          style={{width: 30, marginTop: -40, marginLeft: 10}}>
                          <Button
                            title="x"
                            onPress={() => this.removeFromBasket(partneritem)}
                          />
                        </View>
                      </View>

                      <View style={styles.cartProductInfoViewParent}>
                        <View style={styles.cartProductInfoView}>
                          <Text style={{textAlign: 'right', color: '#808285'}}>
                            {' '}
                            قیمت:{' '}
                          </Text>
                          <Text style={{textAlign: 'left', color: '#808285'}}>
                            {partneritem.muliplyPrice
                              ? numberWithCommas(partneritem.muliplyPrice)
                              : numberWithCommas(partneritem.price)}{' '}
                            تومان
                          </Text>
                        </View>
                        <View style={styles.cartProductInfoView}>
                          <Text style={{textAlign: 'right', color: '#808285'}}>
                            {' '}
                            شهر:‌{' '}
                          </Text>
                          <Text style={{textAlign: 'left', color: '#808285'}}>
                            {partneritem.partner.city.name}
                          </Text>
                        </View>
                        <View style={styles.cartProductInfoView}>
                          <Text style={{textAlign: 'right', color: '#808285'}}>
                            {' '}
                            فروشگاه:‌{' '}
                          </Text>
                          <Text style={{textAlign: 'left', color: '#808285'}}>
                            {partneritem.partner.name}
                          </Text>
                        </View>


                        
                        
                              {/*
                              
                              Zeini 



                        {partneritem.isMainBasketItem &&
                          partneritem.relatedBasketPartnerItems && (
                            <View style={{marginTop: 10}}>
                              {this.showRelatedPartnerItemsInBasket(
                                partneritem,
                              )}
                            </View>
                          )
                        }
                        
                        */}

                        {partneritem.relatedGoods && (
                          <View style={{marginTop: 10}}>
                         {/*{this.showRelatedGoods(partneritem)}*******zeini*****show goodModals *****************************/} 

                       </View>
                        )}



                        <View>
                          {partneritem.iHaveGoodsConfirmed && (
                            <View
                              style={{
                                justifyContent: 'flex-start',
                                flexDirection: 'row',
                              }}>
                              <TouchableOpacity
                                style={{
                                  backgroundColor: '#72bf45',
                                  padding: 5,
                                  borderWidth: 1,
                                  borderColor: '#ee710e',
                                }}
                                onPress={() => {
                                  DataBeanService.setSelectedServiceTypeTblItem(
                                    partneritem,
                                  );
                                  DataBeanService.setShowServiceRelatedGoodsModal(
                                    true,
                                  );

                                  SettingSingleton.navigator.navigate(
                                    'BuySuccessedHelperScreen',
                                  );
                                }}>
                                <Text style={{color: 'white'}}>تغییر</Text>
                              </TouchableOpacity>
                              <View
                                style={{
                                  backgroundColor: '#72bf45',
                                  padding: 5,
                                }}>
                                <Text style={{color: 'white'}}>
                                  اقلام مورد نیاز این سرویس را دارم
                                </Text>
                              </View>
                            </View>
                            
                          )}
                        </View>
                        <View>{this.renderCount(partneritem)}</View>
                      </View>
                    </View>

                  
                     
                    {partneritem.relatedGoods && (
                        
                     <View style={{flexDirection:'row-reverse',paddingLeft:30}}>

                       <View style={{padding:10}}>
                          <Image source={CheckBoxChecked} style={{width:20,height:20}}/>
                       </View>
                       <View>{this.showRelatedGoods(partneritem)}</View>
                                         
                    </View>
                    )}


                     {partneritem.isMainBasketItem &&
                          partneritem.relatedBasketPartnerItems && (


                            <View style={{marginTop: 10}}>
                              {this.showRelatedPartnerItemsInBasket(
                                partneritem,
                              )}
                            </View>

                          )
                        }
                      
                    

                    <GreyDivider />

            

                  </ScrollView>
                );
              })}
            </View>


        <View style={{backgroundColor:'#fbb717', paddingHorizontal:windowWidth*.1, paddingVertical: 20,width: windowWidth}}> 
          <Text style={{textAlign: "right", color: "white", fontSize: 16}}>کالاهای موجود در سبد شما ثبت و رزرو نشده اند برای ثبت سفارش مراحل بعدی را تکمیل کنید. </Text>
       </View>

                       




       <View
            style={{
             // backgroundColor: '#fbb717',
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 5,
              marginTop:20
            }}>
            <View>
              <Text>قیمت کل {numberWithCommas(DataBean.totalFee)}  تومان  </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                margin: 5,
              }}>
              <Button
                title="پیش نمایش خرید"
                onPress={() => this.buyPreview()}
              />
            </View>
        </View>










            <View style={{height: 220}} />
          </ScrollView>
        </SafeAreaView>
        
      </>
    );
  }

  buyBackup() {
    var surchaseService = new PurchaseService();

    var purchase = {};
    var d = new Date();
    purchase.PurchaseDate = `${d.getFullYear()}-${d.getMonth()}-${d.getDay()}T${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;

   // console.log('surchaseService.Save(purchase)============>', purchase);
    surchaseService
      .Save(purchase)
      .then(res => {
     /*   console.log(
          'surchaseService\n' +
            '      .Save(purchase)\n' +
            '      .then(res => { \n',
          res,
        );*/
        if (res) {
          return res.json();
        } else {
          return res;
        }
      })
      .then(id => {
      //  console.log('.then(id =>============>', id);
        var selectedItems = this.GetSelectedItems(id);

        console.log(
          'purchaseItemService.SaveAll(selectedItems)============>',
          selectedItems,
        );

        var purchaseItemService = new PurchaseItemService();
        purchaseItemService
          .SaveAll(selectedItems)
          .then(res => {
            SettingSingleton.navigator.navigate('AfterBuySuccessScreen');
          })
          .catch(e => {
            console.log('  .catch(e => {', e);
            alert('خطایی رخ داد');
            /*   alert(e);*!/*/
          });
      })
      .catch(e => {
        console.log('  .catch(e => {', e);
        alert('خطایی رخ داد');
        /*   alert(e);*!/*/
      });
  }

  GetSelectedItems(id) {
    var arr = [];
    for (let i = 0; i < DataBean.selectedItems.length; i++) {
      var itemToPurchase: itemToPurchase = {};
      itemToPurchase.TblItemId = DataBean.selectedItems[i].tblItem.id;
      itemToPurchase.PurchaseId = id;
      arr.push(itemToPurchase);
    }
    return arr;
  }
  /* removeFromBasket(tmp) {
        var partneritem: PartnersItems = tmp;
        if (partneritem.isLeaf){
            Alert.alert('قابل حذف از سبد نیست', "این آیتم وابسته آیتم های دیگر است و نمی تواند حذف شود", [
                {text: 'بله', onPress: () => {}},
            ]);
        }
        if (partneritem.relatedBasketPartnerItems){

        }

    }*/
  removeFromBasket(partneritem) {
    DataBeanService.pushOrPopToBasket(partneritem);
    DataBeanService.refreshTotalFee();

    // Removes the first element of fruits
  }

  backToTiresListScreen() {
    SettingSingleton.navigator.navigate('TblItemsListScreen');
  }

  showRelatedPartnerItemsInBasket(partneritem: PartnersItems) {
    return partneritem.relatedBasketPartnerItems.map((value, index, array) => {
      if (value.id == partneritem.id) {
        return <></>;
      }

      return this.getRelateView(value, partneritem);
    });
  }

  checkFromRelatedBasketPartnerItems(
    parent: PartnersItems,
    value: PartnersItems,
  ) {
    let selectedItems = [...DataBean.selectedItems];

    var index = selectedItems.findIndex(i => i.id == parent.id);

    var jindex = selectedItems[index].relatedBasketPartnerItems.findIndex(
      j => j.id == value.id,
    );

    for (
      let i = 0;
      i < selectedItems[index].relatedBasketPartnerItems.length;
      i++
    ) {
      selectedItems[index].relatedBasketPartnerItems[i].checked = false;
    }

    // انتخاب
    if (!selectedItems[index].relatedBasketPartnerItems[jindex].checked) {
      selectedItems[index].relatedBasketPartnerItems[jindex].checked = true;
    } else {
      selectedItems[index].relatedBasketPartnerItems[jindex].checked = !DataBean
        .selectedItems[index].relatedBasketPartnerItems[jindex].checked;
    }

    // پایان انتخاب
    DataBeanService.setSelectedItems(selectedItems);
    DataBeanService.refreshTotalFee();
  }

  renderCount(partneritem: PartnersItems) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          marginTop: 15,
          width: 108,
        }}>
        <Text style={{color: '#fbb717', textAlign: 'right'}}>تعداد </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'stretch',
            backgroundColor: '#fbb717',
            height: 36,
            borderRadius: 2,
            width: 108,
          }}>
          <TouchableOpacity
            style={{width: 36}}
            onPress={() => this.increase(partneritem)}>
            <Text
              style={{
                fontSize: 25,
                color: 'white',
                lineHeight: 36,
                textAlign: 'center',
              }}>
              +
            </Text>
          </TouchableOpacity>
          <View
            style={{
              width: 30,
              height: 30,
              margin: 3,
              alignItems: 'center',
              backgroundColor: 'white',
              borderRadius: 15,
            }}>
            <Text
              style={{
                fontSize: 17,
                width: '100%',
                height: '100%',
                textAlign: 'center',
                lineHeight: 30,
                color: 'grey',
              }}>
              {partneritem.count ? partneritem.count : '0'}
            </Text>
          </View>
          <TouchableOpacity
            style={{width: 36}}
            onPress={() => this.decrease(partneritem)}>
            <Text
              style={{
                fontSize: 25,
                color: 'white',
                lineHeight: 36,
                textAlign: 'center',
              }}>
              -
            </Text>
          </TouchableOpacity>
        </View>
      </View>


    );
  }

  decrease(partneritem: PartnersItems) {
    if (!partneritem.count) {
      return;
    }
    if (partneritem.count <= 1) {
      return;
    }
    partneritem.count--;

    partneritem.muliplyPrice = partneritem.price * partneritem.count;
    this.refreshItems(partneritem);
  }

  increase(partneritem: PartnersItems) {
    if (!partneritem.count) {
      partneritem.count = 0;
    }
    partneritem.count++;

    partneritem.muliplyPrice = partneritem.price * partneritem.count;
    this.refreshItems(partneritem);
  }

  refreshItems(partneritem: PartnersItems) {
    let selectedItems = [...DataBean.selectedItems];

    var index = selectedItems.findIndex(i => i.id == partneritem.id);

    selectedItems[index] = partneritem;

    DataBeanService.setSelectedItems(selectedItems);
    DataBeanService.refreshTotalFee();
  }




  getRelateView(value: PartnersItems, partneritem) {
    return (
      <TouchableOpacity
        style={{
          //borderWidth: 1,
          //borderColor: '#ee710e',
          //borderRadius: 5,
          flexDirection: 'row-reverse',
          padding: 10,
          flexWrap: 'wrap',
        }}
        onPress={() =>
          this.checkFromRelatedBasketPartnerItems(partneritem, value)
        }>

        {/**  Zeini */}
        <View style={{paddingRight:10,paddingLeft:10}}>
        
            {partneritem.relatedBasketPartnerItems &&
            partneritem.relatedBasketPartnerItems.length >= 2 &&
            value.checked && (
                          
                  <Image source={CheckBoxChecked} style={{width:20,height:20,}}/>
               

            )}

            {
            !(
            partneritem.relatedBasketPartnerItems &&
            partneritem.relatedBasketPartnerItems.length >= 2 &&
            value.checked
            )
            
            && (
          
                     <Image source={CheckBoxEmpty} style={{width:20,height:20,}}/>
        
            )}
        </View>



        <Text style={{flexWrap: 'wrap'}}>
          {value.itemComNameByPartner
            ? value.itemComNameByPartner
            : value.tblItem.name}
        </Text>
        <Text>
          {' '}
          {value.muliplyPrice ? value.muliplyPrice : value.price} تومان{' '}
        </Text>


        {/** 
        <View>
          {partneritem.relatedBasketPartnerItems &&
            partneritem.relatedBasketPartnerItems.length > 2 &&
            value.checked && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}
          {partneritem.relatedBasketPartnerItems &&
            partneritem.relatedBasketPartnerItems.length == 2 && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}
        </View>
        */}



      </TouchableOpacity>
    );
  }

  

  showRelatedGoods(partneritem: PartnersItems) {
    if (!partneritem.relatedGoods || partneritem.relatedGoods.length <= 0) {
      return <></>;
    }

    return partneritem.relatedGoods.map((value, index, array) => {
      if (value.id == partneritem.id) {
        return <></>;
      }

      return this.getRelateGoodView(value, partneritem);
    });
  }

  getRelateGoodView(value, partneritem) {
    return (
      
      <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
        <TouchableOpacity
          style={{
        //zeini
          borderWidth: 1,
            borderColor: '#ee710e',
            borderRadius: 5,
            flexDirection: 'row-reverse',
            padding: 10,
          }}
          onPress={() => {
            DataBeanService.setSelectedServiceTypeTblItem(partneritem);
            DataBeanService.setShowServiceRelatedGoodsModal(true);
            SettingSingleton.navigator.navigate('BuySuccessedHelperScreen');
          }}>
          <Text>تغییر</Text>
        </TouchableOpacity>
        <View
          style={{
      //zeini     //borderWidth: 1,
            borderColor: '#ee710e',
            borderRadius: 5,
            flexDirection: 'row-reverse',
            padding: 10,
          }}>
          <Text>
            {value.itemComNameByPartner
              ? value.itemComNameByPartner
              : value.tblItem.name}
          </Text>

          <Text>
            {' '}
            {value.muliplyPrice ? value.muliplyPrice : value.price} تومان{' '}
          </Text>
          {value.partner.id != partneritem.partner.id && (
            <Text>({value.partner.name})</Text>
          )}
          {/*   <View>
          {partneritem.relatedBasketPartnerItems &&
            partneritem.relatedBasketPartnerItems.length > 2 &&
            value.checked && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}
          {partneritem.relatedBasketPartnerItems &&
            partneritem.relatedBasketPartnerItems.length == 2 && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}
        </View>*/}
        </View>
      </View>
    );
  }

  buyPreview() {
    SettingSingleton.navigator.navigate('BasketPreviewBeforeBuyScreen');
  }
}

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  cartTireImageView: {
    borderColor: '#fbb717',
    borderWidth: 4,
    borderRadius: 3,
    width: 140,
    height: 140,
  },

  cartTireImage: {
    width: '90%',
    height: '90%',
    margin: '5%',
  },

  cartTireName: {
    fontSize: 25,
    textAlign: 'right',
    marginRight: windowWidth * 0.05,
    marginTop: 10,
  },

  cartProductInfoView: {
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginBottom: 5,
  },

  cartProductInfoViewParent: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: windowWidth * 0.1,
    paddingRight: windowWidth * 0.1,
  },
});
