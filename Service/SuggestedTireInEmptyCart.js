import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions } from 'react-native';

const TireWithPrice = (props) => {
    return(
        <View style={[styles.tireWithPriceView, {backgroundColor: props.bgColor || "orange"}]}>

            <Image style={styles.tireWithPriceImg}  resizeMode='contain' source={props.img}/>
            <Text style={styles.tireWithPriceName}>{props.name}</Text>

        </View>
    );
}

export default TireWithPrice;

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create(
    {
            tireWithPriceView: {
                width: 0.29 * windowWidth,
                height: 0.30 * windowWidth,
                marginTop: 0.03 * windowWidth,
                // backgroundColor: '#feb816',
                borderRadius: 5,
                textAlign: "right",
                position: 'relative',
            },

            tireWithPriceName: {
                color: 'black',
                fontSize: 20,
                textAlign: "right",
                padding: 5
            },

            tireWithPriceInfo: {
                color: 'white',
                fontSize: 11,
                textAlign: "right",
                paddingRight: 5
            },

            tireWithPriceImg: {
                width: '95%',
                height: '50%',
                borderRadius: 5,
                marginLeft: '2.5%',
                marginRight: '2.5%',
                marginTop: 5,
                backgroundColor: 'white'
            }
    }
)

