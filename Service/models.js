export class Partner {
  id;
  name;
  description;
  behaviour;
  cityId;
  city: City;
  tblItem: TblItem[];
  media: Media[];
  adress;
}

export class Media {
  id;
  name;
  type;
  partner: Partner;
}

export class ActiveComponent {

  constructor(componentName: string, compnent: React.Component) {
    this.componentName = componentName;
    this.compnent = compnent;
  }

  componentName: string;
  compnent: React.Component;
}

export class City {
  id;
  name;
}

export class TblItem {
  id;
  name;
  price;
  partnetId;
  serviceOrGood;
}

export class itemToPurchase {
  TblItemId;
  PurchaseId;
  tblItem: TblItem;
}

export class Category {
  id;
  name;
}

export class ServiceProposeViewModel {
  thisPartnerSell: PartnersItems[] = [];
  otherPartnersSell: PartnersItems[] = [];
}

export class AlreadyPurchasedViewModel {

  partnerItems:PartnersItems[];
  totalFee:number;
  paymentStatusText:string;
  title:string;
  purchaseId;
  orderId;
  paymentId;
}

export class TuppleAlreadyPurchasedViewModel {
  successPurchased:AlreadyPurchasedViewModel[];
  failPurchased:AlreadyPurchasedViewModel[];

}
export class PartnersItems {
  id;
  count: number;
  partnerId;
  tblItemId;
  sendType: PartneritemSendType;
  price;
  itemComNameByPartner;
  partner: Partner;
  tblItem: TblItem;
  basketItems: BasketItems[];
  relatedBasketPartnerItems: PartnersItems[];
  isLeaf: boolean;
  isMainBasketItem;
  checked: boolean;
  muliplyPrice: number;
  relatedGoods: PartnersItems[];
  selected: boolean;
  iHaveGoodsConfirmed: boolean;
}

export const PartneritemSendType = {
  None: 0,
  Motory: 1,
  PTT: 2,
};

export class BasketItems {
  id;
  basketId;
  partnerItemId;
  basket: Basket;
  partnerItem: PartnersItems;
}

export class Basket {
  id;
  name;
  partnetId;
  Partnet: Partner;
  BasketItems: BasketItems[];
}

