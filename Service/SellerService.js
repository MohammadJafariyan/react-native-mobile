import React from 'react';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import {MyResponse} from './Myglobal';
import {Media, Partner} from './models';
import {retrocycle} from '../Helpers/JsonRefrecenseResolver';

export default class SellerService {
  getById(id): Promise<MyResponse<Partner>> {
    console.log('SellerService', ApiUrls.GetSellerByIdURL(id));
    return fetch(ApiUrls.GetSellerByIdURL(id))
      .then(response => response.json())
      .then(res => retrocycle(res))

      .catch(error => {
        console.log('errrrrrr SellerService rrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);

        // resolve(list);
      });
    /*  return new Promise(
        function (resolve, reject) {
          console.log('request : ===========>',ApiUrls.GetAllTblitems);
          return fetch(ApiUrls.GetAllTblitems)
              .then(response => response.json())
              .catch(error => {

                    console.log('errrrrrrrrrrrrrrrrrrrrrrrrrrrrror', error);
                   // console.error(error);
                    var list=[];

                    list.push(getNew(1));
                    list.push(getNew(2));
                    list.push(getNew(3));
                    list.push(getNew(4));
                    list.push(getNew(5));
                    resolve(list);
          });

        });*/
  }

  getImagesByPartnerId(id): Promise<MyResponse<Media[]>> {
    console.log('SellerService', ApiUrls.GetSellerByIdURL(id));
    return fetch(ApiUrls.GetSellerImagesByIdURL(id))
      .then(response => response.json())
      .then(res => retrocycle(res))

      .catch(error => {
        console.log('errrrrrr SellerService rrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);

        // resolve(list);
      });
  }
}
export function getNew(number) {
  var obs = {};
  obs.name = 'تبریز' + number;
  obs.id = 12 + number;
  obs.price = number * 350 + 100000;

  var partnet = {};
  partnet.name = 'عباسی' + number;
  partnet.id = 15 + number;
  partnet.description = 'نصف راه' + number;

  obs.partnet = partnet;
  return obs;
}
