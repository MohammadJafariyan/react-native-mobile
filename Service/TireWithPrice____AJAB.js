//Dummy


import CityService from './ServicesService';

<View
	style={{
		flex:1,

	  	//borderWidth : 5,
	  	//borderColor: '#F44336',


		flexDirection: 'column',
		justifyContent: 'flex-start',

		alignItems: 'stretch'

	      }}>
        <View style={{


          height: 100,
	  //borderWidth : 5,
	  //borderColor: '#F44336',
	  justifyContent: 'center',
	  alignItems: 'center',

        }}>
          <Text style={{fontSize:30}}>AA</Text>
        </View>

        <View style={{


          height: 100,
	  //borderWidth : 5,
	  //borderColor: '#F44336',
	  justifyContent: 'center',
	  alignItems: 'center',

        }}>
          <Text style={{fontSize:30}}>BB</Text>
        </View>


        <View style={{

          flex: 1,
          height: 100,
	  //borderWidth : 5,
	  //borderColor: '#F44336',

	  flexDirection: 'row',
	  justifyContent: 'flex-start',

	  alignItems: 'flex-start',
	  alignContent: 'flex-start',
	  flexWrap: 'wrap',


        }}>
          {this.showDummyList()}
        </View>
	</View>;



// category Screen
            <LinearGradient
                style={style.linearBG}
                colors={['#fdbb13', '#fbe70f', 'white']}>
              <View
                  style={{
                    flex: 1,

                    //borderWidth : 5,
                    ////borderColor: '#F44336',

                    justifyContent: 'flex-start',

                    alignItems: 'stretch',
                  }}>
                <View
                    style={{
                      height: 100,
                      //borderWidth: 5,
                      //borderColor: '#F44336',
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row-reverse',
                    }}>
                  <Text>انتخاب شهر </Text>
                  <View style={{borderWidth: 1, borderColor: '#ee710e'}}>
                    <Picker
                        selectedValue={DataBean.selectedCityId}
                        style={{height: 50, width: 200}}
                        onValueChange={(itemValue, itemIndex, arr) => {
                          console.log('picker====>selected==>', itemValue);

                          DataBeanService.setSelectedCategoryId(itemValue);
                          DataBeanService.setSelectedCityName(
                              DataBean.cities[itemIndex].name,
                          );

                          DataBeanService.setSelectedCityId(itemValue);
                          DataBeanService.setCategories([]);
                          DataBeanService.setItemList([]);

                          this.readAndSetCategories();
                        }}>
                      {DataBean.cities && this.showCitiesItems()}
                    </Picker>
                  </View>
                </View>

                <View
                    style={{
                      height: 100,
                      //borderWidth: 5,
                      //borderColor: '#F44336',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                  <Text style={{fontSize: 30}}>دسته بندی محصولات</Text>
                </View>

                <View
                    style={{
                      flex: 1,
                      height: 100,
                      //borderWidth: 5,
                      //borderColor: '#F44336',

                      flexDirection: 'row-reverse',
                      justifyContent: 'flex-start',

                      alignContent: 'flex-start',
                      flexWrap: 'wrap',
                      margin: 10,
                    }}>
                  {DataBean.categories && this.showCategoriesAsButtons()}

                  {DataBean.categories && (
                      <View>
                        {!DataBean.categories ||
                        (DataBean.categories.length <= 0 && (
                            <Text style={{textAlign: 'center', marginTop: 100}}>
                              {' '}
                              بازار در این شهر خالی است
                            </Text>
                        ))}
                      </View>
                  )}
                </View>
              </View>
            </LinearGradient>





// screenService.js
import { Dimensions } from 'react-native';
import Orientation from 'react-native-orientation';

let screen = {};

Orientation.addOrientationListener(orientation => {
  const { height, width } = Dimensions.get('window');
  const min = Math.min(height, width);
  const max = Math.max(height, width);
  const isLandscape = orientation === 'LANDSCAPE';
  screen = {
    height: isLandscape ? min : max,
    width: isLandscape ? max : min,
  };
});

export default { screen };

Then we just call our module like screenService.screen.height to get the current value. The logic works for Android and iOS, so no conditional code is required. Hope that can help someone until the issue is resolved.





/////////



import React from 'react';
import {
  Alert,
  Button,
  default as Colors,
  Image,
  Picker,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';
import TblItemService from '../Service/TblItemService';
import {SettingSingleton} from './NavigateManagerScreen';
import {MyResponse, MyResponseType} from '../Service/Myglobal';
import {City, PartnersItems} from '../Service/models';
import {DataBean, DataBeanService} from '../Service/DataBean';
import SelectRelateItemsModal from './SelectRelateItemsModal';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import ServiceRelatedGoodsModal from './ServiceRelatedGoodsModal';
import {BasketValidatorService} from '../Service/BasketValidatorService';


/*
*
*
*  SingletonHolder.navigator.selectedCityId = this.state.selectedCityId;
    SingletonHolder.navigator.selectedCategoryId = this.state.selectedCategoryId;
*/
export default class TblItemsListScreen extends React.Component {
  constructor(state: {text: string}) {
    super();

    DataBeanService.setShowModal(false);
    this.state = state;
    DataBean.currentScreen = this;
    this.state = DataBean;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  getAllTblItems(
    getAll,
    selectedCategoryId,
  ): Promise<MyResponse<PartnersItems>> {
    console.log('--------------------------calling--------------------------');
    var s = new TblItemService();
    return s.getAll(getAll, selectedCategoryId);
  }

  getAllCities(): Promise<MyResponse<City>> {
    let s = new CityService();
    return s.getAllCity();
  }

  loadTblitems() {
    console.log('loadTblitems , selectedCityId=>', DataBean.selectedCityId);
    console.log(
      'loadTblitems , selectedCategoryId=>',
      DataBean.selectedCategoryId,
    );
    this.getAllTblItems(
      DataBean.selectedCityId,
      DataBean.selectedCategoryId,
    ).then(res => {
      if (res.type === MyResponseType.Success) {
        if (DataBean.selectedItems.length > 0) {
          // this.setState({selectedItems: SingletonHolder.selectedItems});
        }

        if (DataBean.selectedItems && DataBean.selectedItems.length > 0) {
          for (var i = 0; i < res.list.length; i++) {
            if (
              DataBean.selectedItems.find(s => s.id == res.list[i].id) != null
            ) {
              res.list[i].selected = true;
            }
          }
        }

        DataBeanService.setItemList(res.list);
      } else {

        alert(ApiUrls.showErrorMsg(res.message));

      }

      DataBeanService.retRefreshing(false);
    });
  }

  componentDidMount() {
    // todo: در این تابع ، وب سرویس ها فراخوانی می شوند
    this._isMounted = true;

    DataBeanService.retRefreshing(true);
    if (DataBean.isCategoryChanged == true) {
      console.log('---------------------refresh tirelistst------------');
      DataBeanService.setItemList([]);
      DataBean.isCategoryChanged = false;
    }
    this.loadTblitems();
  }

  renbody() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'space-around',
          flexDirection: 'column',
        }}>
        <View
          style={{
            flex: 9,
          }}>
          {!DataBean.itemList ||
            (DataBean.itemList.length <= 0 && !DataBean.refreshing && (
              <Text style={{textAlign: 'center', marginTop: 100}}>
                {' '}
                بازار انتخاب شده در این شهر خالی است
              </Text>
            ))}

          {DataBean.itemList &&
            DataBean.itemList.length > 0 &&
            this.show_TireList_ZZ_threeColumn()}
        </View>
        <View style={{height: 100}} />

        {DataBean.showModal && <SelectRelateItemsModal />}
        {DataBean.showServiceRelatedGoodsModal && <ServiceRelatedGoodsModal />}
        {/* <View
          style={{
            flex: 1,
            backgroundColor: '#8dfcff',
          }}>
          {DataBean.selectedItems && DataBean.selectedItems.length > 0 && (
            <Button title="خرید" onPress={() => this.goBuyScreen()} />
          )}
        </View>*/}
      </View>
    );
  }

  render() {
    // todo:در این متد صفحه اصلی اسکرین نمایش داده می شود
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <View
          style={{
            backgroundColor: '#fbb717',
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          {/*// todo: اینجا dropdown*/}
          <Text style={{fontSize: 35, color: 'white'}}>
            {DataBean.selectedCityName}
          </Text>

          {DataBean.selectedCategoryName && (
            <Text style={{fontSize: 20, color: 'white'}}>
              {DataBean.selectedCategoryName} ها و خدمات مرتبط در بازار
            </Text>
          )}
        </View>
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}
            refreshControl={
              <RefreshControl
                refreshing={DataBean.refreshing}
                onRefresh={() => this.refreshComp()}
              />
            }>
            {this.renbody()}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }

  show_TireList_ZZ_threeColumn() {
    return DataBean.itemList.map((m, i) => {
      return (
        <View>
            {m && this.showItemsClass(m)}
        </View>
      );
    });
  }

  showTireListSingleList() {
    return DataBean.itemList.map((m, i) => {
      return (
        <View
          style={{flex: 1, justifyContent: 'flex-start', flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              borderRadius: 50,
              borderWidth: 1,
              borderColor: '#ffb915',
              margin: 5,
            }}>
            {m && this.showTblItem(m)}
          </View>
        </View>
      );
    });
  }

  showTireList(Rlist, Llist) {
    // todo: تابع نمایش دهنده ی لیست بصورت دو ستونی
    return Rlist.map((m, i) => {
      let l_item = Llist.length <= i ? null : Llist[i];
      let r_item = Rlist.length <= i ? null : Rlist[i];

      return (
        <View
          style={{flex: 1, justifyContent: 'flex-start', flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              alignItems: 'stretch',
              height: 150,
            }}>
            {l_item && this.showTblItem(l_item)}
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'stretch',
              height: 150,
            }}>
            {r_item && this.showTblItem(r_item)}
          </View>
        </View>
      );
    });
  }

  showCitiesItems() {
    // todo: تابع نمایش دهنده شهر ها
    return DataBean.cities.map((m, i) => {
      return <Picker.Item label={m.name} key={m.id} value={m.id} />;
    });
  }



  showItemsClass(item) {
  function numberWithCommas(x) {
    y = Number(x)
    return y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };
	  return(



	<TireWithPrice name={item.partner.name}/>



	)
  };

  showTblItem(item) {
    return (
      <TouchableOpacity
        key={item.id}
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingRight: 40,
          paddingLeft: 10,
          paddingTop: 10,
          paddingBottom: 5,
        }}
        onPress={() => this.addToBasket(item)}>
        <View style={{flex: 1}}>
          <Image
            source={require('../assets/img/index.jpg')}
            style={{flex: 1, opacity: 0.4}}
          />
        </View>



        <View style={{flex: 9, flexDirection: 'column'}}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            {item.itemComNameByPartner
              ? item.itemComNameByPartner
              : item.tblItem.name}
          </Text>

          <Text style={{}}>{item.partner.name}</Text>
          <Text style={{}}>
            {item.partner.city.name} - {item.partner.adress}
          </Text>

          <View style={{flex: 1, height: 60}} />
          <View style={{flexDirection: 'row', alignItems: 'stretch'}}>
            {item.selected && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}

            <Text style={{flex: 1, textAlign: 'left'}}>
              {' '}
              {item.price} تومان{' '}
            </Text>
            {/* <Button
              title={item.partner.name}
              onPress={() => this.TestNavigate()}
            />*/}

            <View>
              <Button
                title="صفحه فروشنده"
                onPress={() => this.goSellerScreen(item)}
              />
            </View>
          </View>

          {showFixTypes(item.partner)}
        </View>
      </TouchableOpacity>
    );
  }

  selectThisItem(item) {
    DataBeanService.pushOrPopToBasket(item);

  }

  goBuyScreen() {
    SettingSingleton.selectedItems = DataBean.selectedItems;
    SettingSingleton.navigator.navigate('BuySuccessedScreen');
  }

  refreshComp() {
    DataBeanService.retRefreshing(true);
    this.componentDidMount();
  }

  goHelloScreen() {
    SettingSingleton.navigator.navigate('HelloScreen');
  }

  TestNavigate() {
    SettingSingleton.navigator.navigate('CarInfo');
  }

  goSellerScreen(item) {
    SettingSingleton.navigator.navigate('SellerScreen');
    console.log('goSellerScreen', item.partner.id);

    DataBeanService.setPartner(item.partner);
  }

  goCategory() {
    SettingSingleton.navigator.navigate('CategoryScreen');
  }

  addToBasket(item: PartnersItems) {

    //service & motory
    if (item.tblItem.serviceOrGood === 1 && item.sendType == 1) {
      this.addToBasketService(item);
    } else {
      this.addToBasketGood(item);
    }
  }

  addToBasketWithRelatedItems(tmp) {
    var item: PartnersItems = tmp;
    if (!item.selected) {
      item.selected = true;
      for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
        item.relatedBasketPartnerItems[i].selected = true;
      }
    } else {
      item.selected = false;
      for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
        item.relatedBasketPartnerItems[i].selected = false;
      }
    }

    // todo : آپدیت انتخاب شده ها
    let selectedItems = [...DataBean.selectedItems];

    if (!selectedItems) {
      selectedItems = [];
    }

    // todo: اگر انتخاب شده باشد
    if (item.selected) {
      selectedItems.push(item);
      for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
        selectedItems.push(item.relatedBasketPartnerItems[i]);
      }
    } else {
      // todo : اگر انتخاب نشده باشد ان را حذف کن
      var i = selectedItems.findIndex(s => s.id == item.id);
      selectedItems.splice(i, 1);

      for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
        var j = selectedItems.findIndex(
          s => s.id == item.relatedBasketPartnerItems[i].id,
        );
        selectedItems.splice(j, 1);
      }
    }

    DataBeanService.setSelectedItems(selectedItems);
  }

  showAlertForMainItem(item: PartnersItems) {
    DataBeanService.setShowModal(true);
    DataBeanService.setShowRelateAlertForItem(item);
    DataBeanService.setCallback(this, 'callbackForRelateItemsModal');
  }
  callbackForRelateItemsModal(isOk: boolean, item: PartnersItems) {
    if (isOk) {
      //this.showConfirm(item);
      this.selectThisItem(item);
      DataBeanService.setShowModal(false);
    }
  }

  showConfirm(item: PartnersItems) {
    var text = '';
    for (var i = 0; i < item.relatedBasketPartnerItems.length; i++) {
      text += item.relatedBasketPartnerItems[i].tblItem.name + '\n';
    }

    text += '\n';
    text += 'در این صورت مایلید اضافه شود ؟';
    Alert.alert('این آیتم باید همراه آیتم های زیر خرید شود ', text, [
      {
        text: 'خیر',
        onPress: () => console.log('NO Pressed'),
        style: 'cancel',
      },
      {text: 'بله', onPress: () => this.selectThisItem(item)},
      //  {text: 'بله', onPress: () => this.addToBasketWithRelatedItems(item)},
    ]);
  }

  addToBasketGood(item: PartnersItems) {
    item.muliplyPrice = null;
    if (item.relatedBasketPartnerItems) {
      // تنها در اضافه کردن پیغام می دهد
      if (!item.selected) {
        // آیا آیتم اصلی است ؟ اضافه کن و چیزی نگو و الی اگر آیتم اصلی نیست ؟ پس خود آن را اضافه کن و کار دیگری نکن

        var xodash = item.relatedBasketPartnerItems.find(r => r.id == item.id);

        for (let i = 0; i < item.relatedBasketPartnerItems.length; i++) {
          item.relatedBasketPartnerItems[i].muliplyPrice = null;
        }

        console.log('item.isMainBasketItem');
        console.log(xodash.isMainBasketItem);
        if (xodash.isMainBasketItem) {
          item.isMainBasketItem = true;
          this.showAlertForMainItem(item);
        } else {
          this.selectThisItem(item);
        }
      } else {
        this.selectThisItem(item);
      }
    } else {
      this.selectThisItem(item);
    }
  }

  addToBasketService(item: PartnersItems) {
    DataBeanService.setSelectedServiceTypeTblItem(item);
    DataBeanService.setShowServiceRelatedGoodsModal(true);

    /*todo:از اینجا ماند که کاربر سرویس را انتخاب می کند.بعد از آن باید کالا های مرتبط را انتخاب نماید*/
  }
}

export function showFixTypes(partner) {
  return (
    <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch'}}>
      {partner.movable === 0 && (
        <Text style={{flex: 1, textAlign: 'right', color: 'blue'}}> FIX</Text>
      )}
      {partner.movable === 1 && (
        <Text style={{flex: 1, textAlign: 'right', color: 'red'}}> MOT</Text>
      )}

      {partner.movable === 2 && (
        <Text style={{flex: 1, textAlign: 'right', color: 'yellow'}}> PTT</Text>
      )}
      {partner.movable === 3 && (
        <Text style={{flex: 1, textAlign: 'right', color: 'green'}}> MTT</Text>
      )}
    </View>
  );
}

const TireWithPrice = (props) => {
  return(
      <View style={styles.tireWithPriceView}>

      <Image style={styles.tireWithPriceImg}  resizeMode='contain' source={{uri: props.img}}/>
      <Text style={styles.tireWithPriceName}>{props.name}</Text>
      <Text style={styles.tireWithPriceInfo}>فروشنده: {props.shop}</Text>

    </View>
  );
}


const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create(
    {
            tireWithPriceView: {
                width: 0.3 * windowWidth,
                height: 0.32 * windowWidth,
                marginTop: 0.03 * windowWidth,
                marginLeft: 5,
                marginRight: 5,
                backgroundColor: '#feb816',
                borderRadius: 5,
                textAlign: "right",
                position: 'relative'
            },

            tireWithPriceName: {
                color: 'black',
                fontSize: 20,
                textAlign: "right",
                paddingRight: 5
            },

            tireWithPriceInfo: {
                color: 'white',
                fontSize: 11,
                textAlign: "right",
                paddingRight: 5
            },

            tireWithPriceImg: {
                width: '95%',
                height: '50%',
                borderRadius: 5,
                marginLeft: '2.5%',
                marginRight: '2.5%',
                marginTop: 5,
                backgroundColor: 'white'
            }
    ,




  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
