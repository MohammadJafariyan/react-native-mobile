import {MyResponse, MyResponseType} from './Myglobal';
import {Partner} from './models';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import {retrocycle} from '../Helpers/JsonRefrecenseResolver';

export default class RegisterService {
  SendMobile(mobileNumber: string): Promise<MyResponse<number>> {
    var data = new FormData();
    data.append('mobileNumber', mobileNumber);

    return fetch(ApiUrls.SendMobileUrl(), {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: data,
    })
      .then(response => response.json())
      .then(res => retrocycle(res))

      .catch(error => {
        console.log('errrrrrr SellerService rrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);

        alert('خطا در اتصال به اینترنت');
        // resolve(list);
      });
  }
  ValidateRegisterNumber(
    registerNumber: string,
    customerId: number,
  ): Promise<MyResponse<string>> {
    return fetch(ApiUrls.ValidateRegisterNumberURL(registerNumber, customerId))
      .then(response => response.json())
      .then(res => retrocycle(res))

      .catch(error => {
        console.log('errrrrrr SellerService rrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);

        alert('خطا در اتصال به اینترنت');
        // resolve(list);
      });
  }
  ValidateToken(token: string): Promise<MyResponse<string>> {
    return fetch(ApiUrls.ValidateTokenURL(token))
      .then(response => response.json())
      .then(res => retrocycle(res))
      .catch(error => {
        console.log('errrrrrr SellerService rrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);

        alert('خطا در اتصال به اینترنت');
        // resolve(list);
      });
  }

  SignInWithUsernamePassword(username, password): Promise<MyResponse<string>> {
    var data = new FormData();
    data.append('username', username);
    data.append('password', password);

    return fetch(ApiUrls.SignInWithUsernamePasswordURL(), {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: data,
    })
      .then(response => response.json())
      .catch(error => {
        console.log('errrrrrr SellerService rrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);

        alert('خطا در اتصال به اینترنت');
        // resolve(list);
      });
  }
}
