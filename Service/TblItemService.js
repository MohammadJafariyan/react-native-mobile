import React from 'react';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import {PartnersItems, TblItem} from './models';
import {MyResponse} from './Myglobal';
import {retrocycle} from '../Helpers/JsonRefrecenseResolver';

export default class TblItemService {
  getAllCategories(selectedCityId): Promise<MyResponse<Category>> {
    return fetch(ApiUrls.GetAllCategories(selectedCityId))
      .then(response => response.json())
        .then(res=>retrocycle(res))
        .then(res=>{
            console.log(res);
            return res;
        }).catch(error => {
        console.log('errrrrrrrrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);
        var list = [];

        list.push(getNew(1));
        list.push(getNew(2));
        list.push(getNew(3));
        list.push(getNew(4));
        list.push(getNew(5));
        return new Promise(function(resolve, reject) {
          resolve(list);
        });
        // resolve(list);
      });
  }
  getAll(cityId, selectedCategoryId): Promise<MyResponse<PartnersItems>> {
    return fetch(ApiUrls.GetAllTblitems(cityId, selectedCategoryId))
      .then(response => response.json())
        .then(res=>retrocycle(res))
       /* .then(res=>{
            console.log(res);
            console.log('res.type -------------------------------------->');
            console.log(res.type);
            return res;
        })*/
        .catch(error => {
        console.log('errrrrrrrrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);
        var list = [];

        list.push(getNew(1));
        list.push(getNew(2));
        list.push(getNew(3));
        list.push(getNew(4));
        list.push(getNew(5));
        return new Promise(function(resolve, reject) {
          resolve(list);
        });
        // resolve(list);
      });
    /*  return new Promise(
        function (resolve, reject) {
          console.log('request : ===========>',ApiUrls.GetAllTblitems);
          return fetch(ApiUrls.GetAllTblitems)
              .then(response => response.json())
              .catch(error => {

                    console.log('errrrrrrrrrrrrrrrrrrrrrrrrrrrrror', error);
                   // console.error(error);
                    var list=[];

                    list.push(getNew(1));
                    list.push(getNew(2));
                    list.push(getNew(3));
                    list.push(getNew(4));
                    list.push(getNew(5));
                    resolve(list);
          });

        });*/
  }
}
export function getNew(number) {
  var obs = {};
  obs.name = 'تبریز' + number;
  obs.id = 12 + number;
  obs.price = number * 350 + 100000;

  var partnet = {};
  partnet.name = 'عباسی' + number;
  partnet.id = 15 + number;
  partnet.description = 'نصف راه' + number;

  obs.partnet = partnet;
  return obs;
}
