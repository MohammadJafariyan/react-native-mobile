import {_MyLog, ApiUrls} from '../MyGlobal/ApiUrls';
import {MyResponse, MyResponseType} from './Myglobal';
import {TuppleAlreadyPurchasedViewModel, PartnersItems, AlreadyPurchasedViewModel} from './models';
import {IdpayTx} from '../Models/models';
import {getNew} from "./TblItemService";
import {DataBean} from "./DataBean";

export class SendPurchaseItem {
  PartnerItemId;
  Count;
  IHaveGoodsConfirmed;
  Checked;
  PartnerId;
  TblItemId;
  RelatedBasketPartnerItems: SendPurchaseItem[];
  RelatedGoods: SendPurchaseItem[];
}

export class PurchaseService {
  Save(obj) {
    return fetch(ApiUrls.PurchaseSaveURL, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(obj),
    });
  }

  getPurchaseStatus(purchaseId, token): Promise<MyResponse<IdpayTx>> {
    console.log(ApiUrls.getPurchaseStatusURL(purchaseId, token));
    console.log('-------------post------------');

    return fetch(ApiUrls.getPurchaseStatusURL(purchaseId, token))
      .then(response => {
        console.log(response);
        return response;
      })
      .then(response => response.json())
      .catch(error => {
        console.log('errrrrrr SellerService rrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);

        alert('خطا در اتصال به اینترنت');
        // resolve(list);
      });
  }

  Purchase(
    total,
    selectedItems: SendPurchaseItem[],
    token,
  ): Promise<MyResponse<string>> {
    console.log(ApiUrls.purchaseURL(token, total));
    console.log(JSON.stringify(selectedItems));
    console.log('-------------post------------');

    return fetch(ApiUrls.purchaseURL(token, total), {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(selectedItems),
    })
      .then(response => {
        console.log(response);
        return response;
      })
      .then(response => response.json())
      .catch(error => {
        console.log('errrrrrr SellerService rrrrrrrrrrrrrrrrrrrrrrror', error);
        // console.error(error);

        alert('خطا در اتصال به اینترنت');
        // resolve(list);
      });
  }

  Extract(selectedItems: PartnersItems[]) {
    var arr = [];
    for (let i = 0; i < selectedItems.length; i++) {
      var purchaseItem = new SendPurchaseItem();

      purchaseItem = this.ExtractOnePartnerItem(selectedItems[i]);

      purchaseItem.RelatedBasketPartnerItems = this.ExtractRelated(
        selectedItems[i].relatedBasketPartnerItems,
      );

      purchaseItem.RelatedGoods = this.ExtractRelated(
        selectedItems[i].relatedGoods,
      );

      arr.push(purchaseItem);
    }
    return arr;
  }

  ExtractRelated(relatedBasketPartnerItems: PartnersItems[]) {
    if (!relatedBasketPartnerItems) {
      return relatedBasketPartnerItems;
    }
    var arr = [];
    for (let i = 0; i < relatedBasketPartnerItems.length; i++) {
      var partnerItemTMP = new PartnersItems();

      partnerItemTMP = this.ExtractOnePartnerItem(relatedBasketPartnerItems[i]);
      arr.push(partnerItemTMP);
    }
    return arr;
  }

  ExtractOnePartnerItem(selectedItem: PartnersItems) {
    var purchaseItem = new SendPurchaseItem();
    purchaseItem.PartnerItemId = selectedItem.id;
    purchaseItem.Count = selectedItem.count;
    purchaseItem.IHaveGoodsConfirmed = selectedItem.iHaveGoodsConfirmed;
    purchaseItem.Checked = selectedItem.checked;
    purchaseItem.PartnerId = selectedItem.partnerId;
    purchaseItem.TblItemId = selectedItem.tblItemId;
    purchaseItem.ServiceOrGood = selectedItem.tblItem.serviceOrGood;
    purchaseItem.Price = selectedItem.price;
    return purchaseItem;
  }

  GetMyPurchases():Promise<MyResponse<TuppleAlreadyPurchasedViewModel>> {
    return fetch('http://hido').catch(e=>{
      var arr:TuppleAlreadyPurchasedViewModel=new TuppleAlreadyPurchasedViewModel();

      arr.successPurchased=[];
      arr.failPurchased=[];



      function MakeNewSuccess() {
        var example=new AlreadyPurchasedViewModel();
        example.partnerItems=DataBean.selectedItems;
        example.paymentStatusText=4500000;
        example.title="خرید موفق";
        example.totalFee=3500000;

        example.date=new Date().toLocaleDateString();
        arr.successPurchased.push(example);
      }





      function MakeNewFail() {
        var example2=new AlreadyPurchasedViewModel();
        example2.partnerItems=DataBean.selectedItems;
        example2.paymentStatusText=3500000;
        example2.totalFee=9500000;
        example2.title="خرید ناموفق ";
        example2.date=new Date().toLocaleDateString();

        arr.failPurchased.push(example2);
      }

      for (let i = 0; i < 20; i++) {
        MakeNewFail();
      }

      for (let i = 0; i < 20; i++) {
        MakeNewSuccess();
      }

      return new Promise(function(resolve, reject) {
        var res=new MyResponse(MyResponseType.Success,
            0,null,arr,'دریافت موفق');

        resolve(res);
      })
    })

  }
}

export class PurchaseItemService {
  SaveAll(obj) {
    return fetch(ApiUrls.PurchaseItemSaveAllURL, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(obj),
    });
  }
}
