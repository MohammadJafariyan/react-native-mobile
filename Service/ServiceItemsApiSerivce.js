import {PartnersItems, ServiceProposeViewModel} from './models';
import {MyResponse} from './Myglobal';
import {ApiUrls} from '../MyGlobal/ApiUrls';
import {retrocycle} from '../Helpers/JsonRefrecenseResolver';

export default class ServiceItemsApiService {
  getServiceItemsForServiceByPartnerId(
      partnerItemId,
    partnerId,
    cityId,
  ): Promise<MyResponse<ServiceProposeViewModel>> {
    console.log('getServiceItemsForServiceByPartnerId==>');
    console.log(
      ApiUrls.GetServiceItemsForServiceByPartnerIdAPI(
          partnerItemId,
        partnerId,
        cityId,
      ),
    );
    return fetch(
      ApiUrls.GetServiceItemsForServiceByPartnerIdAPI(
          partnerItemId,
        partnerId,
        cityId,
      ),
    )
      .then(response => response.json())
      .then(res => retrocycle(res))
      .then(responseJson => {
        console.log(
          ' getServiceItemsForServiceByPartnerId response=>',
          responseJson,
        );
        return responseJson;
      });
  }
}
