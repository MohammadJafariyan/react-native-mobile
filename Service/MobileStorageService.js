import {AsyncStorage} from 'react-native';

export class MobileStorageService {
  static _storeData = async (key, val) => {
    try {
      await AsyncStorage.setItem(key, val);
    } catch (error) {
      // Error saving data
      console.log(error);
      alert('خطا در ثبت به دیتای موبایل');
    }
  };

  static _removeItem = async key => {
    try {
      await AsyncStorage.removeItem(key);
    } catch (error) {
      // Error saving data
      console.log(error);
      alert('خطا در حذف دیتای موبایل');
    }
  };

  static _retrieveData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // Our data is fetched successfully
        console.log(value);
        return value;
      } else {
        return null;
      }
    } catch (error) {
      // Error retrieving data
      console.log(error);
      alert('خطا در دسترسی به دیتای موبایل');
    }
  };
}
