// @flow
import {requireNativeComponent, NativeModules} from 'react-native';
import React from 'react';
import {
  ActiveComponent,
  TuppleAlreadyPurchasedViewModel,
  City,
  Media,
  Partner,
  PartnersItems,
  ServiceProposeViewModel,
  AlreadyPurchasedViewModel,
} from './models';
import {Customer} from '../Models/customer';
import {_MyLog} from '../MyGlobal/ApiUrls';
import {ProfileScreen} from '../Sceeens/ProfileScreen';
import {SettingSingleton} from '../Sceeens/NavigateManagerScreen';
import {MyResponse} from './Myglobal';
import {IdpayTx} from '../Models/idpayTx';
import {CarTip, TblCar} from '../Models/tblCar';
import {TblCategory} from '../Models/tblCategory';
import help from '../assets/img/help.png';
import service from '../assets/img/service.png';
import home from '../assets/img/home.png';
import menu from '../assets/img/menu.png';
import cart from '../assets/img/cart.png';
import BottomNavigationBar from '../Components/BottomBar';

export class DataBeanStructure {
  index = 2;
  routes = [
    {
      key: 'music',
      // title: 'راهنمای خرید',
      icon: help,
      navPress: () => {
        SettingSingleton.navigator.navigate('WizardScreen');
      },
    },
    {
      key: 'albums',
      //  title: 'خدمات',
      icon: service,
      navPress: () => {
        SettingSingleton.navigator.navigate('TireServiceScreen');
      },
    },
    {
      key: 'category',
      //   title: 'دسته بندی',
      icon: home,
      navPress: () => {
        SettingSingleton.navigator.navigate('CategoryScreen');
      },
    },
    {
      key: 'tirelist',
      //   title: 'تایر ها',
      icon: menu,
      navPress: () => {
        SettingSingleton.navigator.navigate('TblItemsListScreen');
      },
    },
    {
      key: 'basket',
      //  title: 'سبد خرید',
      icon:
        DataBean && DataBean.selectedItems && DataBean.selectedItems.length > 0
          ? this.showCart
          : cart,
      navPress: () => {
        SettingSingleton.navigator.navigate('BuySuccessedScreen');
      },
    },
  ];
  cities: City[];
  selectedCityId: number;
  selectedCityName: string;
  selectedCategoryName: string;
  selectedCategoryId;
  categories;
  selectedItems: PartnersItems[] = [];
  partner: Partner;
  refreshing: boolean;
  itemList: PartnersItems[];
  currentScreen = {};
  listerners: Listener[];
  isCategoryChanged = false;
  partnerImages: Media[];
  showRelateAlertForItem: PartnersItems;
  callbackObject: any;
  showModal: boolean = false;
  callbackfuncName: string;
  totalFee;
  selectedServiceTypeTblItem: PartnersItems;
  showServiceRelatedGoodsModal: boolean;
  serviceProposeViewModel: ServiceProposeViewModel;
  goDetailPartnerItem: PartnersItems;
  mobileNumber: string;
  customerId: number;
  okcontract: boolean;
  showWebView: boolean;
  username: string;
  password: string;
  todayDate;

  alreadyPurchasedPartnerItems: TuppleAlreadyPurchasedViewModel;
  selectedAlreadyPurchyasedViewModel: AlreadyPurchasedViewModel;
  customer: Customer;
  systemMessage: string;
  successMessage: string;
  navigateScreensStack: string[];
  activeComponents: ActiveComponent[];
  paymentStatus: MyResponse<IdpayTx>;
  citySearchTerm: string;
  carSearchTerm: string;
  searchCars: TblCar[];
  showOkButton: boolean;
  selectedCar: TblCar = new TblCar();
  years: {id: string}[];
  selectedCarTips: CarTip[];
  showOnFlyMenu: boolean;
  currentTab: number;
  serviceServices: TblCategory[];
  bottomBar: BottomNavigationBar;
  serviceTotalFee;
  serviceNeedReview: string;
}

export class Listener {
  funcName: string;
  serviceObject;
}

export const DataBean: DataBeanStructure = new DataBeanStructure();

DataBean.selectedItems = [];

export class DataBeanService {
  static setCities(list: City[]) {
    DataBean.cities = list;
    this.setState();
  }

  static fireUpdateEvent() {
    if (!DataBean.listerners) {
      DataBean.listerners = [];
    }
    /*console.log(
      '<,,,,,,,<<<<<<<<<<<<<<<<<<<<<fireUpdateEvent>>>>>>>>>>>>>>>>>>>>>>',
      DataBean.listerners.length,
    );*/

    // یک کالر داریم ، که کال میکند لیسنر ها را
    for (let i = 0; i < DataBean.listerners.length; i++) {
      // اگر با نام باشد ، این مدلی بخوان
      if (DataBean.listerners[i].funcName) {
        DataBean.listerners[i].serviceObject[DataBean.listerners[i].funcName]();
      } else {
        // اگر با نام نباشد این مدلی
        DataBean.listerners[i].serviceObject();
      }
    }
  }

  /*
  * import { requireNativeComponent, NativeModules } from "react-native";

let CustomComponent = null;

if ("CustomComponent" in NativeModules.UIManager) {
   CustomComponent = requireNativeComponent("CustomComponent")
}*/
  static setState() {
    DataBean.currentScreen.setState(DataBean);
    //this.fireUpdateEvent();

    SettingSingleton.navigator.setState({uidd: Math.random()});
  }

  static setSelectedCityId(id) {
    DataBean.selectedCityId = id;
    this.setState();
  }

  static setSelectedCityName(name) {
    DataBean.selectedCityName = name;
    this.setState();
  }

  static setSelectedCategoryId(itemValue) {
    DataBean.selectedCategoryId = itemValue;
    DataBean.isCategoryChanged = true;
    this.setState();
  }

  static setSelectedCategoryName(name) {
    DataBean.selectedCategoryName = name;
    this.setState();
  }

  static setCategories(list) {
    DataBean.categories = list;
    this.setState();
  }

  static setItemList(list) {
    DataBean.itemList = list;
    this.setState();
  }

  static setSelectedItems(selectedItems) {
    DataBean.selectedItems = selectedItems;
    this.setState();
  }

  static retRefreshing(bool) {
    DataBean.refreshing = bool;
    this.setState();
  }

  static setPartner(partner) {
    DataBean.partner = partner;
    this.setState();
  }

  static setPartnerImages(list) {
    DataBean.partnerImages = list;
    this.setState();
  }

  static setShowRelateAlertForItem(item) {
    DataBean.showRelateAlertForItem = item;
  }

  static setCallback(callbackObject, callbackfuncName) {
    DataBean.callbackObject = callbackObject;
    DataBean.callbackfuncName = callbackfuncName;
  }

  static setShowModal(b) {
    DataBean.showModal = b;
    this.setState();
  }

  static getLastPrice(partnersItem) {
    partnersItem.muliplyPrice = partnersItem.price * partnersItem.count;
    return partnersItem.muliplyPrice
      ? partnersItem.muliplyPrice
      : partnersItem.price
      ? partnersItem.price
      : 0;
  }
  static refreshTotalFee() {
    var total = 0;
    if (DataBean.selectedItems && DataBean.selectedItems.length > 0) {
      for (let i = 0; i < DataBean.selectedItems.length; i++) {
        let price = DataBeanService.getLastPrice(DataBean.selectedItems[i]);
        if (DataBean.selectedItems[i].relatedBasketPartnerItems) {
          for (
            let j = 0;
            j < DataBean.selectedItems[i].relatedBasketPartnerItems.length;
            j++
          ) {
            let sub = DataBean.selectedItems[i].relatedBasketPartnerItems[j];
            if (sub.checked) {
              let subPrice = DataBeanService.getPrice(sub);
              sub.muliplyPrice = subPrice * DataBean.selectedItems[i].count;

              price += sub.muliplyPrice;
            }
          }
        }
        if (DataBean.selectedItems[i].relatedGoods) {
          for (
            let j = 0;
            j < DataBean.selectedItems[i].relatedGoods.length;
            j++
          ) {
            let sub = DataBean.selectedItems[i].relatedGoods[j];
            let subPrice = DataBeanService.getPrice(sub);
            sub.muliplyPrice = subPrice * DataBean.selectedItems[i].count;

            price += sub.muliplyPrice;
          }
        }

        total += price;
      }
    }
    DataBeanService.setTotalFee(total);
  }
  static getPrice(partnersItem) {
    return partnersItem.price ? partnersItem.price : 0;
  }
  static setTotalFee(total) {
    DataBean.totalFee = total;
    this.setState();
  }

  static setSelectedServiceTypeTblItem(item) {
    DataBean.selectedServiceTypeTblItem = item;
    this.setState();
  }

  static setShowServiceRelatedGoodsModal(b) {
    DataBean.showServiceRelatedGoodsModal = b;
    this.setState();
  }

  static setServiceProposeViewModel(Single) {
    DataBean.serviceProposeViewModel = Single;
    this.setState();
  }

  static pushOrUpdateToSelectedItems(value: PartnersItems) {
    //check null
    let selectedItems = [...DataBean.selectedItems];
    if (!selectedItems) {
      selectedItems = [];
    }
    value.selected = true;
    value.count = 1;

    // find if exist
    var i = selectedItems.findIndex(s => s.id == value.id);
    if (i >= 0) {
      // update if exist
      selectedItems[i] = value;
    } else {
      // push if not exist
      selectedItems.push(value);
    }

    DataBeanService.setSelectedItems(selectedItems);
    DataBeanService.refreshTotalFee();
  }

  static pushOrPopToBasket(item: PartnersItems) {
    item.count = 0;

    if (!item.selected) {
      item.selected = true;
      item.count = 1;
    } else {
      item.selected = false;
      item.count = 0;
    }
    let selectedItems = [...DataBean.selectedItems];

    /* var exist= selectedItems.find(s=>s.id==item.id);
    if(exist)
    {
      return;
    }*/

    if (!selectedItems) {
      selectedItems = [];
    }

    // todo: اگر انتخاب شده باشد
    if (item.selected) {
      selectedItems.push(item);
    } else {
      // todo : اگر انتخاب نشده باشد ان را حذف کن
      var i = selectedItems.findIndex(s => s.id == item.id);
      selectedItems.splice(i, 1);
    }

    DataBeanService.setSelectedItems(selectedItems);
    DataBeanService.refreshTotalFee();
  }

  static setGoDetailPartnerItem(item) {
    DataBean.goDetailPartnerItem = item;
    this.setState();
  }
  static setMobileNumber(text) {
    DataBean.mobileNumber = text;
    this.setState();
  }

  static setCustomerId(single) {
    DataBean.customerId = single;
    this.setState();
  }

  static setIsOkcontract(b) {
    DataBean.okcontract = b;
    this.setState();
  }

  static setShowWebView(b) {
    DataBean.showWebView = b;
    this.setState();
  }

  static setUsername(text) {
    DataBean.username = text;
    this.setState();
  }

  static setPassword(text) {
    DataBean.password = text;
    this.setState();
  }

  static setPurchaseId(single) {
    DataBean.purchaseId = single;
    this.setState();
  }

  static setTotay() {
    DataBean.todayDate = new Date().toLocaleDateString('fa-IR');
    this.setState();
  }

  static setCustomerProfile(customer: Customer) {
    DataBean.customer = customer;
    this.setState();
  }

  static setSystemMessage(message) {
    DataBean.systemMessage = message;
    this.setState();
  }

  static showSuccessMessage(b) {
    DataBean.successMessage = b;
    this.setState();
  }

  static pushToNavScreens(name) {
    if (
      name == 'CodeScreen' ||
      name == 'RegisterScreen' ||
      name == 'SignInScreen' ||
      name == 'VerifyRegisterScreen' ||
      name == 'SignInByUsernamePasswordScreen' ||
      name == 'HelloScreen'
    ) {
      return;
    }

    if (!DataBean.navigateScreensStack) {
      DataBean.navigateScreensStack = [];
    }
    if (
      DataBean.navigateScreensStack.length > 0 &&
      DataBean.navigateScreensStack[DataBean.navigateScreensStack.length - 1] ==
        name
    ) {
      // اگر آرایه مقدار داشته باشد و آخرین پوش آن تکراری باشد
    } else {
      DataBean.navigateScreensStack.push(name);
    }
  }

  static pushToActiveComponents(aus: ActiveComponent) {
    if (!DataBean.activeComponents) {
      DataBean.activeComponents = [];
    }
    var i = DataBean.activeComponents.findIndex(
      f => f.componentName == aus.componentName,
    );
    if (i >= 0) {
      DataBean.activeComponents[i] = aus;
    } else {
      DataBean.activeComponents.push(aus);
    }
  }

  static setPaymentStatus(res) {
    DataBean.paymentStatus = res;
    this.setState();
  }

  static popActiveComponent(name) {
    if (!DataBean.activeComponents) {
      DataBean.activeComponents = [];
    }
    var i = DataBean.activeComponents.findIndex(f => f.componentName == name);
    if (i >= 0) {
      DataBean.activeComponents.splice(i, 1);
    }
  }

  static getActiveComponent(name) {
    if (!DataBean.activeComponents) {
      DataBean.activeComponents = [];
    }
    var i = DataBean.activeComponents.findIndex(f => f.componentName == name);
    if (i >= 0) {
      return DataBean.activeComponents[i].compnent;
    }
    console.error(name);
    console.error('active not found');
    return null;
  }

  static setAlreadyPurchasedPartnerItems(
    list: TuppleAlreadyPurchasedViewModel,
  ) {
    DataBean.alreadyPurchasedPartnerItems = list;
    this.setState();
  }

  static setSelectedAlreadyPurchyasedViewModel(
    value: AlreadyPurchasedViewModel,
  ) {
    DataBean.selectedAlreadyPurchyasedViewModel = value;
    this.setState();
  }

  static setCitySearchTerm(citySearchTerm) {
    DataBean.citySearchTerm = citySearchTerm;
    this.setState();
  }

  static setCarSearchTerm(carSearchTerm) {
    DataBean.carSearchTerm = carSearchTerm;
    this.setState();
  }

  static setSearchCars(res) {
    DataBean.searchCars = res;
    this.setState();
  }

  static setshowOkButton(b) {
    DataBean.showOkButton = b;
    this.setState();
  }

  static setSelectedCar(tblCar) {
    DataBean.selectedCar = tblCar;
    this.setState();
  }

  static setYears(r) {
    DataBean.years = r;
    this.setState();
  }

  static setSelectedCarTips(list) {
    DataBean.selectedCarTips = list;
    this.setState();
  }

  static toggleShowOnFlyMenu() {
    if (DataBean.showOnFlyMenu) {
      DataBean.showOnFlyMenu = false;
    } else {
      DataBean.showOnFlyMenu = true;
    }
    this.setState();
  }

  static setCurrentTab(number) {
    DataBean.currentTab = number;
    this.setState();
  }

  static setServiceServices(list) {
    DataBean.serviceServices = list;
    this.setState();
  }

  static setNavStates(index) {
    DataBean.index = index;
    this.setState();
  }

  static setBottomNavbar(previeusScreen) {
    if (previeusScreen == 'CategoryScreen') {
      DataBean.bottomBar._handleBack(2);
    }
    if (previeusScreen == 'TblItemsListScreen') {
      DataBean.bottomBar._handleBack(3);
    }
    if (previeusScreen == 'BuySuccessedScreen') {
      DataBean.bottomBar._handleBack(4);
    }
    if (previeusScreen == 'ServicesService') {
      DataBean.bottomBar._handleBack(1);
    }
    if (previeusScreen == 'WizardScreen') {
      DataBean.bottomBar._handleBack(0);
    }
  }

  static setCurrentStep() {
    if (!DataBean.currentStep) {
      DataBean.currentStep = 1;
    }
    this.setState();
  }

  static setServiceTotalFee(totalFee) {
    DataBean.serviceTotalFee = totalFee;
    this.setState();
  }

  static setServiceNeedReview(needReview) {
    DataBean.serviceNeedReview = needReview;
    this.setState();
  }
}
