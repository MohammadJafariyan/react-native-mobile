import {MyResponse, MyResponseType} from './Myglobal';
import {TblCategory} from '../Models/tblCategory';
import {PartnersItems} from '../Models/partnersItems';

export default class ServicesService {
  getAllServices() {
    return new Promise(function(resolve, reject) {
      //   console.log('returnRandomFakeResponse')

      var arr = [];
      var res = new MyResponse();
      res.type = MyResponseType.Success;

      function addToArr(name, icon, catarr: TblCategory, color) {
        var service = new PartnersItems();
        service.itemComNameByPartner = name;
        service.icon = icon;
        service.price = Math.floor(Math.random() * 100) * 10000;
        service.bgColor = color;
        service.isPriceNeedReview = service.price % 3 == 0 ? true : false;
        if (!catarr.services) {
          catarr.services = [];
        }
        catarr.services.push(service);
      }
      var cat = new TblCategory('تایر', 'home');
      addToArr('بالانس', 'home', cat, '#B7005AEA');
      addToArr('تعویض در محل', 'home', cat, '#b3328e');
      addToArr('پنچر گیری', 'home', cat, '#0239ff');

      var cat2 = new TblCategory('باتری', 'home');
      addToArr('تعویض باطری', 'home', cat2, '#bb981a');

      var cat3 = new TblCategory('روغن', 'home');
      addToArr('تعویض روغن', 'home', cat3, '#B7005AEA');

      arr.push(cat);
      arr.push(cat2);
      arr.push(cat3);

      res.message = 'خطا فیک رخ داده است';
      res.list = arr;
      resolve(res);
    });
  }
}
