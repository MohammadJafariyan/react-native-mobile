// @flow
import {MyResponse, MyResponseType} from './Myglobal';
import {Brand, CarTip, TblCar} from '../Models/tblCar';
import {MobileStorageService} from './MobileStorageService';

export class CarService {
  getAllCarsWithCategory() {
    var brands = [];

    this.addToBrand = function(name) {
      var car = new Brand();
      car.name = name;
      car.id = Math.random();
      brands.push(car);
      return car;
    };
    this.addToArr = function(name, arr, jpg) {
      var car = new CarTip();
      car.name = name;
      car.id = Math.random();
      car.image=jpg;
      arr.push(car);
    };


    var iranXodro = this.addToBrand('ایرانخودرو');
    var saipa = this.addToBrand('سایپا');
    var ford = this.addToBrand('Ford');
    var nissan = this.addToBrand('Nissan');


    this.addToArr('پژو 405' , iranXodro , '');
    this.addToArr('پراید' , iranXodro , '');
  }

  clearMyCar() {
    return new Promise(function(resolve, reject) {
      MobileStorageService._removeItem('car_name');
      MobileStorageService._removeItem('car_image');
      MobileStorageService._removeItem('car_builtYear');
      MobileStorageService._removeItem('car_tipName');

      var res = new MyResponse();
      res.type = MyResponseType.Success;

      resolve(res);
    });
  }

  save(car: TblCar) {
    return new Promise(function(resolve, reject) {
      //   console.log('returnRandomFakeResponse')

      MobileStorageService._storeData('car_name', car.name);
      MobileStorageService._storeData('car_image', car.image);
      MobileStorageService._storeData('car_builtYear', car.builtYear);
      //   MobileStorageService._storeData('car_tipName', car.carTip.name);

      var res = new MyResponse();
      res.type = MyResponseType.Success;

      resolve(res);
    });
  }

  getTipsForMyCar(selected, tok) {
    return new Promise(function(resolve, reject) {
      //   console.log('returnRandomFakeResponse')

      var arr = [];

      this.addToArr = function(name, jpg) {
        var car = new CarTip();
        car.name = name;
        car.id = Math.random();
        arr.push(car);
      };

      this.addToArr('تیپ 1');
      this.addToArr('تیپ 2');

      var res = new MyResponse();
      res.type = MyResponseType.Success;
      res.list = arr;

      resolve(res);
    });
  }

  getAllAsPaging(searchTerm, skip, take, tok): Promise<MyResponse<TblCar>> {
    return new Promise(function(resolve, reject) {
      //   console.log('returnRandomFakeResponse')

      var arr = [];

      this.addToArr = function(name, jpg) {
        var car = new TblCar();
        car.name = name;
        car.image = jpg;

        arr.push(car);
      };

      this.addToArr(
        'پراید',
        'https://media.khabaronline.ir/d/2020/05/16/3/5393878.jpg',
      );
      this.addToArr(
        'پرو 405',
        'https://www.dorostcar.com/wp-content/uploads/2018/02/Peugeot-405-GLX.jpg',
      );

      this.addToArr(
        'ford',
        'https://imgctcf.aeplcdn.com/thumbs/p-nc-b-ver29/images/cars/generic/Ford-EcoSport-Top-Economical-Car.jpg',
      );

      var res = new MyResponse();
      res.type = MyResponseType.Success;
      res.list = arr;

      resolve(res);
    });
  }
}
