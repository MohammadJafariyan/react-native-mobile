import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const Title = (props) => {
    return(
         <View style={{flex: 1, flexDirection: "row-reverse"}}>
            <View style={styles.cartSuggestionTextOrangeBox}></View>
            <Text style={styles.cartSuggestionText}>{props.title}</Text>
        </View>
    );
}

export default Title;

const styles = StyleSheet.create(
    {
        cartSuggestionText: {
            textAlign: "right",
            fontSize: 14,
            paddingRight: 5
        },

        cartSuggestionTextOrangeBox: {
            height: 5, 
            width: 50, 
            backgroundColor: "orange", 
            top: 9
        },
    }
)