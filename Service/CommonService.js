import {TblCar} from '../Models/tblCar';
import {MyResponse, MyResponseType} from './Myglobal';

export class CommonService {
  getYears() {
    return new Promise(function(resolve, reject) {
      //   console.log('returnRandomFakeResponse')

      var arr = [];

      this.addToArr = function(yar) {
        var car = {};
        car.id = yar;

        arr.push(car);
      };

      for (let i = 1400; i > 1365; i--) {
        this.addToArr(i + '');
      }

      var res = new MyResponse();
      res.type = MyResponseType.Success;
      res.list = arr;

      resolve(res);
    });
  }
}
