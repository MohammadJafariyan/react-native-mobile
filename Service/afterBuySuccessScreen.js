import React from 'react';
import {Button, Text, View} from 'react-native';
import {SettingSingleton} from '../Sceeens/NavigateManagerScreen';
import {DataBean} from './DataBean';
import {AbstractScreen} from "../Sceeens/AbstractScreen";


export default class AfterBuySuccessScreen extends  AbstractScreen {



  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'space-around',
          flexDirection: 'column',
        }}>
        <Button title="بازگشت" onPress={() => this.backToHome()} />

        <Text style={{color: '#ff0907'}}>
          تعداد خرید شده : {DataBean.selectedItems.length}
        </Text>
      </View>
    );
  }

  backToHome() {
    SettingSingleton.navigator.navigate('TblItemsListScreen');
  }
}
