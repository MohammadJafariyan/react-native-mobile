import {_MyLog, ApiUrls} from '../MyGlobal/ApiUrls';
import {retrocycle} from '../Helpers/JsonRefrecenseResolver';
import {MyResponseOfCustomer} from '../Models/myResponseOfCustomer';
import {Customer} from '../Models/customer';
import {MyResponseOfString} from '../Models/myResponseOfString';
import {MyResponse, MyResponseType} from './Myglobal';
import {MobileStorageService} from './MobileStorageService';

export class CustomerService {
  GetCustomerProfileWithValidation(token): Promise<MyResponseOfCustomer> {
    _MyLog(ApiUrls.GetCustomerProfileWithValidation(token));
    return fetch(ApiUrls.GetCustomerProfileWithValidation(token))
      .then(response => response.json())
      .then(res => retrocycle(res))
      .then(responseJson => {
        console.log(' responseJson', responseJson);
        console.log(responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log('errrrrrrrrrrrrrrrrrrrrrrrrrrrrror', error);
        //   console.error(error);

        var list = [];
        var obs = {};
        obs.name = 'تبریز';
        obs.id = 12;
        list.push(obs);
        return new Promise(function(resolve, reject) {
          resolve(list);
        });
      });
  }
  isMyCarSelected(): Promise<MyResponse<boolean>> {
    return new Promise(function(resolve, reject) {
      //   console.log('returnRandomFakeResponse')

      MobileStorageService._retrieveData('car_name').then(car_name => {
        var res = new MyResponse();
        res.type = MyResponseType.Success;
        _MyLog('car_name is >>', car_name);
        res.single = car_name ? true : false;
        resolve(res);
      });
    });
  }

  SaveProfile(customer: Customer, token): Promise<MyResponseOfString> {
    _MyLog(ApiUrls.SaveProfileURL(token));
    _MyLog(JSON.stringify(customer));
    return fetch(ApiUrls.SaveProfileURL(token), {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(customer),
    })
      .then(response => response.json())
      .then(res => retrocycle(res))
      .then(responseJson => {
        console.log(' responseJson', responseJson);
        console.log(responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log('errrrrrrrrrrrrrrrrrrrrrrrrrrrrror', error);
        //   console.error(error);

        var list = [];
        var obs = {};
        obs.name = 'تبریز';
        obs.id = 12;
        list.push(obs);
        return new Promise(function(resolve, reject) {
          resolve(list);
        });
      });
  }

  SaveUsernamePassword(username, password, tok) {
    _MyLog(ApiUrls.SaveUsernamePasswordURL(username, password, tok));

    return fetch(ApiUrls.SaveUsernamePasswordURL(username, password, tok))
      .then(response => response.json())
      .then(res => retrocycle(res))
      .then(responseJson => {
        console.log(' responseJson', responseJson);
        console.log(responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log('errrrrrrrrrrrrrrrrrrrrrrrrrrrrror', error);
        //   console.error(error);

        var list = [];
        var obs = {};
        obs.name = 'تبریز';
        obs.id = 12;
        list.push(obs);
        return new Promise(function(resolve, reject) {
          resolve(list);
        });
      });
  }
}
